package edu.ycu.hockshop.dfsapi;

import java.io.File;
import java.io.IOException;

/**
 * 分布式文件存储接口.
 *
 * @author wrh
 * @since 2018-03-16
 *
 */
public interface DFS {

    /**
     * 文件上传接口.
     *
     * @param input 输入文件
     * @param outPath 输出路径(Hadoop文件系统绝对路径)
     * @return 上传成功返回true
     */
    boolean uploadFile(byte[] input, String outPath) throws IOException;

    /**
     * 获取文件.
     * @param fileName 文件名称
     * @param filePath 文件路径(Hadoop文件系统路径)
     * @return
     */
    byte[] obtainFileByFileName(String fileName, String filePath);

    /**
     * 通过文件名删除文件.
     * @param absolutePath 文件绝对路径
     * @return 是否删除成功.
     */
    boolean deleteFileByName(String absolutePath) throws IOException;

    /**
     * 判断文件是否存在.
     * @param path 文件路径(Hadoop文件系统路径)
     * @return 存在返回true,否则返回false
     */
    boolean isExits(String path) throws IOException;

}
