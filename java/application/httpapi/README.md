# 数字资产存证系统http接口模块
-------

## 部署说明

1. 前提准备

    - JAVA要求1.8及以上,.
    - 构建工具maven使用版本3.3以上

2. 各模块需求环境

    - [blockchain区块链中间件项目](../blockchain)为独立项目,其中包含两个子项目,server及client,
    本项目直接依赖client项目作为SDK与区块网络进行交互,client项目通过网络请求server进行数据交互,关于server项目部署参考
    
3. 应用打包

    在此目录下执行 `mvn clean package spring-boot:repackage`,构建之后在当前路径下的 `target`目录下将会看到 `hockshop-httpapi-1.0-SNAPSHOT.jar`文件.
    
4. 应用启动

    在上一步构建后的jar文件所在的路径下执行 `java -jar hockshop-httpapi-1.0-SNAPSHOT.jar`即可.

## 接口说明

**接口中${project_url}为项目地址**

**[错误码详细说明文档](doc/err-code.md)**
------

### [0.用户注册登陆接口](doc/user-api.md)

- 注册

- 登录


### [1.文件操作接口](doc/file-api.md)

包含以下接口

- 上传

- 下载 

### [2.系统功能接口](doc/feature-api.md)

- 获取能够处理的数字资产类型

- 获取可提取的信息特征

- 提取文件信息

### [3.数字资产接口](doc/asstets-api.md)

- 数字资产存证

- 数字资产检索及获取
  - 通过文件
  - 通过关键信息
  - 通过所有者
  - 通过日期
  
- 数字资产鉴权(下载)
    

### [4.服务器相关接口](doc/monitor-api.md)

- 获取已经监控的主机ip

- 获取服务器信息接口