# 数字资产信息相关接口
--------

## 数字资产上传

```
url: http://${project_url}/v1/assets
请求方式: POST,请求时请以json方式请求:contentType=application/json
```
参数说明:

|参数名      |参数类型|是否必须|备注|
|-----------|------|------ |---|
|type       |enum  |是     |doc, img, voice,video,other|
|md5        |string|是     |文件的md5值|
|info       |string|是     |需要上链的关键信息,建议使用资产特征模型序列化的json类型,便于上层应用使用数据|
|other      |String|否     |不上链的信息(由于单个区块容量有限,大于2MB信息将不予以上链)|
|classname  |string|是     |解析该资产文件对应的全类名|
|file_name  |string|是     |文件名(备注名)|

返回说明:成功时,err_code为0,description为数字资产id.

```json
{
  "description": "7f3434f87074af2f04dd6cb5798e4382",
  "err_code": 0
}
```

## 数字资产删除

```
url: http://${project_url}/v1/assets/delete/{id}
请求方式: GET
```
返回说明:删除成功时,err_code为0,description为数字资产id.

## 数字资产获取

#### 根据数字资产id获取:

```
url: http://${project_url}/v1/assets/id={id}
请求方式: GET
参数说明:id,类型string
```

返回示例:

```json
{
    "description": {
        "classname": "classname",
        "id": "2895f50a76f54330a876bdf359ac6d30",
        "info": "info",
        "md5": "4403b4786b706ee6b20e92b1586239dd",
        "oprate_code": "ADD",
        "timestamp": 1526304008292,
        "type": "document",
        "user": "99cb87c4462d02c2e2c459e395727b07"
    },
    "err_code": 0
}
```
    
#### 根据要页数进行批量获取:

    ```
    url: http://${project_url}/v1/assets/page={page}
    请求方式: GET
    参数说明:page，int类型
    ```
    返回示例:
    ```json
    {
        "description": [
            {
                "classname": "classname",
                "id": "4adfb7efa61f4b068bc900993d53ad26",
                "info": "info",
                "md5": "4403b4786b706ee6b20e92b1586239dd",
                "oprate_code": "ADD",
                "timestamp": 1526303661359,
                "type": "document",
                "user": "99cb87c4462d02c2e2c459e395727b07"
            },
            {
                "classname": "classname",
                "id": "72f5efe501cf4085afab3b179e3c777a",
                "info": "info",
                "md5": "4403b4786b706ee6b20e92b1586239dd",
                "oprate_code": "ADD",
                "timestamp": 1526303870606,
                "type": "document",
                "user": "99cb87c4462d02c2e2c459e395727b07"
            },
            {
                "classname": "classname",
                "id": "13e18bbcac234e34aea54f5cf6d908bb",
                "info": "info",
                "md5": "4403b4786b706ee6b20e92b1586239dd",
                "oprate_code": "ADD",
                "timestamp": 1526303897848,
                "type": "document",
                "user": "99cb87c4462d02c2e2c459e395727b07"
            },
            {
                "classname": "classname",
                "id": "2895f50a76f54330a876bdf359ac6d30",
                "info": "info",
                "md5": "4403b4786b706ee6b20e92b1586239dd",
                "oprate_code": "ADD",
                "timestamp": 1526304008292,
                "type": "document",
                "user": "99cb87c4462d02c2e2c459e395727b07"
            }
        ],
        "err_code": 0
    }
    ```

#### 根据用户名批量获取数字资产

- 获取当前登陆用户的数字资产(layui table格式数据)
```
url: http://${project_url}/v1/assets/layui/current_user
```

返回示例:
```json

```

- 用户名获取资产信息
```
url: http://${project_url}/v1/assets?user={user}
请求方式: GET
参数说明:user,string,用户公钥的md5值
```
#### 条件检索

- 通过文件md5搜索
```
url: http://${project_url}/v1/assets/layui/md5={md5}
请求方式: GET
参数说明:md5,string,原文件md5
```

- 通过时间戳搜索
```
url: http://${project_url}/v1/assets/layui/timestamp={timestamp}
请求方式: GET
参数说明:timestamp,string,时间戳

```

- 通过文件名搜索
```
url: http://${project_url}/v1/assets/layui/file_name={file_name}
请求方式: GET
参数说明:file_name,string,文件名

```

## 数字资产特征文件下载
url: http://${project_url}/v1/assets/feature/file/{id}
请求方式: GET
参数: id 为区块的id
```
