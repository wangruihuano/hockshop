# 监控服务接口
-------

## 获取已经监控的主机ip

```
   url: http://${project_url}/monitor/server-ips
   请求方式: GET/POST
```
response:(eg)

```json
{
	"description": ["192.168.0.104", "192.168.0.103", "192.168.0.167", "192.168.0.138"],
	"err_code": 0
}
```

## 通过ip获取监控信息

```
   url: http://${project_url}/monitor/node?ip={ip}
   request method: GET/POST
   param:ip,String
```
response:(eg)
```json
{
	"description": {
		"cpu": [{
			"usage_rate": 3.261844573106092E-4
		}, {
			"usage_rate": 2.4459845087647776E-4
		}],
		"hardDisk": {
			"avail": 47,
			"free": 47,
			"total": 49,
			"used": 2
		},
		"hostname": "hadoop.master",
		"ip": "192.168.0.104",
		"memory": {
			"free": 1534888,
			"total": 1848348,
			"used": 313460
		},
		"serverStatus": "work"
	},
	"err_code": 0
}
```