# 文件相关接口
-----

## 文件上传

```
url: http://${project_url}/file
请求方式: POST/FORM
参数：upload_file,类型为file
```
正确情况下err_code为0,description为文件的md5值。
```json用户服务接口.
{
    "description": "7f3434f87074af2f04dd6cb5798e4382",
    "err_code": 0
}
```
失败情况示例：
```json
{
    "description": "server exception",
    "err_code": 1005
}
```

## 文件下载

```
url: http://${project_url}/file/{md5}
请求方式: GET
参数说明:{md5}:要下载的文件md5值
```