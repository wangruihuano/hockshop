# 特征提取功能接口
-------

## 获取能够处理的数字资产类型

```
url: http://${project_url}/function/type/{type}
请求方式: POST/GET
参数说明：type,类型为enum,为基础数据类型,目前包括doc,img,voice,video和other
```

请求成功返回示例,description中为Map类型，其中键为类型,值为处理该类型的类的全类名,在之后的信息提取请求中将会作为参数:

```json
{
"description": {
    "txt": "edu.ycu.hockshop.marmot.media.Text",
    "docx": "edu.ycu.hockshop.marmot.media.Docx"
},
"err_code": 0
}
```

## 获取可提取的信息特征

```
url: http://${project_url}/function/feature
请求方式: POST
参数说明：classname,类型为String,参见"获取能够处理的数字资产类型"接口
```

返回示例:
```json
{
    "description": [
        "content",
        "length"
    ],
    "err_code": 0
}
```
  
## 提取文件信息

```
url: http://${project_url}/function/extract
请求方式: POST/GET
```
|参数名|类型|是否必须|备注|
|----|---|----|----|
|md5|String|是||
|classname|String|是|在请求获取能够处理的数字资产类型中返回的值|
|features|String[]|是|参见上述"获取可提取的信息特征"接口|

请求示例： `http://127.0.0.1:8080/function/extract?md5=a14f3b360eed66d164b0f9bacfbe8f4c&classname=edu.ycu.hockshop.marmot.media.Text&features=content&features=length`
返回示例：
```json
{
	"description": {
		"content": "恭喜你,你申请的QQ号码是:1344151646\n",
		"length": 25
	},
	"err_code": 0
}
```