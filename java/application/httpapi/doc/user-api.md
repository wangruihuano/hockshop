# 用户及权限相关接口
------

## 注册

```
url: http://${project_url}/user/registe
请求方式: POST/GET
```
请求之后会下载一个key文件，之后的登录及认证均使用该文件.

## 登录


  
### 请求密文

```
url: http://${project_url}/user/check
请求方式: POST
参数：publicKey,类型为String,该参数为用户的公钥
```
返回说明：
公钥验证成功(description为密文)：
```json
{
"description": "qDr8hPm6UL3dVpzA5Acx1OtWyG+riWJVcFp9JEiktQ9t1x2MvE2bzSCGZoIAKVJPDGryiIEcgVwtjhskxXmI5v46bwULWEt5sG4spnR3K6S1TXiUjVMzKAz1QO3dF4jFI+cx04o3XqLIWQVEZ4EKIDqPeNyn6bl3crglb0Fp+Ys=",
"err_code": 0
}
```

### 使用密文登陆
```
url: http://${project_url}/user/login
请求方式: POST
参数:uuid,类型为字符串,该参数为解密后的密文
```
返回说明：
登录成功(description为用户唯一标志)：
```json
{
    "description": "d9264ba044a7d939d3c0dcb960e77db7",
    "err_code": 0
}
```
