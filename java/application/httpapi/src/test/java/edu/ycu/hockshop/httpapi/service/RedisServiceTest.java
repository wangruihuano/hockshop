package edu.ycu.hockshop.httpapi.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RedisServiceTest {

    Logger logger = Logger.getLogger(RedisServiceTest.class);

    @Autowired
    RedisService redisService;

    @Test
    public void setTest(){
        try{
            redisService.set("test","123456");
        }catch (Exception e){
            logger.debug("redis插入数据异常",e);
        }
    }

    @Test
    public void getTest(){
        try {
            String value= redisService.get("test",String.class);
            System.out.println(value);
        }catch (Exception e){
            logger.debug("redis获取数据异常",e);
        }
    }
}
