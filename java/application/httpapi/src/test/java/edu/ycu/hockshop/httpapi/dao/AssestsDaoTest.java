package edu.ycu.hockshop.httpapi.dao;

import edu.ycu.hockshop.common.model.DigitizedAssests;
import edu.ycu.hockshop.httpapi.Application;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 数字资产数据持久测试.
 * @author wangruihuan
 * @since 2018-05-07
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class AssestsDaoTest {

    private static final Logger logger = Logger.getLogger(AssestsDaoTest.class);
    private static final String test_id = "testid1";
    @Autowired AssestsDao assestsDao;

    @Test
    public void insertTest(){
        DigitizedAssests assests = null;
        try{
            assests = new DigitizedAssests();
            assests.setId(test_id);
            assests.setMd5("testmd5");
            assests.setTimestamp(System.currentTimeMillis());
            assests.setUser("testuser1");
            for (int i=0;i<50;i++){
                assests.setTimestamp(System.currentTimeMillis());
                System.out.println(assestsDao.addAssests(assests));
            }
        }catch (Exception e){
            logger.debug("添加测试异常:"+assests.toString(),e);
        }

    }

    @Test
    public void getByIdTest(){
        String id = test_id;
        try {
            DigitizedAssests assests=assestsDao.getById(id);
            System.out.println(assests);
        }catch (Exception e){
            logger.debug("通过id获取异常:"+id,e);
        }
    }

    @Test
    public void countTest(){
        try {
            Integer i=assestsDao.getCount();
            System.out.println(i);
        }catch (Exception e){
            logger.debug("获取数量异常",e);
        }
    }

    @Test
    public void getByUserTest(){
        try {
            List<DigitizedAssests> assests=assestsDao.getByUser("testuser1");
            System.out.println(assests.size());
        }catch (Exception e){
            logger.debug("获取数量异常",e);
        }
    }

    @Test
    public void getPageTest(){
        try {
            List<DigitizedAssests> assests=assestsDao.getPageAssests(20,50);
            System.out.println(assests.size());
        }catch (Exception e){
            logger.debug("获取数量异常",e);
        }
    }
}
