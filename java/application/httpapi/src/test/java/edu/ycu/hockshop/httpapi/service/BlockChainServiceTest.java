package edu.ycu.hockshop.httpapi.service;

import edu.ycu.hockshop.common.model.DigitizedAssests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 区块链服务测试.
 * @author wangruihuan
 * @since 2018-05-21
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class BlockChainServiceTest {

    Logger logger = LoggerFactory.getLogger(BlockChainServiceTest.class);

    @Autowired BlockChainService service;
    @Test
    public void uploadTest(){
        try {
            DigitizedAssests assests = new DigitizedAssests();
            for (int i=0;i<1000;i++){
                assests.setInfo("info");
                assests.setUser("user");
                assests.setMd5("md5");
                assests.setTimestamp(System.currentTimeMillis());
                service.upLoadAssests(assests);
            }
        }catch (Exception e){
            logger.debug("区块上传异常",e);
        }

    }
}
