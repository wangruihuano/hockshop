package edu.ycu.hockshop.httpapi.other;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.common.model.DigitizedAssests;
import edu.ycu.hockshop.httpapi.constant.ErrMsg;
import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import org.junit.Test;

/**
 * json序列化测试.
 * @author wrh
 * @since 2018-04-15
 */
public class JsonTest {

    @Test
    public void asstsTest(){
        DigitizedAssests[] assests=new DigitizedAssests[3];
        assests[0]=new DigitizedAssests();
        assests[0].setMd5("0md5");
        assests[0].setInfo("0info");
        assests[0].setOprate_code(DigitizedAssests.OprateCode.ADD);
        assests[0].setUser("0user");

        assests[1]=new DigitizedAssests();
        assests[1].setMd5("1md5");
        assests[1].setInfo("1info");
        assests[1].setOprate_code(DigitizedAssests.OprateCode.ADD);
        assests[1].setUser("1user");

        assests[2]=new DigitizedAssests();
        assests[2].setMd5("2md5");
        assests[2].setInfo("2info");
        assests[2].setOprate_code(DigitizedAssests.OprateCode.ADD);
        assests[2].setUser("2user");

        ErrorEnum errorEnum=ErrorEnum.SUCCESS;
        errorEnum.setDescription(assests);

        ErrMsg msg=new ErrMsg(errorEnum.getCode(),errorEnum.getDescription());

        System.out.println(JSON.toJSONString(msg));

    }
}
