package edu.ycu.hockshop.httpapi.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.blockchainapi.BlockChain;
import edu.ycu.hockshop.common.model.DigitizedAssests;
import edu.ycu.hockshop.common.model.netty.NettyRequestMsg;
import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.dfsapi.DFS;
import edu.ycu.hockshop.httpapi.dao.AssestsDao;
import edu.ycu.hockshop.httpapi.service.BlockChainService;
import edu.ycu.hockshop.httpapi.service.FileService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static edu.ycu.hockshop.common.Constant.REDUCE_FIEL_PATH;

/**
 * 区块莲服务实现类.
 *
 * @author wrh
 * @since 2018-04-14
 */
@Service
public class BlockChainServiceImpl implements BlockChainService {

    private static final Logger logger = Logger.getLogger(BlockChainServiceImpl.class);

//    private
    @Reference(version = "${demo.service.version}",
        application = "${dubbo.application.id}")
    BlockChain blockChain;

    @Autowired
    AssestsDao assestsDao;
    @Autowired
    FileService fileService;


    @Value("${appcfg.rmp.dir}")
    String tmpDir;

    @Override
    public String upLoadAssests(DigitizedAssests assests) {

        //将数字资产特征相关信息列化之后写入文件并上传至分布式文件系统.
        try {
            File featureFile = new File(tmpDir+"/"+assests.getMd5());
            if (featureFile.exists()){
                featureFile.delete();
            }
            featureFile.createNewFile();
            DigitizedAssests fileAssets = new DigitizedAssests();
            fileAssets.setInfo(assests.getInfo());
            fileAssets.setOther(assests.getOther());
            FileUtils.writeStr2File(featureFile, JSON.toJSONString(fileAssets));
            fileService.uploadFile(featureFile,REDUCE_FIEL_PATH);
        }catch (IOException e){
            logger.warn("存储特征文件异常",e);
            return null;
        }

        //设置数字资产id
        assests.setId(UUID.randomUUID().toString().replaceAll("-", ""));

        //将不上链的信息置为null
        assests.setOther(null);

        try {
            //上传至区块网络
            boolean sucess=blockChain.pushAssetsInfo(assests);

            if (sucess) {
                //数据库备份
                String id = save2DataBase(assests);
                return assests.getId();
            }
        } catch (Exception e) {
            logger.warn("上传至区块网络异常", e);
        }

        return null;
    }

    /**
     * 存入数据库.
     * @param assests
     * @return 若成功返回数字资产id.否则返回null.
     */
    private String save2DataBase(DigitizedAssests assests) {
        Integer success = assestsDao.addAssests(assests);
        if (success == 1) {
            return assests.getId();
        }
        return null;
    }

    /**
     * ${@inheritDoc}
     * @param id
     * @return 若存在返回数字资产,否则返回null.
     */
    @Override
    public DigitizedAssests getAssests(String id) {

        NettyRequestMsg request = new NettyRequestMsg();
        request.setCode(NettyRequestMsg.NettyRequestCode.GET);
        request.setContent(id);

        DigitizedAssests assests = null;
        try {
            //从区块网络中获取数据
            assests=blockChain.pullAssetsInfo(id);
            //TODO 待添加数据库缓存
            return assests;
        } catch (Exception e) {
            logger.warn("从区块网络获取数据异常：id=" + id, e);
        }
        return null;
    }

    @Override
    public List<DigitizedAssests> getAssestsByUser(String user) {
        //TODO 待添加与区块中数据验证
        List<DigitizedAssests> dbass = assestsDao.getByUser(user);
        return parseFromDB(dbass);
    }

    @Override
    public List<DigitizedAssests> getAssestsByFileMD5(String md5) {
        List<DigitizedAssests> dbass = assestsDao.getByFileMD5(md5);
        return parseFromDB(dbass);
    }

    @Override
    public List<DigitizedAssests> getAssestsByTimestamp(String timestamp) {
        List<DigitizedAssests> dbass = assestsDao.getByTimestamp(timestamp);
        return parseFromDB(dbass);
    }

    @Override
    public List<DigitizedAssests> getAssestsByFileName(String file_name) {
        List<DigitizedAssests> dbass = assestsDao.getByFileName(file_name);
        return parseFromDB(dbass);
    }


    //TODO
    @Override
    public DigitizedAssests[] getAssests(String first_id, int num) {
        return null;
    }

    @Override
    public List<DigitizedAssests> getPageByIndex(int pageSize, int begin) {
        List dbAss = assestsDao.getPageAssests(begin, pageSize);
        return parseFromDB(dbAss);
    }

    @Override
    public int getCount() {
        return assestsDao.getCount();
    }

    /**
     * 通过数据库的资产id从区块服务中获取详细资产信息.
     *
     * @param dbass
     * @return
     */
    private List<DigitizedAssests> parseFromDB(List<DigitizedAssests> dbass) {

        List res = new ArrayList();
        for (DigitizedAssests assests : dbass) {
            if (assests!=null){
                DigitizedAssests blockAss =getAssests(assests.getId());
                if (blockAss!=null){
                    res.add(blockAss);
                }
            }
        }
        if (res.size()==0){
            return null;
        }
        return res;
    }

    @Override
    public boolean delete(String id) throws Exception {
        int res=assestsDao.delete(id);
        logger.info("数据库清理数据:"+id+",执行结果"+res);
        return blockChain.cancelAssetsInfo(id);
    }
}
