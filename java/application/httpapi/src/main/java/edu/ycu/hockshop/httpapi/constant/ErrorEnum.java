package edu.ycu.hockshop.httpapi.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * 错误状态码枚举类型.
 */
public enum  ErrorEnum {

    //请求成功
    SUCCESS("",0),

    //服务异常
    SERVER_ERR("server exception",1005),

    //公钥不合法
    INVALID_PUBLIC_KEY("",3001),

    //私钥不匹配
    UNMATCHED_PRIVATE_KEY("Unmatched private key",3002),

    PARAMS_ERROR("The parameter is illegal",3003),

    //权限错误
    PERMISSION_DENIED("Permission denied",1001),

    //用户不存在
    USER_NOT_EXITS("user not exits",1002),

    //文件损坏
    FILE_ERR("damaged file",1003),

    //文件不存在
    FILE_NOT_EXIT("file not exits",1004),

    //区块上传失败
    BLOCK_UP_LOAD_FAIL("block up load fail",2001),

    //该用户没有资产
    NO_ASSESTS_FOR_USER("there is no assets for request user",2004),

    //区块已经存在
    BLOCK_EXITED("block exited",2002),

    BLOCK_NOT_EXITED("block not exits",2003),

    //不存在能够处理该文件类型对应的方法
    TYPE_NOT_EXIST("type not exist",4001),

    //提取信息错误
    EXTRACT_ERROR("extract error",4002),

    //区块中数字资产对应文件不存在
    FILE_INBLOCK_NOT_EXIT("file in block does not exist",2004),


    ERVER_MONITOR_DOWN("server monitor is not work",5001),

    //监控服务未启动
    NO_CONFIG_SERVER("there is no server config monitor",5002),

    //区块链服务故障
    BLOCK_SERVER_EXCEPTION("block chain server failure",5003),

    ;

    @Setter @Getter private Object description;
    @Getter private int code;

    ErrorEnum(Object description, int code) {
        this.description = description;
        this.code = code;
    }

}
