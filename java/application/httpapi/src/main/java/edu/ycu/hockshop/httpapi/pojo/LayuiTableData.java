package edu.ycu.hockshop.httpapi.pojo;

import edu.ycu.hockshop.common.model.DigitizedAssests;
import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LayuiTableData {

    private int code;
    private String msg;
    private int count;
    private List<DigitizedAssests> data;

    public void setErrEnum(ErrorEnum errEnum){
        this.code = errEnum.getCode();
        this.msg = errEnum.getDescription().toString();
    }
}
