package edu.ycu.hockshop.httpapi.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import edu.ycu.hockshop.httpapi.service.FeaturesService;
import edu.ycu.hockshop.httpapi.service.FileService;
import edu.ycu.hockshop.marmotapi.Marmot;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static edu.ycu.hockshop.common.Constant.DFS_FILE_PATH;

/**
 * 特征提取相关业务实现类.
 * @author wangruihuan
 * @see edu.ycu.hockshop.httpapi.service.FeaturesService
 *
 */
@Service
public class FeaturesServiceImpl implements FeaturesService {

    private static final Logger logger = Logger.getLogger(FeaturesServiceImpl.class);

    /**
     * 信息抽取核心类.
     * @return
     */
    @Reference(version = "${demo.service.version}",
            application = "${dubbo.application.id}")
    public Marmot marmot;

    @Autowired
    FileService fileService;


    /**
     * {@inheritDoc}
     * @param firstType 第一类型名称
     * @return
     */
    @Override
    public Map<String, String> getSecondTypeByFirst(String firstType) {
        Map info = marmot.getTypeInfo();
        return (Map) info.get(firstType);
    }


    /**
     * 提取信息并将信息保存至分布式文件系统中.
     * @param className 全类名
     * @param md5 文件md5
     * @param features 需要抽取的特征
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public Object exteactInfo(String className, String md5, String[] features) throws IOException, ClassNotFoundException {
        return marmot.doExtract(className,fileService.downloadFile(md5,DFS_FILE_PATH),features);
    }

    /**
     * {@inheritDoc}
     * @param className 全类名
     * @return
     * @throws ClassNotFoundException
     */
    @Override
    public String[] getFeatures(String className) throws Exception{
        return marmot.getFeaturesByTypeName(className);
    }
}
