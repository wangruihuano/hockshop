package edu.ycu.hockshop.httpapi.service;

import edu.ycu.hockshop.common.util.encrypt.KeyPair;

import javax.servlet.http.HttpSession;

/**
 * 用户服务接口.
 * @author wrh
 * @since 2018-04-16
 */
public interface UserService {


    /**
     * 注册。
     * @return 返回密钥对.
     */
    KeyPair registe();

    //TODO 此方法有待完善.
    /**
     * 检查公钥.若公钥存在,session中写入随机数及公钥的md5值.
     * 在此应用中，公钥的md5作为用户唯一标志.
     * @param pubicKey 公钥
     * @return 公钥存在返回公钥加密的随机数,否则返回null.
     */
    String checkpubicKey( String pubicKey, HttpSession userSession);

    /**
     * 登陆.
     * @param random 客户端解密后的随机数
     * @return 若校验成功，返回用户id,否则返回null.
     */
    String  login( String random, HttpSession userSession);


}
