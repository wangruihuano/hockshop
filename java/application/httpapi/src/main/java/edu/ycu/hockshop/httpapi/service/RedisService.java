package edu.ycu.hockshop.httpapi.service;

/**
 * Redis服务接口.
 * @author wrh
 * @since 2018-04-17
 */
public interface RedisService {

    void set(String key, Object value);

    <T> T get(String key, Class<T> clazz);
}
