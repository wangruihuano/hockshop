package edu.ycu.hockshop.httpapi.pojo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * User实体类.
 * @since 2018-05-01
 * @author wrh
 */
@Getter
@Setter
public class User {

    //用户id
    private String uid;

    //资产id
//    private List<String> assestsId;

    //用户公钥
    private String pubkey;

}
