package edu.ycu.hockshop.httpapi.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import edu.ycu.hockshop.blockchainapi.BlockChain;
import edu.ycu.hockshop.common.model.DigitizedAssests;
import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller()

public class TestController {
    @Reference(version = "${demo.service.version}",
            application = "${dubbo.application.id}")
    BlockChain blockChain;

    @RequestMapping("/test")
    @ResponseBody
    public String test(){
        ErrorEnum msg=ErrorEnum.SUCCESS;
        DigitizedAssests digitizedAssests = new DigitizedAssests();
        digitizedAssests.setUser("licte");
        digitizedAssests.setId("oTO"+Math.random()+"OTo");
        digitizedAssests.setInfo("这是一条有格调测试数据！");
        digitizedAssests.setMd5("这是一条有格调测试数据！");
        msg.setDescription(digitizedAssests.getId());
        try {
            Boolean info = blockChain.pushAssetsInfo(digitizedAssests);
            return msg.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ErrorEnum.BLOCK_UP_LOAD_FAIL.toString();
    }
}
