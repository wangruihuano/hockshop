package edu.ycu.hockshop.httpapi;

import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import edu.ycu.hockshop.httpapi.controller.BaseController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static edu.ycu.hockshop.httpapi.constant.Tag.USER_LOGIN_CHECK_TAG;
import static edu.ycu.hockshop.httpapi.constant.Tag.USER_SESSION_ID_TAG;

/**
 *
 *  请求过滤器。
 *  响应头设置,解决跨域问题.
 *  权限检查也在此类中设置.
 * @author wrh
 * @since 2018-04-17
 */
@Component
public class CorsFilter extends BaseController implements Filter {

    private Logger logger = Logger.getLogger(CorsFilter.class);

    @Value(value = "${appcfg.checkPermission}")
    private boolean checkPermisson;

    @Value("#{'${appcfg.checkUrl}'.split(',')}")
    private List<String > checkUrl;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
//        response.setHeader("Connection","Keep-Alive");
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
//        response.setHeader("Access-Control-Max-Age", "3600");
//        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
//        response.setContentType("textml;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "0");
        response.setHeader("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With,userId,token");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("XDomainRequestAllowed","1");


        //权限校验
        if (checkPermisson){
            String reqUrl = request.getRequestURI();
           logger.debug("getRequestURI"+reqUrl);
           if (checkUrl.indexOf(reqUrl)<0){
               boolean check=false;
               Object attr=request.getSession().getAttribute(USER_LOGIN_CHECK_TAG);
                if (attr!=null){
                    check = (boolean) attr;
                }
               if (!check){
                   writeErrMsg(response,ErrorEnum.PERMISSION_DENIED);
                   return;
               }
           }
        }

        chain.doFilter(req, res);
    }

    @Override
    public void destroy() {

    }

}

