package edu.ycu.hockshop.httpapi.controller;

import edu.ycu.hockshop.common.util.encrypt.MD5;
import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import edu.ycu.hockshop.httpapi.constant.ResetFulAPI;
import edu.ycu.hockshop.httpapi.service.FileService;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import static edu.ycu.hockshop.common.Constant.DFS_FILE_PATH;
import static edu.ycu.hockshop.common.Constant.REDUCE_FIEL_PATH;

/**
 * 文件控制层.
 */
@Controller
@RequestMapping(ResetFulAPI.UPLOAD_FILE)
public class FileController extends BaseController {

    private Logger logger = Logger.getLogger(FileController.class);

    @Autowired
    FileService fileService;


    /**
     * 文件下载.
     *
     * @param md5      文件md5值.
     * @param response
     * @return
     * @since 2018-4-14
     */
    @RequestMapping(value = "/{md5}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> downloadFile(@PathVariable("md5") String md5, HttpServletResponse response) {
        return download(md5,DFS_FILE_PATH);
    }

    /**
     * 文件上传.
     *
     * @param upload_file
     * @param response
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void uploadFile(@RequestParam("upload_file") MultipartFile upload_file, HttpServletResponse response) {
        File file = new File(UUID.randomUUID().toString());
        ErrorEnum err = ErrorEnum.SERVER_ERR;
        try {
            if (!file.exists()){
                file.createNewFile();
            }
            IOUtils.copy(upload_file.getInputStream(),new FileOutputStream(file));
//            upload_file.transferTo(file);
            logger.info("上传文件绝对路径:" + file.getAbsolutePath()+",文件大小:"+file.length()/1024+"KB");
            String fileMD5 = MD5.fileMD5(file);
            file.renameTo(new File(fileMD5));
            String res = fileService.uploadFile(new File(fileMD5), DFS_FILE_PATH);
            if (res != null) {
                err = ErrorEnum.SUCCESS;
                err.setDescription(res);
            }
        } catch (IOException e) {
            logger.warn(e.getMessage());
            err = ErrorEnum.FILE_ERR;
        }
        writeErrMsg(response, err);
    }

    /**
     * 特征文件下载.
     * @param md5 原文件md5
     * @return
     */
    @RequestMapping(value = "/feature/{md5}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> featureFile(@PathVariable("md5") String md5){
        return download(md5,REDUCE_FIEL_PATH);
    }

    private ResponseEntity<byte[]> download(String md5,String path){
        try {
            byte[] file = fileService.downloadFile(md5,path);
            ResponseEntity<byte[]> entity = generateByteEntity(file,md5);
            return entity;
        } catch (Exception e){
            return null;
        }
    }
}
