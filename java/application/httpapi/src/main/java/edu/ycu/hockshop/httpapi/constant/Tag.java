package edu.ycu.hockshop.httpapi.constant;

public interface Tag {

    String USER_SESSION_ID_TAG = "uid";
    String RANDOM_SESSION_TAG = "random";
    String USER_LOGIN_CHECK_TAG = "check";
}
