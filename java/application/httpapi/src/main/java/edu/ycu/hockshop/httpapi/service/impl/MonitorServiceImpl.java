package edu.ycu.hockshop.httpapi.service.impl;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.httpapi.service.MonitorService;
import edu.ycu.monitor.BaseServerNode;
import edu.ycu.monitor.MonitorClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MonitorServiceImpl implements MonitorService {

    @Autowired
    MonitorClient client;

    @Value("#{'${appcfg.serverIP}'.split(',')}")
    private List<String > checkUrl;

    @Override
    public BaseServerNode getByIP(String ip) {

        if (!checkUrl.contains(ip)){
            return null;
        }

        BaseServerNode node = null;
        String info= client.request(ip);
        if (info!=null){
            node=JSON.parseObject(info,BaseServerNode.class);
            node.setIp(ip);
        }
        return node;
    }

    @Override
    public List<String> getServerIpList() {
        return checkUrl;
    }
}
