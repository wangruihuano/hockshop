package edu.ycu.hockshop.httpapi.exception;

/**
 * 区块网络异常.
 * @author wangruihuan
 * @since 2018-05-07
 */
public class BlockChainException extends Throwable {

    public BlockChainException() {
        super("BlockChainException");
    }
}
