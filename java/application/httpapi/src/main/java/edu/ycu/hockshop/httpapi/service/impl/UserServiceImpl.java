package edu.ycu.hockshop.httpapi.service.impl;

import edu.ycu.hockshop.common.util.encrypt.KeyPair;
import edu.ycu.hockshop.common.util.encrypt.MD5;
import edu.ycu.hockshop.common.util.encrypt.RSAUtils;
import edu.ycu.hockshop.httpapi.pojo.User;
import edu.ycu.hockshop.httpapi.service.RedisService;
import edu.ycu.hockshop.httpapi.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.UUID;

import static edu.ycu.hockshop.httpapi.constant.Tag.RANDOM_SESSION_TAG;
import static edu.ycu.hockshop.httpapi.constant.Tag.USER_LOGIN_CHECK_TAG;
import static edu.ycu.hockshop.httpapi.constant.Tag.USER_SESSION_ID_TAG;

/**
 * 用户服务实现类.
 */
@Service
public class UserServiceImpl implements UserService {

    private Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    RedisService redisService;


    @Override
    public KeyPair registe() {
        KeyPair keyPair = new KeyPair(RSAUtils.createKeys(1024));
        //redis服务中注册公钥md5及公钥
        User user = new User();
        user.setUid(MD5.encode(keyPair.getPublicKey()));
        user.setPubkey(keyPair.getPublicKey());
        redisService.set(user.getUid(),user);
        return keyPair;
    }

    @Override
    public String checkpubicKey( String pubicKey, HttpSession userSession) {
        //检查公钥是否存在
        User user= redisService.get(MD5.encode(pubicKey),User.class);
        //若用户不存在或公钥不一致,返回null
        if (user==null || (!user.getPubkey().equals(pubicKey))){
            return null;
        }

        //加密后的字符串
        String encryptSty = null;

        //用户session中写入id
        userSession.setAttribute(USER_SESSION_ID_TAG,MD5.encode(pubicKey));

        //生成随机字符串并记入session
        String randomStr = UUID.randomUUID().toString();
        userSession.setAttribute(RANDOM_SESSION_TAG,randomStr);

        try {
            //公钥加密随机字符串
            encryptSty = RSAUtils.encryptByPublicKey(randomStr,pubicKey);
        } catch (Exception e) {
           logger.warn("加密出错",e);
        }
        return encryptSty;
    }

    @Override
    public String login(String random, HttpSession userSession) {
        //字符串校验
        if (random!=null && random.trim()!=null && !random.equals("")){
            String randomStr = (String) userSession.getAttribute(RANDOM_SESSION_TAG);
            if (randomStr!=null && random.equals(randomStr)){
                userSession.setAttribute(USER_LOGIN_CHECK_TAG,true);
                return (String) userSession.getAttribute(USER_SESSION_ID_TAG);
            }
        }
        return null;
    }
}
