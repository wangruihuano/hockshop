package edu.ycu.hockshop.httpapi;

import edu.ycu.monitor.MonitorClient;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 应用配置.
 * @author wrh
 * @since 2018-04-17
 */
@Configuration
public class AppConfig {

    private static final Logger logger = Logger.getLogger(AppConfig.class);

    public static void main(String[] args) {
//        io.netty.util.internal.ReflectionUtil.trySetAccessible
//        io.netty.util.internal.ReflectionUtil.trySetAccessible
//        io.netty.util.ResourceLeakDetector.addExclusions
    }
    /**
     * Monitor client ,to fetch server info.
     * @return
     */
    @Bean
    public MonitorClient client(){
        return new MonitorClient();
    }

}