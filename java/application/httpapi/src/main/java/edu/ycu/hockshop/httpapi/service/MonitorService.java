package edu.ycu.hockshop.httpapi.service;

import edu.ycu.monitor.BaseServerNode;

import java.util.List;

/**
 * 监控服务.
 * @author wrh
 */
public interface MonitorService {

    /**
     * 根据ip获取节点信息.
     * @param ip 主机ip
     * @return 返回主机信息
     * @see BaseServerNode
     */
    BaseServerNode getByIP(String ip);

    /**
     * 获取已经部署监控服务的主机ip列表.
     * @return
     */
    List<String> getServerIpList();
}
