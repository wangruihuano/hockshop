package edu.ycu.hockshop.httpapi.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 错误消息实体类型.
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ErrMsg {

    private int err_code;
    private Object description;
}
