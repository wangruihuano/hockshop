package edu.ycu.hockshop.httpapi.service.impl;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.httpapi.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

/**
 * Redis服务实现类.
 * @author wrh
 * @since 2018-04-17
 */
@Service
public class RedisServiceImpl implements RedisService{

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, JSON.toJSONString(value));
    }

    public <T> T get(String key, Class<T> clazz) {
        String res = redisTemplate.opsForValue().get(key);
        return JSON.parseObject(res,clazz) ;
    }



}
