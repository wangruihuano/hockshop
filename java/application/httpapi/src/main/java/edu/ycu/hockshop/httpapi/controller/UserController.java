package edu.ycu.hockshop.httpapi.controller;

import edu.ycu.hockshop.common.util.encrypt.KeyPair;
import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import edu.ycu.hockshop.httpapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 用户控制器.
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    UserService userService;

    @RequestMapping("/registe")
    public ResponseEntity<byte[]> registe(){
        KeyPair keyPair=userService.registe();
        if (keyPair!=null){
            return generateByteEntity(keyPair.toString().getBytes(),"key");
        }
        return null;
    }

    @RequestMapping(value = "/check",method = RequestMethod.POST)
    public void check(@RequestParam(name = "publicKey") String publicKey, HttpServletResponse response, HttpSession session){
        ErrorEnum errorEnum = ErrorEnum.INVALID_PUBLIC_KEY;
        if (publicKey!=null && !publicKey.trim().equals("")){
            String entryStr = userService.checkpubicKey(publicKey, session);
            if (entryStr!=null){
                errorEnum = ErrorEnum.SUCCESS;
                errorEnum.setDescription(entryStr);
            }
        }
        writeErrMsg(response,errorEnum);
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public void login(@RequestParam(name = "uuid") String uuid, HttpServletResponse response, HttpSession session){
        ErrorEnum errorEnum=ErrorEnum.UNMATCHED_PRIVATE_KEY;
        String userId=userService.login(uuid,session);
        if (userId!=null){
            errorEnum = ErrorEnum.SUCCESS;
            errorEnum.setDescription(userId);
        }
        writeErrMsg(response,errorEnum);
    }
}
