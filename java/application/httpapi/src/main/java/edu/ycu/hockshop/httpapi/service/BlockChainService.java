package edu.ycu.hockshop.httpapi.service;

import edu.ycu.hockshop.common.model.DigitizedAssests;
import edu.ycu.hockshop.httpapi.exception.BlockChainException;

import java.util.List;

/**
 * 区块链服务接口.
 * 包含数字资产的上传,获取相关方法声明.
 * @see DigitizedAssests 数字资产模型
 * @author wrh
 * @since 2018-03-27
 */
public interface BlockChainService {

    /**
     * 上传区块.
     * @param assests 数字资产实体类.
     * @return 上传成功成功返回区块id,否则返回null.
     * @see DigitizedAssests
     */
    String  upLoadAssests(DigitizedAssests assests);

    /**
     * 通过id获取数字资产.
     * @param id
     * @return
     */
    DigitizedAssests getAssests(String id);

    /**
     * 通过所属者获取数字资产.
     * @param user
     * @return
     */
    List<DigitizedAssests> getAssestsByUser(String user);

    /**
     * 通过文件md5获取数字资产信息
     * @param md5 文件md5
     * @return
     */
    List<DigitizedAssests> getAssestsByFileMD5(String md5);

    /**
     * 通过时间戳获取数字资产信息
     * @param timestamp 时间戳
     * @return
     */
    List<DigitizedAssests> getAssestsByTimestamp(String timestamp);

    List<DigitizedAssests> getAssestsByFileName(String file_name);

    /**
     * 获取一组数字资产.
     * @param first_id 第一个块的id.当此参数为空时,默认从第一个区块开始获取.
     * @param num 获取的数量.
     * @return
     */
    DigitizedAssests[] getAssests(String first_id,int num);

    /**
     * 获取一页数字资产信息.
     * @param pageSize
     * @param begin
     * @return
     */
    List<DigitizedAssests> getPageByIndex(int pageSize,int begin);

    /**
     * 获取数字资产数量.
     * @return
     */
    int getCount();

    boolean delete(String id) throws Exception;
}
