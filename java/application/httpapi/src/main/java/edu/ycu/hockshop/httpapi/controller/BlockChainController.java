package edu.ycu.hockshop.httpapi.controller;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.common.model.DigitizedAssests;
import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import edu.ycu.hockshop.httpapi.constant.ResetFulAPI;
import edu.ycu.hockshop.httpapi.constant.Tag;
import edu.ycu.hockshop.httpapi.pojo.LayuiTableData;
import edu.ycu.hockshop.httpapi.service.BlockChainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 区块莲服务控制器。
 *
 * @author wrh
 * @since 2018-04-15
 */
@RequestMapping(ResetFulAPI.ASSETS)
@Controller
public class BlockChainController extends BaseController {

    @Autowired
    BlockChainService blockChainService;
    @Value("${appcfg.blockNumLimit}")
    int blockNumLimit;

    /**
     * 区块上传.
     *
     * @param json  数字资产JSON信息
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String uploadBlock(@RequestBody String json, HttpSession session) {
        DigitizedAssests assests = null;
        if (json!=null && !json.trim().equals("")){
            assests = JSON.parseObject(json,DigitizedAssests.class);
        }
        if (assests != null) {
            String blockId = null;
                //填充资产其余信息
                 String user = (String) session.getAttribute(Tag.USER_SESSION_ID_TAG);
                // 二次校验用户登陆信息
                if (user == null) {
                    return msg(ErrorEnum.PERMISSION_DENIED);
                }
                assests.setUser(user);
                assests.setOprate_code(DigitizedAssests.OprateCode.ADD);
                assests.setTimestamp(System.currentTimeMillis());

                blockId = blockChainService.upLoadAssests(assests);

                //若上传成功，做正常相应
                if (blockId != null) {
                    return successMsg(blockId);
                }
        }
        return msg(ErrorEnum.BLOCK_UP_LOAD_FAIL);
    }

    /**
     * 通过用户名获取数字资产
     *
     * @param user
     * @param response
     */
    @RequestMapping(value = "/user={user}", method = RequestMethod.GET)
    public void getBlocksByUser(@PathVariable String user, HttpServletResponse response) {
        List assests = blockChainService.getAssestsByUser(user);
        ErrorEnum msg = ErrorEnum.NO_ASSESTS_FOR_USER;
        if (assests != null && assests.size() > 0) {
            msg = ErrorEnum.SUCCESS;
            msg.setDescription(assests);
        }
        writeErrMsg(response, msg);
    }

    /**
     * 批量获取数字资产.
     * @apiNote 本接口中参数page默认从第一页开始,不可小于1.
     * @param page     第几页
     * @param response
     */
    @RequestMapping(value = "/page={page}", method = RequestMethod.GET)
    public void getBlocks(@PathVariable int page, HttpServletResponse response) {
        ErrorEnum msg = ErrorEnum.BLOCK_NOT_EXITED;
        page--;
        List res = blockChainService.getPageByIndex(blockNumLimit, page);
        if (res != null && res.size() > 0) {
            msg = ErrorEnum.SUCCESS;
            msg.setDescription(res);
        }
        writeErrMsg(response, msg);
    }

    @RequestMapping("/id={id}")
    public void getById(@PathVariable("id") String id,HttpServletResponse response){
        DigitizedAssests assests= blockChainService.getAssests(id);
        ErrorEnum msg = ErrorEnum.BLOCK_NOT_EXITED;
        if (assests!=null){
            msg = ErrorEnum.SUCCESS;
            msg.setDescription(assests);
        }
        writeErrMsg(response,msg);
    }

    /**
     * 适配至layui表格数据.
     * @param page 第几页
     * @param limit 每页的数量
     * @return
     */
    @RequestMapping("/layui")
    @ResponseBody
    public String  getBlocks2Layui(@RequestParam int page, @RequestParam int limit){
        LayuiTableData data = new LayuiTableData();
        page--;
        List res = blockChainService.getPageByIndex(limit, page*limit);
        if (res != null && res.size() > 0) {

            if (res!=null && res.size()>0){
                data.setCode(0);
                data.setCount(blockChainService.getCount());
                data.setMsg("");
                data.setData(res);
            }

        }else {
            data.setCode(ErrorEnum.BLOCK_NOT_EXITED.getCode());
            data.setMsg(ErrorEnum.BLOCK_NOT_EXITED.getDescription().toString());
        }
        return JSON.toJSONString(data);
    }

    /**
     * 获取当前登陆用户的数字资产.
     * @param session
     * @return
     */
    @RequestMapping("/layui/current_user")
    @ResponseBody
    public String getBlocks2LayuiByUser(HttpSession session){
        LayuiTableData data = new LayuiTableData();
        String user = (String) session.getAttribute(Tag.USER_SESSION_ID_TAG);
        if (user!=null){
            List res=blockChainService.getAssestsByUser(user);
            if (res!=null && res.size()>0){
                data= new LayuiTableData(0,"",res.size(),res);
            }else {
                data.setErrEnum(ErrorEnum.NO_ASSESTS_FOR_USER);
            }
        }else {
            data.setErrEnum(ErrorEnum.PERMISSION_DENIED);
        }
        return JSON.toJSONString(data);
    }

    /**
     * 通过文件md5获取数字资产信息.
     * @param md5
     * @return
     */
    @RequestMapping("/layui/md5={md5}")
    @ResponseBody
    public String getBlocks2LayuiByFileMD5(@PathVariable("md5") String md5){

        List res=blockChainService.getAssestsByFileMD5(md5);
        return layuiTableData(res);
    }

    /**
     * 通过时间戳获取数据,适配至layui.
     * @param timestamp
     * @return
     */
    @RequestMapping("/layui/timestamp={timestamp}")
    @ResponseBody
    public String getBlocks2LayuiByTimestamp(@PathVariable("timestamp") String timestamp){
        List res=blockChainService.getAssestsByTimestamp(timestamp);
        return layuiTableData(res);
    }

    @RequestMapping("/layui/file_name={file_name}")
    @ResponseBody
    public String getBlocks2LayuiByfileName(@PathVariable("file_name") String file_name){
        List res=blockChainService.getAssestsByFileName(file_name);
        return layuiTableData(res);
    }


    /**
     * 适配数据至layui表格.
     * @param res
     * @return
     */
    private String layuiTableData(List res){
        LayuiTableData data = new LayuiTableData();
        if (res!=null && res.size()>0){
            data= new LayuiTableData(0,"",res.size(),res);
        }else {
            data.setErrEnum(ErrorEnum.NO_ASSESTS_FOR_USER);
        }
        return JSON.toJSONString(data);
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") String id){
        ErrorEnum msg = ErrorEnum.BLOCK_NOT_EXITED;
        try {
            if (blockChainService.delete(id)){
                msg = ErrorEnum.SUCCESS;
                msg.setDescription(id);
            }
        } catch (Exception e) {
            logger.warn("删除异常",e);
        }
        return JSON.toJSONString(msg);
    }

}
