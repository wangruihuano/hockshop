package edu.ycu.hockshop.httpapi.controller;

import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import edu.ycu.hockshop.httpapi.service.FeaturesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 功能控制器.
 * @author wrh
 * @since 2018-04-22
 */
@Controller
@RequestMapping("/function")
public class FeaturesContriller extends BaseController{

    @Autowired FeaturesService featuresService;

    /**
     * 获取二级类别.
     * @param type
     * @param response
     */
    @RequestMapping("/type/{type}")
    public void getSecondType(@PathVariable("type") String type, HttpServletResponse response){
        ErrorEnum errorEnum = ErrorEnum.TYPE_NOT_EXIST;
        Map info = featuresService.getSecondTypeByFirst(type);
        if (info!=null && info.size()>0){
            errorEnum = ErrorEnum.SUCCESS;
            errorEnum.setDescription(info);
        }
        writeErrMsg(response,errorEnum);
    }

    /**
     * 获取处理对应文件类型的特征
     * @param className 请求参数的全类名
     * @param response
     */
    @RequestMapping(value = "/feature",method = RequestMethod.POST)
    public void getFeatures(@RequestParam("classname") String className, HttpServletResponse response){

        ErrorEnum errorEnum = ErrorEnum.SERVER_ERR;

        String[] features;
        try {
            features = featuresService.getFeatures(className);
            if (features!=null && features.length>0){
                errorEnum = ErrorEnum.SUCCESS;
                errorEnum.setDescription(features);
            }
        } catch (Exception e) {
            errorEnum = ErrorEnum.TYPE_NOT_EXIST;
        }

        writeErrMsg(response,errorEnum);

    }

    /**
     * 提取信息.
     * @param md5
     * @param classname
     * @param response
     */
    @RequestMapping(value = "/extract")
    public void extractInfo(@RequestParam("md5") String md5,
                            @RequestParam("features") String[] features,
                            @RequestParam("classname") String classname, HttpServletResponse response){
        ErrorEnum errorEnum = ErrorEnum.EXTRACT_ERROR;
        Object info= null;
        try {
            info = featuresService.exteactInfo(classname,md5,features);
            if (info!=null){
                errorEnum = ErrorEnum.SUCCESS;
                errorEnum.setDescription(info);
            }
        } catch (IOException e) {
            errorEnum = ErrorEnum.FILE_ERR;
        } catch (ClassNotFoundException e) {
            errorEnum = ErrorEnum.TYPE_NOT_EXIST;
        }

        writeErrMsg(response,errorEnum);
    }

}
