package edu.ycu.hockshop.httpapi.dao;

import edu.ycu.hockshop.common.model.DigitizedAssests;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 数字资产数据持久至数据库接口.
 * @author wangruihuan
 * @since 2018-05-07
 */
@Mapper
public interface AssestsDao {

    String TABLE_NAME = "`assests`";


    /**
     * 添加数字资产之数据库.
     * @param assests
     * @return
     */
    @Insert("INSERT INTO " +
            TABLE_NAME +
            " (`id`," +
            " `timestamp`," +
            " `md5`," +
            " `user`," +
            " `file_name`)" +
            " VALUES (" +
            " #{assests.id}," +
            " #{assests.timestamp,javaType=java.lang.Long, jdbcType=BIGINT}," +
            " #{assests.md5}," +
            " #{assests.user}," +
            " #{assests.file_name})")
    Integer addAssests(@Param("assests") DigitizedAssests assests);

    /**
     * 通过id获取数字资产.
     * @param id
     * @return
     */
    @Select("select * from " +
            TABLE_NAME  +
            "where id = #{id}")
    DigitizedAssests getById(@Param("id") String id);

    /**
     * 通过user获取数字资产.
     * @param user
     * @return
     */
    @Select("select * from " +
            TABLE_NAME +
            " where user = #{user}")
    List<DigitizedAssests> getByUser(@Param("user") String user);

    /**
     * 获取一页数据.
     * 插入数据的时间顺序逆序排序,即第一页为最后插入的一页.
     * @param begin 索引起点
     * @param end 终点
     * @return
     */
    @Select("select * FROM " +
            TABLE_NAME +
            " order by `index` desc limit #{begin},#{end} ")
    List<DigitizedAssests> getPageAssests(@Param("begin") int begin, @Param("end") int end);

    /**
     * 获取数字资产数量.
     * @return
     */
    @Select("select count(`index`) from " +
            TABLE_NAME)
    Integer getCount();

    /**
     * 通过文件md5获取数字资产
     * @param md5
     * @return
     */
    @Select("select * from " +
            TABLE_NAME +
            " where md5 = #{md5}")
    List<DigitizedAssests> getByFileMD5(@Param("md5")String md5);

    /**
     * 通过时间戳获取数字资产
     * @param timestamp
     * @return
     */
    @Select("select * from " +
            TABLE_NAME +
            " where timestamp = #{timestamp}")
    List<DigitizedAssests> getByTimestamp(@Param("timestamp")String timestamp);

    /**
     * 通过文件名获取数字资产
     * @param file_name
     * @return
     */
    @Select("select * from " +
            TABLE_NAME +
            " where file_name = #{file_name}")
    List<DigitizedAssests> getByFileName(@Param("file_name")String file_name);

    /**
     * 删除
     * @param id
     * @return
     */
    @Delete("delete from "+
    TABLE_NAME +
    "where id = #{id}")
    int delete(@Param("id") String id);
}
