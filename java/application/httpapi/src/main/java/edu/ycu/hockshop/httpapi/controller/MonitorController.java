package edu.ycu.hockshop.httpapi.controller;

import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import edu.ycu.hockshop.httpapi.service.MonitorService;
import edu.ycu.monitor.BaseServerNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 监控信息Controller.
 * @author wrh
 */
@Controller
@RequestMapping("/monitor")
public class MonitorController extends BaseController{

    @Autowired MonitorService monitorService;

    /**
     * 根据ip获取服务其信息.
     * @param ip 主机ip.
     * @param response
     */
    @RequestMapping(value = "/node")
    public void getServerNode(@RequestParam("ip") String ip, HttpServletResponse response){

        ErrorEnum errorEnum = ErrorEnum.ERVER_MONITOR_DOWN;
        BaseServerNode node=monitorService.getByIP(ip);
        if (node!=null){
            errorEnum = ErrorEnum.SUCCESS;
            errorEnum.setDescription(node);
        }
        writeErrMsg(response,errorEnum);
    }

    /**
     * 获取已经部署服务的监控.
     * @param response
     */
    @RequestMapping(value = "/server-ips")
    public void getServerList(HttpServletResponse response){
        ErrorEnum errorEnum = ErrorEnum.NO_CONFIG_SERVER;
        List<String> ips=monitorService.getServerIpList();
        if (ips!=null && ips.size()>0){
            errorEnum = ErrorEnum.SUCCESS;
            errorEnum.setDescription(ips);
        }
        writeErrMsg(response,errorEnum);
    }
}
