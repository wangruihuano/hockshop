package edu.ycu.hockshop.httpapi.controller;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.httpapi.constant.ErrMsg;
import edu.ycu.hockshop.httpapi.constant.ErrorEnum;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * 控制器基类
 * 包含网络IO读写功能
 *
 * @author wrh
 */
public class BaseController {

    Logger logger = Logger.getLogger(BaseController.class);

    /**
     * 响应json字符串
     *
     * @param response
     * @param object   写出的对象
     */
    public final void writeJson(HttpServletResponse response, Object object) {
        String jsonStr = JSON.toJSONString(object);
        writeStr(response, jsonStr);
    }

    /**
     * 写出错误信息.
     *
     * @param response
     * @param error
     * @see ErrorEnum
     * @since 2018-03-19
     */
    public final void writeErrMsg(HttpServletResponse response, ErrorEnum error) {
        ErrMsg errMsg = new ErrMsg(error.getCode(), error.getDescription());
        writeJson(response, errMsg);
    }

    /**
     * 响应字符串
     *
     * @param response
     * @param msg
     */
    public final void writeStr(HttpServletResponse response, String msg) {
        response.setContentType("application/json;charset=UTF-8");
        try {
            response.getWriter().write(msg);
        } catch (IOException e) {
            logger.warn(e.getMessage());
        }
    }

    /**
     * 将文件转为字节响应体.
     * @param file
     * @return
     * @throws IOException
     */
    public final ResponseEntity<byte[]> generateByteEntity(File file) throws IOException {
        InputStream is=new FileInputStream(file);;
        return generateByteEntity(is, file.getName());
    }

    /**
     * 将流转为字节响应体.
     * @param is 输入流
     * @param name 文件名
     * @return
     * @throws IOException
     */
    public  final ResponseEntity<byte[]> generateByteEntity(InputStream is,String name) throws IOException {
        byte[] body;
        body = new byte[is.available()];
        is.read(body);
        return generateByteEntity(body,name);
    }

    /**
     * 将字节数组为字节响应体.
     * @param body
     * @param name
     * @return
     */
    public final ResponseEntity<byte[]> generateByteEntity(byte[] body, String name){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attchement;filename=" + name);
        HttpStatus statusCode = HttpStatus.OK;
        ResponseEntity<byte[]> entity = new ResponseEntity<>(body, headers, statusCode);
        return entity;
    }

    /**
     * 将内容放入成功响应信息.
     * @param obj
     * @return
     */
    public static final String successMsg(Object obj){
        ErrorEnum msg = ErrorEnum.SUCCESS;
        msg.setDescription(obj);
        return msg(msg);
    }

    /**
     * 将ErrorEnum序列化为json.
     * @param msg
     * @return
     */
    public static final String msg(ErrorEnum msg){
        return JSON.toJSONString(new ErrMsg(msg.getCode(),msg.getDescription()));
    }

}
