package edu.ycu.hockshop.httpapi.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.dfsapi.DFS;
import edu.ycu.hockshop.httpapi.service.FileService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

/**
 * 文件服务实现类.
 *
 * @author wrh
 * @since 2018-03-19
 */
@Service
public class FileServiceImpl implements FileService {

    private static final Logger logger = Logger.getLogger(FileServiceImpl.class);

    /**
     * Distributed File System interface.
     *
     * @return
     */
    @Reference(version = "${demo.service.version}",
            application = "${dubbo.application.id}",
            group = "${dfs.dubbo.group}")
    public DFS dfs;

    /**
     * @param name
     * @param path
     * @return
     */
    @Override
    public boolean fileExited(String name, String path) throws IOException {
        return dfs.isExits(path+name);
    }


    /**
     * {@inheritDoc}
     * @param file 文件.
     * @param out 输出路径
     * @return
     * @throws IOException
     */
    @Override
    public String uploadFile(File file, String out) throws IOException {
        if (file == null) {
            return null;
        }

        // 判断文件是否存在
        if (!dfs.isExits(out+file.getName())) {
            if (dfs.uploadFile(FileUtils.getBytes(file), out + file.getName())) {
                return file.getName();
            }
        }else {
            return file.getName();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     *
     * @param name 文件名
     * @param path 路径
     * @return
     * @throws IOException
     */
    @Override
    public byte[] downloadFile(String name, String path) throws IOException {
        return dfs.obtainFileByFileName(name, path);
    }

}
