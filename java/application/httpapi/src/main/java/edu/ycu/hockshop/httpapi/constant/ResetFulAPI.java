package edu.ycu.hockshop.httpapi.constant;

/**
 * http接口请求地址.
 *
 * @author wrh
 * @since 2018-03-08
 */
public interface ResetFulAPI {

    String index = "/index";


    //数字资产接口版本号
    String ASSETS_VERSION = "v1";
    //数字资产首页
    String ASSETS = ASSETS_VERSION + "/assets";

    /*
        文件相关接口
     */
    String UPLOAD_FILE = "/file";


    /*
        数字资产关键信息提取接口
     */
    String EXTRACTION_VERSION = "v1";
    String ASSETS_INFO_EXTRACTION = EXTRACTION_VERSION + "/extraction";


    /*
       区块链服务器相关API
     */
    //当前区块链接口API版本号
    String BLOKCHAIN_API_VERSION = "v1";
    //区块服务器操作首页
    String BLOCKCHAIN_SERVER = BLOKCHAIN_API_VERSION + "/block-chain-server";
    //获取所有区块链务器状态接口
    String BLOCKCHAIN_SERVER_LIST = BLOCKCHAIN_SERVER + "/list";

    /*
       文件服务器API
     */
    //当前文件服务接口API版本号
    String FILE_API_VERSION = "v1";
    //文件服务器操作首页
    String FILE_SERVER = FILE_API_VERSION + "/file-server";
    //获取所有文件服务器状态接口
    String FILE_SERVER_NODE_LIST = FILE_SERVER + "/list";
}
