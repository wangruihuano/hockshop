package edu.ycu.hockshop.httpapi.service;


import java.io.File;
import java.io.IOException;

/**
 * 文件服务接口.
 *
 * @author wrh
 * @since 2018-03-13
 */
public interface FileService {

    /**
     * 判断文件是否存在.
     * @param name 文件名
     * @param path 路径
     * @return 若存在返回true.不存在返回false.
     */
    boolean fileExited(String name, String path) throws IOException;

    /**
     * 文件上传.
     * 上传成功后返回文件名(在此应用中为MD5值).
     * @param file 文件.
     * @param out 输出路径
     * @return
     */
    String  uploadFile(File file, String out) throws IOException;

    /**
     * 根据文件名及路径下载文件
     * @param name 文件名
     * @param path 路径
     * @return 文件字节数组
     */
    byte[] downloadFile(String name,String path) throws IOException;

}
