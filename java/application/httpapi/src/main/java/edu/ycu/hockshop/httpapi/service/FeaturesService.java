package edu.ycu.hockshop.httpapi.service;

import java.io.IOException;
import java.util.Map;

/**
 * 功能服务接口.
 * @author wrh
 * @since 2018-04-22
 */
public interface FeaturesService {

    /**
     * 通过第一类型名称获取第二类型.
     * @param firstType 第一类型名称
     * @return 第二类型,map中的键为名称,值为对应处理的全类路径
     */
    Map<String, String> getSecondTypeByFirst(String firstType);

    /**
     * 提取信息.
     * @param className 全类名
     * @param md5 文件md5
     * @param features 需要抽取的特征
     * @return 提取出来的信息.
     * @since 2018-04-23
     */
    Object exteactInfo(String className, String md5, String[] features) throws IOException, ClassNotFoundException;

    /**
     * 获取特征.
     * @param className 全类名
     * @return
     */
    String[] getFeatures(String className) throws Exception;
}
