# 分布式文件存储模块
--------

本模块项目提供了[分布式文件接口](./src/main/java/edu/ycu/hockshop/dfs/DFS.java) 的Hadoop具体实现，若有必要，你也可以通过实现该接口创建自定义的分布式文件系统供[httpapi项目](../hockshop-httpapi)使用.

同时，本模块提供高可用的文件上传及本地缓存.

