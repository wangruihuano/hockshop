package edu.ycu.dfs;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.Stack;

/**
 * 栈测试.
 * @author wrh 
 * @since 2018-04-26
 */
public class StackTest {

    Logger logger = Logger.getLogger(StackTest.class);

    @Test
    public void addSameTest(){
        try {
            Stack<String> stack = new Stack<>();
            stack.add("1");
            stack.add("1");
            System.out.println(stack.size());
        }catch (Exception e){
            logger.debug("栈添加相同数据测试异常",e);
        }

    }
}
