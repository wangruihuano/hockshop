package edu.ycu.dfs;

import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.dfsapi.DFS;
import edu.ycu.hockshop.dfs.impl.HadoopDFS;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.*;

/**
 * Hadoop文件系统测试.
 * 
 * @author wrh
 * @since 2018-03-20
 */
public class HadoopDFS_Test {

    private static Logger logger= Logger.getLogger(HadoopDFS_Test.class);

    String  filePath = HadoopDFS_Test.class.getClassLoader().getResource("log4j.properties").getFile();

    DFS dfs;

    {
        try {
            dfs=new HadoopDFS();
        }catch (Exception e){
            logger.debug("dfs init error",e);
        }
    }

    @Test
    public void uploadTest(){
        boolean success=false;
        try {
            success=dfs.uploadFile(FileUtils.getBytes(new File(filePath)),"/test");
        }catch (Exception e){
            logger.debug("文件上传成功?"+success,e);
        }
    }

    @Test
    public void getFileTest(){
        try {
            byte[] file=dfs.obtainFileByFileName("test","/");
            File downloadFile = new File("text_download_file");
            IOUtils.copy(new ByteArrayInputStream(file),new FileOutputStream(downloadFile));
            logger.debug("文件已保存在:"+downloadFile.getAbsolutePath());
        } catch (Exception e) {
            logger.debug("获取文件至text_download_file出现异常",e);
        }
    }

    @Test
    public void delTest(){
        try {
            if (dfs!=null){
                boolean delSusses = dfs.deleteFileByName("/test");
                logger.debug("删除成功?"+delSusses);
            }
        } catch (IOException e) {
            logger.debug("删除文件测试出现异常",e);
        }catch (NullPointerException e){
            logger.debug("删除文件测试出现异常",e);
        }
    }
}
