package edu.ycu.hockshop.dfs;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ActionAspect {

    @Pointcut("execution(* edu.ycu.hockshop.dfs.*.*(..))")
    public void doAction(){}

    @Before("doAction()")
    public void deBefore(JoinPoint joinPoint){

    }
}
