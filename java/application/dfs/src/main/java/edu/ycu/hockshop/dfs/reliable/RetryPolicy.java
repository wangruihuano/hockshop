package edu.ycu.hockshop.dfs.reliable;

/**
 * Make a policy to define how long to wait when task upload fail.
 * @author wrh
 * @since 2018-05-02
 */
public class RetryPolicy {

    //if upload fail,retry waiting max times.
    public static final int MAX_RETRY_TIME = 60;

    //waiting period.
    public static final int WAITING_PERIOD = 1;


    /**
     * more fail,longer wait until equals MAX_RETRTIME.
     * @param failTimes
     * @return
     */
    public static long MFLW(int failTimes){
        if (failTimes>MAX_RETRY_TIME){
            failTimes = MAX_RETRY_TIME;
        }
        return failTimes*1000*WAITING_PERIOD;
    }

    private RetryPolicy(){}
}
