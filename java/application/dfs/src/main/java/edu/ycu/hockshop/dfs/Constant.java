package edu.ycu.hockshop.dfs;

/**
 * 常量定义.
 * @since 2018-05-01
 * @author wrh
 */
public interface Constant {

    //任务栈序列化文件路径
    String STACK_FILE = "stack.ser";
}
