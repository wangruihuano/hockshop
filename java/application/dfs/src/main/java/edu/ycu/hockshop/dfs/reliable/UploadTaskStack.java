package edu.ycu.hockshop.dfs.reliable;

import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.common.util.io.ObjectUtil;
import edu.ycu.hockshop.dfs.Constant;
import edu.ycu.hockshop.dfsapi.DFS;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Stack;

/**
 * 任务栈.
 * 用来存储没有上传成功的文件.
 * 若任务栈中有未上传成功的文件,则将会根据等待策略一直上传,直到栈清空.
 *
 * @author wrh
 * @since 2018-05-01
 */
public class UploadTaskStack implements Serializable, Runnable {

    private Logger logger = Logger.getLogger(UploadTaskStack.class);

    @Getter
    private Stack<Task> stack;

    @Getter @Setter
    private long stackEmptySleepTime = 10*1000;

    //upload file failed times
    private int failTimes = 0;

    private DFS dfs;

    private static UploadTaskStack taskStack;

    private UploadTaskStack(DFS dfs){
        this.dfs = dfs;
        init();
    }

    /**
     * Design it as a singleton , one app one task.
     *
     * @return
     * @throws IOException
     */
    public static UploadTaskStack taskStack(DFS dfs) {
        if (taskStack == null && dfs != null) {
            taskStack = new UploadTaskStack(dfs);
        }
        return taskStack;
    }

    /**
     * 添加任务.
     *
     * @param file 加入任务栈的文件
     * @return 是否添加成功
     */
    public boolean addTask(File file,String outPath) {

        boolean addSucess=false;
        //judge the stack if contains param
        if (!stack.contains(file)) {
            addSucess=stack.add(new Task(file,outPath));
            writeStack();
        }
        return addSucess;
    }

    /**
     * 移除任务.
     *
     * @param file 要移除的文件
     * @return 是否移除成功
     */
    public boolean remove(File file) {
        return stack.remove(file);
    }

    private void init() {
        File file = new File(Constant.STACK_FILE);

        /*
            reload the stack from file if the file exists
         */
        if (!file.exists()) {
            try {
                file.createNewFile();
                stack = new Stack<>();
            } catch (IOException e) {
                logger.error("创建序列化文件出错", e);
            }
        } else {
            try {
                stack = (Stack<Task>) ObjectUtil.deserialize(file);
            } catch (ClassNotFoundException e) {
                logger.error("deserialize error", e);
                stack = new Stack<>();
            }catch (EOFException e){
                /** Do nothing,EOFException is a message
                 * @see EOFException
                 */
            }
            catch (IOException e) {
                logger.error("deserialize file io exception ", e);
                stack = new Stack<>();
            }
            writeStack();
        }

    }

    /**
     * Loop execution task if task has file.
     */
    @Override
    public void run() {
        while (true) {

            if (stack!=null && stack.empty()){
                try {
                    Thread.sleep(stackEmptySleepTime);
                } catch (InterruptedException e) {
                    logger.warn("thread sleep error",e);
                }
            }

            if (stack != null && stack.size() > 0) {
                try {
                    Thread.sleep(RetryPolicy.MFLW(failTimes));
                    synchronized (stack) {
                        Task task = stack.peek();

                        if (dfs.uploadFile(FileUtils.getBytes(task.getFile()),task.getOutPath())) {
                            stack.pop();
                            writeStack();
                            failTimes=0;
                            continue;
                        }
                    }
                } catch (IOException e) {
                    logger.warn("file upload error",e);
                } catch (InterruptedException e) {
                    logger.warn("thread sleep error",e);
                }
                failTimes++;
            }
        }
    }

    private void writeStack(){
        try {
            ObjectUtil.serialize(stack,new File(Constant.STACK_FILE));
        } catch (IOException e) {
            logger.error("serialize error",e);
        }
    }
}
