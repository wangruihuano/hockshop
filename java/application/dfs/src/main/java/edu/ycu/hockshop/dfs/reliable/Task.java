package edu.ycu.hockshop.dfs.reliable;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.File;
import java.io.Serializable;

/**
 * Upload file task.
 * @author wrh
 * @since 2018-05-02
 */
@Getter
@AllArgsConstructor
public class Task implements Serializable {

    //本地文件
    private File file;

    //分布式文件系统中对应的文件路径
    private String outPath;
}
