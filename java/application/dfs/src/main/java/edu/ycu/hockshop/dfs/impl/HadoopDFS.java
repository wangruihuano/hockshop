package edu.ycu.hockshop.dfs.impl;

import com.alibaba.dubbo.config.annotation.Service;
import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.dfsapi.DFS;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.UUID;

/**
 * 分布式文件存储接口的Hadoop具体实现类.
 * @see edu.ycu.hockshop.dfsapi.DFS
 * @author wrh
 * @since 2018-03-17
 */
@Service(version = "1.0.0",
        group = "HadoopDFS",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}")
public class HadoopDFS implements DFS {

    private static Logger logger = Logger.getLogger(HadoopDFS.class);

    /**
     * 临时文件存储目录
     */
    String tmpDir = "/tmp/";

    private FileSystem fileSystem;


    public HadoopDFS() {
        init();
    }

    /**
     * 静态加载hadoop配置文件
     */
    private void init() {
        Configuration conf = new Configuration();
        conf.addResource(HadoopDFS.class.getClassLoader().getResourceAsStream("core-site.xml"));
        conf.addResource(HadoopDFS.class.getClassLoader().getResourceAsStream("hdfs-site.xml"));

        try {
            fileSystem = FileSystem.get(conf);
        } catch (Exception e) {
            logger.warn(e.getMessage());
        }
    }

    /**
     *
     * {@inheritDoc}
     * @param fileByte 本地字节
     * @param outPath 输出路径(Hadoop文件系统路径)
     * @return
     */
    @Override
    public boolean uploadFile(byte[] fileByte, String outPath) throws IOException {
        File tmp = new File(tmpDir+UUID.randomUUID().toString());
        FileUtils.getFile(fileByte,tmp);

        try {
            Path src = new Path(tmp.getPath());
            Path dst = new Path(outPath);
            fileSystem.copyFromLocalFile(true, src, dst);
            return true;
        } catch (IOException e) {
            logger.debug("文件读写异常:" + e.getMessage());
            throw e;
        }
    }

    /**
     * 通过文件名和hadoop文件路径获取文件.
     * 若没有该文件返回null.
     * @param fileName 文件名称
     * @param filePath 文件路径(Hadoop文件系统路径)
     * @return
     */
    @Override
    public byte[] obtainFileByFileName(String fileName, String filePath){
        try {
            Path path = new Path(filePath+fileName);
            File tmp = new File(tmpDir+fileName);
            if (!tmp.exists()){
                tmp.createNewFile();
            }

            fileSystem.copyToLocalFile(false, path, new Path(tmp.getAbsolutePath()));
            byte[] res = FileUtils.getBytes(tmp);
            tmp.delete();
            return res;
        } catch (IOException e) {
            logger.debug("获取文件异常:" + e.getMessage());
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * @param absolutePath 文件绝对路径
     * @return 文件是否删除成功.
     * @since 2018-03-20
     */
    @Override
    public boolean deleteFileByName(String absolutePath) throws IOException {
        return fileSystem.delete(new Path(absolutePath),false);
    }

    /**
     * {@inheritDoc}
     * @param path 文件路径(Hadoop文件系统路径)
     * @return
     * @throws IOException
     */
    @Override
    public boolean isExits(String path) throws IOException {
        return fileSystem.exists(new Path(path));
    }
}
