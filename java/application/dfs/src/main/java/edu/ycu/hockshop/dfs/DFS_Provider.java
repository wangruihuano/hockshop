package edu.ycu.hockshop.dfs;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class DFS_Provider {

    public static void main(String[] args) {
        new SpringApplicationBuilder(DFS_Provider.class)
                .web(WebApplicationType.NONE)
                .run(args);
    }
}
