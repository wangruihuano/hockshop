package edu.ycu.hockshop.marmot.standard;

import java.io.File;
import java.io.Serializable;

/**
 * 基础文件类.
 * 自定义的媒体类型抽取类需要继承自此类.
 * @author wrh
 * @since 2018-04-23
 */
public abstract class BaseExtractableFile implements Serializable {

    //文件
    protected File file;

    public BaseExtractableFile(File file) {
        this.file = file;
    }

}
