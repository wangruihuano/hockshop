package edu.ycu.hockshop.marmotStandard;

/**
 * 可提取信息的接口.
 * 所有自定义的文件提取类均要实现这个接口，
 * 否则反射调用将不成功.
 * @author wrh
 * @since 2018-04-23
 */
public interface Extractable<FEATURE> {

    /**
     * 提取信息.
     * @param featureNames 特征名称
     * @return
     */
    FEATURE extract(String[] featureNames) throws Exception;

    /**
     * 特征模型的类
     * @return
     */
    Class<FEATURE> featureClass();
}
