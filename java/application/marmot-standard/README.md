# 信息抽取标准化项目

为利用JAVA反射机制动态加载类文件,以及实现针对不同媒体实现插件化开发,特地提取此项目进行接口约定.

### 插件开发示例

#### 1.打包此项目

在本项目根目录中执行：`mvn package`

执行之后若显示`[INFO] BUILD SUCCESS` 则表示构建成功，在target目录中你会看到hockshop-marmot-standard-**.jar.

#### 2.开发自定义插件

在你使用的集成开发环境中新建java项目，并将上一步构建生成的jar文件引入构建路径中.

接下来你就可以编写你自定义的插件了.

#### 3.例子

1. 首先你应该自定义一个特征模型,这里有一个文本的例子:
```java
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 文本特征模型.
 * @author wrh
 * @since 2018-04-25
 */
@Getter
@Setter
@ToString
public class TextFeature {

    //内容
    private String content;

    //长度
    private Integer length;
}

```
2. 自定义实现继承 `BaseExtractableFile` 并实现edu.ycu.hockshop.marmot.standard.Extractable的类.

**这意味着你需要利用构造中传入的File实现 `Extractable#extract(String[] featureNames)` 这个方法.其中参数为上面自定义特征类的属性名数组(不一定是全部属性).**

```java

import edu.ycu.hockshop.marmot.feature.TextFeature;
import BaseExtractableFile;
import Extractable;

import java.io.*;

/**
 * 文本文件.
 * @author wrh
 * @since 2018-04-21
 */
public class Text extends BaseExtractableFile implements Extractable<TextFeature>{

    public Text(File file) {
        super(file);
    }


    /**
     * {@inheritDoc}
     * @param featureNames 特征名称
     * @return
     * @throws Exception
     */
    @Override
    public TextFeature extract(String[] featureNames) throws Exception {
        if (file==null || featureNames==null || featureNames.length==0){
           return null;
        }

        TextFeature textFeature = new TextFeature();
        String content = FileUtils.getStringFromFile(file);

        //根据要查询的特征名,赋予特征模型对应的值
        for (String featureName:featureNames) {
            switch (featureName){
                case "content":
                    textFeature.setContent(content);
                    break;
                case "length":
                    textFeature.setLength(content.length());
                    break;
            }
        }
        return textFeature;
    }

    //重写这个方法，返回上一步自定义特征模型的类
    @Override
    public Class<TextFeature> featureClass() {
        return TextFeature.class;
    }
    
    /**
    * 从文件中读取字符串. 
    */
    public static String getStringFromFile(File file) throws IOException {
        BufferedReader bf = new BufferedReader(new FileReader(file));
        StringBuilder builder = new StringBuilder();

        String buffer;
        while ((buffer=bf.readLine())!=null){
            builder.append(buffer+"\n");
        }
        return builder.toString();
    }

}

```


#### 4.测试

你可以以自己喜欢的方式自定义一个测试，确保方法能够正确执行.

可以像下面这样:

```java
import java.io.IOException;
//当然，你还需要根据包名引入上面的Text类

public class TextTest {

    @Test
    public void extractTest(){
        String file_path= this.getClass().getResource("/test.txt").getFile().toString();

        Text text = new Text(new File(file_path));
        try {
            System.out.println(text.extract());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```
#### 5.打包并发布

打包：
    
   根据你使用的构建工具及IDE将项目打包为jar类型

发布：

   //TODO 平台待完善