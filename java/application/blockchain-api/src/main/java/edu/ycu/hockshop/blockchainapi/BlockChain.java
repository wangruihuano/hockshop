package edu.ycu.hockshop.blockchainapi;

import java.util.List;

import edu.ycu.hockshop.common.model.DigitizedAssests;

/**
 * 区块链接口. 所有区块链相关对外接口均在这里定义.
 *
 * @author zql
 * @since 2018-03-12
 */
public interface BlockChain {


	/**
	 * 存入资产
	 * 
	 * @param assets  资产
	 * @return Boolean
	 * @throws Exception 区块连内部异常
	 */
	Boolean pushAssetsInfo(DigitizedAssests assets) throws Exception;

	/**
	 * 获取资产
	 * 
	 * @param currentId 待查询ID
	 * @return DigitizedAssets
	 * @throws Exception 区块连内部异常
	 */
	DigitizedAssests pullAssetsInfo(String currentId) throws Exception;

	/**
	 * 注销资产
	 * 
	 * @param currentId 待查询ID
	 * @return boolean
	 * @throws Exception 区块连内部异常
	 */
	Boolean cancelAssetsInfo(String currentId) throws Exception;

	/**
	 * 获取所有资产
	 * @param start 起始
     * @param end   结束
	 * @return List<DigitizedAssets> 包含所有资产的集合
	 * @throws Exception 区块连内部异常
	 */
	List<DigitizedAssests> pullAllAssetsInfo(String start, String end) throws Exception;

	/**
	 * 通过ID获取资产修改记录
	 * 
	 * @param currentId 待查询ID
	 * @return String
	 * @throws Exception 区块连内部异常
	 */
	String pullAssetsHistoryByID(String currentId) throws Exception;

	/**
	 * 获取调用者
	 * 
	 * @return String
	 * @throws Exception 区块连内部异常
	 */
	String pullCalledChainName() throws Exception;


}
