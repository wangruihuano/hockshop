# marmot 项目说明

此项目用以从文件中提取关键信息,其中部分功能依赖以下工具\环境,请参照以下步骤进行安装或部署.

1. 使用此项目的OCR功能(即从图像提取文字)需要在部署环境中安装tesseract.

    - CentOS,Fedora: `yum install tesseract`
    - Debian,Ubuntu: `apt-get install tesseract-ocr`

2. 人脸特征提取功能依赖pythonWeb项目[py-face](../py-face)

    该python项目部署参照[部署文档](../script/py_face/README.md),项目说明参考[这里](../py-face/README.md).
    
    部署之后将本目录中[src/main/resources/config.properties文件](/src/main/resources/config.properties)中的*FaceAPI_URL*的值替换该python项目的API地址.


3. 此项目用以音频文件的抽取,转码,视频文件的切割,转码,其中部分功能依赖一下工具\环境,请参照以下步骤进行部署和安装.

    1. 安装ffmpeg
        - CentOS,Fedora:
            ```
            rpm --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
            rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
            yum install ffmpeg
            ```
        - Debian,Ubuntu:
        
            `apt-get install ffmpeg`     
                
    2. 安装lame
    
        - CentOS,Fedora:`yum install lame`
        
        - Debian,Ubuntu:`apt-get install lame`
        
    3. 安装yasm
    
        - CentOS,Fedora:`yum install yasm`
        
        - Debian,Ubuntu:`apt-get install yasm`