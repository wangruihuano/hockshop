package edu.ycu.hockshop.marmotTest.media;


import org.apache.log4j.Logger;
import org.apache.poi.POIOLE2TextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.Property;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;

/**
 * Apache POI库测试.
 */
public class POITest {

    private static final Logger logger = Logger.getLogger(POITest.class);
    File inputFile = new File(POITest.class.getClassLoader().getResource("poi/test97-2003.xls").getFile());

    @Test
    public void getTextTest() {
             try {

            FileInputStream fis = new FileInputStream(inputFile);
            POIFSFileSystem fileSystem = new POIFSFileSystem(fis);
            // Firstly, get an extractor for the Workbook
            POIOLE2TextExtractor oleTextExtractor =
                    ExtractorFactory.createExtractor(fileSystem);
            // Then a List of extractors for any embedded Excel, Word, PowerPoint
            // or Visio objects embedded into it.
            System.out.println(oleTextExtractor.getText());
            DocumentSummaryInformation information = oleTextExtractor.getDocSummaryInformation();
            for (Property property : information.getProperties()) {
                System.out.println(property.toString());
            }
            // oleTextExtractor.getDocSummaryInformation().get

            fileSystem.close();
            fis.close();
        } catch (Exception e) {
            logger.info("提取office文件信息异常\n", e);
        }

    }
}
