package edu.ycu.hockshop.marmotTest.media;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.marmot.media.PDF;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;

/**
 * PDF信息提取测试.
 * @author wangruihuan
 * @since 2018-06-21
 */
public class PDFTest {

    private static final Logger logger = LoggerFactory.getLogger(PDFTest.class);

    @Test
    public void extractTest(){
        Class c = PDFTest.class;
        URL url=c.getResource("/pdf/FRJDL-Chai-JOS2005-ESL.pdf");
        String file_path= url.getFile();

        PDF pdf = new PDF(new File(file_path));
        try {
            System.out.println(JSON.toJSONString(pdf.extract(new String[]{"pageNumber","content","version"})));
        }catch (Exception e) {
            logger.debug("TextImg文件读取出错",e);
        }
    }
}
