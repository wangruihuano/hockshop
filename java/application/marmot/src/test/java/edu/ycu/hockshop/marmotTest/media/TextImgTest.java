package edu.ycu.hockshop.marmotTest.media;

import edu.ycu.hockshop.marmot.feature.TextImgFeature;
import edu.ycu.hockshop.marmot.media.TextImg;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.File;

/**
 * OCR测试.
 */
public class TextImgTest {

    Logger logger = Logger.getLogger(TextImgTest.class);
    @Test
    public void test(){
        String  imgPath = TextImgTest.class.getClassLoader().getResource("T.png").getFile();
        try {
            TextImg img  = new TextImg(new File(imgPath));
            TextImgFeature feature=img.extract(new String[]{"eng"});
            System.out.println(feature);
        }catch (Exception e){
            logger.debug("ocr异常",e);
        }
    }
}
