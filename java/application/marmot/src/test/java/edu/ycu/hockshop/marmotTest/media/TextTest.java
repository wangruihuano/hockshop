package edu.ycu.hockshop.marmotTest.media;

import edu.ycu.hockshop.marmot.media.Text;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * 文本抽取测试.
 * @author wrh
 * @since 2018-04-23
 */
public class TextTest {

    private static final Logger logger = Logger.getLogger(TextTest.class);

    @Test
    public void toStringTest(){
        Class c = this.getClass();
        URL url=c.getResource("/file_type.json");
        String file_path= url.getFile();

        Text text = new Text(new File(file_path));
        try {
            System.out.println(text.extract(new String[]{"content"}));
        }catch (Exception e) {
            logger.debug("Text文件读取出错",e);
        }
    }

    @Test
    public void generateFileTest(){
        File file = new File("test");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
