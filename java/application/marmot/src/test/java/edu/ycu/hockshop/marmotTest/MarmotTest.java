package edu.ycu.hockshop.marmotTest;

import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.marmot.MarmotImpl;
import edu.ycu.hockshop.marmotapi.Marmot;
import edu.ycu.hockshop.marmot.feature.TextFeature;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.File;

/**
 * 反射测试.
 */
public class MarmotTest {

    private static final Logger logger = Logger.getLogger(MarmotTest.class);

    @Test
    public void doExtTest(){

        Marmot marmot = new MarmotImpl();

        try {
            String[] fs = marmot.getFeatures("edu.ycu.hockshop.marmot.feature.TextFeature");
            TextFeature s= (TextFeature) marmot.doExtract("edu.ycu.hockshop.marmot.media.Text",
                    FileUtils.getBytes(new File(Object.class.getResource("/file_type.json").getFile()))
            ,fs);
            System.out.println(s);
        } catch (Exception e){
            logger.debug("反射时错误",e);
        }
    }
}
