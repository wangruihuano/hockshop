package edu.ycu.hockshop.marmotTest;

import org.apache.http.client.fluent.Request;
import org.junit.Test;

import java.io.IOException;

/**
 * Http请求测试.
 * @author wangruihuan
 * @since 2018-06-15
 */
public class HttpClientTest {

    @Test
    public void getTest(){
        try {
            String s=Request.Get("http://127.0.0.1:5000/face?file=3.jpeg")
                    .execute().returnContent().asString();
            System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
