package edu.ycu.hockshop.marmotTest;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;

/**
 * Properties文件读写测试.
 * @author wangruihuan
 * @since 2018-06-21
 */
public class PropertiesTest {

    Logger logger = LoggerFactory.getLogger(PropertiesTest.class);
    private static final String fileName = "test.properties";
    @Test
    public void readTest(){
        Properties properties = new Properties();
        // 使用ClassLoader加载properties配置文件生成对应的输入流
        InputStream in = PropertiesTest.class.getClassLoader().getResourceAsStream(fileName);
        // 使用properties对象加载输入流
        try {
            properties.load(in);
            //获取key对应的value值
            String property = "test";
            String res = properties.getProperty(property);
            logger.debug("读取"+fileName+"文件中配置属性"+property+"的值为:"+res);
        }catch (Exception e){
            logger.debug("读取配置异常",e);
        }
    }
}
