package edu.ycu.hockshop.marmotTest.nummpy;

import edu.ycu.hockshop.marmot.numpy.Random;
import org.junit.Test;

/**
 * 随机数测试.
 * @author wrh
 * @since 2018-04-02
 */
public class RandomTest {

    @Test
    public void randomTest(){
        float[] rand_f32 = new Random().rand(100).asTypeFloat32();

        for (Float f:rand_f32){
            System.out.println(f);
        }

        System.out.printf("--------------------------------------------------------");
        double[] rand_f64 = new Random().rand(100).asTypeFloat64();

        for (Double f:rand_f64){
            System.out.println(f);
        }
    }
}
