package edu.ycu.hockshop.marmot.media.feature;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zql
 * @date 2018.6.6
 */
@Data
@EqualsAndHashCode(callSuper = true)
public final class MusicFeature extends AudioFeature {
    /**
     * The singer who sang this song.
     */
    private String singer;
    /**
     * The name of this song.
     */
    private String songName;
    /**
     * The album that contains this song.
     */
    private String album;

}

