package edu.ycu.hockshop.marmot.feature;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 文本特征模型.
 * @author wrh
 * @since 2018-04-25
 */
@Getter
@Setter
@ToString
public class TextFeature implements Serializable {

    //内容
    private String content;

    //长度
    @Deprecated
    private Integer length;
}
