package edu.ycu.hockshop.marmot;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.common.util.io.FileType;
import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.common.util.io.StreamUtils;
import edu.ycu.hockshop.marmotapi.Marmot;
import edu.ycu.hockshop.marmotStandard.Extractable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

/**
 * Marmot核心接口实现类.
 * 包括动态加载类，获取配置等.
 *
 * @author wrh
 * @since 2018-04-21
 */
@Service(version = "1.0.0",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}",
    timeout = 10000)
public class MarmotImpl implements Marmot {

    /**
     * 临时文件存储目录
     */
    String tmpDir = "/tmp/";

    private static final Logger logger = LoggerFactory.getLogger(MarmotImpl.class);

    public static final String TYPE_CONFIG_FILE = "/file_type.json";

    //文件类型及对应的包名
    private Map<String, Map<String, String>> type;

    public MarmotImpl() {
        this(TYPE_CONFIG_FILE);
    }

    /**
     * 自定义配置文件时使用此方法.
     * @param typeFileName 文件路径
     * @apiNote 项目根路径为"/"
     */
    public MarmotImpl(String typeFileName) {
        init(typeFileName);
    }

    /**
     * {@inheritDoc}
     * @param className 类别的全类名
     * @return
     */
    @Override
    public String[] getFeaturesByTypeName(String className) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class clazz = Class.forName(className);
        //获取File类型参数的构造函数
        Constructor constructor = clazz.getConstructor(File.class);
        //示例化对象
        Extractable extractable = (Extractable) constructor.newInstance(new File(""));
        Class featureClass=extractable.featureClass();
        return getFeatures(featureClass);
    }

    /**
     * {@inheritDoc}
     * @param className 全类名
     * @return
     */
    @Override
    public String[] getFeatures(String className) throws ClassNotFoundException {
        Class clazz=Class.forName(className);
        return getFeatures(clazz);
    }

    /**
     * {@inheritDoc}
     * @param clazz 类
     * @return
     */
    @Override
    public String[] getFeatures(Class clazz) {
        // 获取实体类及其父类的所有属性信息，返回Field列表
        List<Field> fields = new ArrayList<>();

        //当基类不为Object时循环获取属性
        while (clazz != null && !clazz.getName().toLowerCase().equals("java.lang.object")) {
            //当父类为null的时候说明到达了最上层的父类(Object类).
            fields.addAll(Arrays.asList(clazz .getDeclaredFields()));
            clazz = clazz.getSuperclass(); //得到父类,然后赋给自己
        }
        // 将属性名填入数组
        String features[] = new String[fields.size()];
        for (int i=0;i<features.length;i++){
            features[i] = fields.get(i).getName();
        }
        return features;
    }

    /**
     * {@inheritDoc}
     * @param jar jar文件
     * @param className 全类名
     * @param file 要解析的文件
     * @param featureNames 抽取的特征名
     * @return 特征模型
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Object doExtract(File jar, String className, File file, String[] featureNames) throws IOException, ClassNotFoundException {

        ClassLoader classLoader = new URLClassLoader(new URL[]{file.toURI().toURL()});
        Class clazz=classLoader.loadClass(className);
        return doExtract(clazz,file,featureNames);
    }

    /**
     * {@inheritDoc}
     **/
    public Object doExtract(Class clazz, File file, String[] featureNames) throws IOException {
        Object res = null;
        try {

            //获取File类型参数的构造函数
            Constructor constructor = clazz.getConstructor(File.class);

            //示例化对象
            Extractable extractable = (Extractable) constructor.newInstance(file);

            //执行提取方法
            res = extractable.extract(featureNames);
        } catch (IOException e) {
            logger.info("IO异常", e);
            throw e;
        } catch (Exception e) {
            logger.info("反射异常", e);
        }

        return res;
    }

    /**
     * {@inheritDoc}
     **/
    @Override
    public Object doExtract(String className, byte[] fileByte, String[] featureNames) throws ClassNotFoundException, IOException {
        try {
            File tmp = new File("/tmp/"+UUID.randomUUID());
            FileUtils.getFile(fileByte,tmp);
            //加载类
            Class clazz = Class.forName(className);
            return doExtract(clazz, tmp,featureNames);
        } catch (ClassNotFoundException e) {
            logger.info("类不存在", e);
            throw e;
        }

    }

    @Override
    public Map<String, Map<String, String>> getTypeInfo() {
        return type;
    }

    public boolean addType(String firstType, String secondType, String className) {
        if (type != null) {
            Map firType = type.get(firstType);
            if (firType != null) {
                Map secType = (Map) firType.get(secondType);
                if (secondType == null) {
                    secType.put(secondType, className);
                    type.put(firstType, firType);

                    //将配置文件写入文件
                    try {
                        FileUtils.writeStr2File(new File(this.getClass().getResource(TYPE_CONFIG_FILE).getFile()),
                                JSON.toJSONString(type));
                    } catch (IOException e) {
                        logger.info("读写配置文件出错", e);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 初始化
     * @param typeFileName 配置文件名
     */
    private void init(String typeFileName) {
//        String file = Object.class.getResource(typeFileName).getFile();
        InputStream in=this.getClass().getResourceAsStream(typeFileName);
        try {
            type = (Map<String, Map<String, String>>) JSON.parse(StreamUtils.getStringFromStream(in));
        } catch (IOException e) {
            e.printStackTrace();
            logger.info("配置文件读取错误", e);
        }
    }
}
