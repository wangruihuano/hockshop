package edu.ycu.hockshop.marmot.utils.aip;

import com.baidu.aip.speech.AipSpeech;
import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.marmot.Constant;
import edu.ycu.hockshop.marmot.utils.ffmpeg.AudioUtils;
import it.sauronsoftware.jave.EncoderException;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import static edu.ycu.hockshop.marmot.Constant.*;

/**
 * 百度相关接口调用工具.
 * @author wangruihuan
 * @since 2018-06-27
 */
public class BaiduApiClient {

    private BaiduApiClient() {
    }


    private static final Logger logger = Logger.getLogger(BaiduApiClient.class);

    private static AipSpeech speech ;

    private static BaiduApiClient client = new BaiduApiClient();
    private static Properties properties;



    /**
     * 初始化client.
     */
    static {
        try {
            //读取配置文件
            String propertiesFilePath = BaiduApiClient.class.getClassLoader().getResource(CONFIG_PROPERTIES).getFile();
            properties  = FileUtils.loadPropertierFromFile(new File(propertiesFilePath));

            //初始化参数
            speech =new AipSpeech(properties.getProperty(APP_ID), properties.getProperty(API_KEY), properties.getProperty(SECRET_KEY));

        } catch (IOException e) {
            logger.error("读取配置文件异常",e);
        }
    }

    public static BaiduApiClient getClient(){
        return client;
    }


    /**
     * 语音识别.
     * 原始 PCM 的录音参数必须符合 8k/16k 采样率、16bit 位深、单声道，支持的格式有：pcm（不压缩）、wav（不压缩，pcm编码）、amr（压缩格式）。
     * @param file 文件.
     * @param format 文件格式
     * @param cuid 指定id,请求之后将会返回
     * @return
     */
    private JSONObject recognition(File file, String format, String cuid,Lang dev_pid){
        HashMap<String,Object> option = new HashMap();
        option.put("cuid",cuid);
        if (dev_pid!=null){
            option.put("dev_pid",dev_pid.getDev_pid());
        }
        JSONObject res = speech.asr(file.getAbsolutePath(), format, 16000, option);
        return res;
    }

    /**
     * 语音识别.
     * @param file 要识别的语音文件.
     * @return 识别之后的结果
     * @throws EncoderException 编码异常
     */
    public String recognition(File file,Lang lang) throws EncoderException {

        // 切割并转码文件
        List<File> files=AudioUtils.splitAndEncode2Pcm(file,60,new File(properties.getProperty(Constant.TMP_DIR)));
        StringBuilder builder = new StringBuilder();

        // 语音识别
        // TODO 待异步
        for (File f:files){
            JSONObject object=recognition(f,"pcm","test",lang);
            logger.info(object);
            if (object.has("result")){
                builder.append(object.getJSONArray("result").get(0));
            }
        }

        return builder.toString();
    }

    /**
     * 支持的语言种类及其对应的dev_pid.
     */
    public enum Lang{
        PuTongHuaWithSimEng(1536),
        PuTongHua(1537),
        English(1737),
        Cantonese(1637),
        SichuanHua(1837),
        PuTongHuaFarField(1936);

        Integer  dev_pid;
        Lang(Integer dev_pid) {
            this.dev_pid = dev_pid;
        }

        public Integer getDev_pid() {
            return dev_pid;
        }
    }

}
