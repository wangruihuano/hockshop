package edu.ycu.hockshop.marmot.media.feature;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 脸部特征.
 */
@Getter
@Setter
@ToString
public class Face {

    /**
     * 人脸位置
     */
    private Rect rect;

    /**
     * 人脸特征点位置.
     */
    private Point[] points;
}
