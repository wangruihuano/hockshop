package edu.ycu.hockshop.marmot.feature;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zql
 * @since 2018.6.6
 */

@Data
public class AudioFeature extends MultimediaFeature implements Serializable {

    /**
     * 音道
     */
    protected Integer channels;

    /**
     * 解码器
     */
    protected String decoder;

    /**
     * 比特率
     */
    protected Integer bitRate;

    /**
     * 采样率
     */
    protected Integer samplingRate;

}
