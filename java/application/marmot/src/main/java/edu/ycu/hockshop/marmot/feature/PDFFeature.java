package edu.ycu.hockshop.marmot.feature;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * PDF特征模型.
 * @author wangruihuan
 * @since 2018-06-20
 */
@Setter
@Getter
@ToString
public class PDFFeature implements Serializable {

    //页数
    private Integer pageNumber;

    //内容
    private String content;

    //版本
    private Float version;
}
