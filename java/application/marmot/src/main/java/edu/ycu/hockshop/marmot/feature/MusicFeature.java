package edu.ycu.hockshop.marmot.feature;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author zql
 * @date 2018.6.6
 */
@Data
@EqualsAndHashCode(callSuper = true)
public final class MusicFeature extends AudioFeature implements Serializable {
    /**
     * The singer who sang this song.
     */
    private String singer;
    /**
     * The name of this song.
     */
    private String songName;
    /**
     * The album that contains this song.
     */
    private String album;

}

