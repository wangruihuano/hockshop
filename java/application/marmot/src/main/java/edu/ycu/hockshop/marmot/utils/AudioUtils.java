package edu.ycu.hockshop.marmot.utils;

import edu.ycu.hockshop.marmot.media.AudioFormat;
import edu.ycu.hockshop.marmot.media.AudioFormat.type;
import edu.ycu.hockshop.marmot.feature.CmdResult;
import lombok.NonNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static edu.ycu.hockshop.marmot.utils.VideoUtils.runCommand;
import static java.util.regex.Pattern.compile;
import static java.util.stream.Collectors.toList;

/**
 * @author zql
 * @date 2018.6.13
 */
public class AudioUtils {
    /**
     * To provide for suitable audio format to aip service
     *
     * @param file The processing file
     * @param type AudioFormat.type
     * @return pcm, wav, amr
     */
    public static File convertToSpecificFormat(File file, type type) {
        File result = null;
        switch (type) {
            case PCM:
                result = AudioUtils.convertFormat(file, AudioFormat.type.PCM, (filePath, outPath) -> {
                    List<String> commends = new ArrayList<>();
                    commends.add("ffmpeg");
                    commends.add("-y");
                    commends.add("-i");
                    commends.add(filePath);
                    commends.add("-acodec");
                    commends.add("pcm_s16le");
                    commends.add("-f");
                    commends.add("s16le");
                    commends.add("-ac");
                    commends.add("1");
                    commends.add("-ar");
                    commends.add("16000");
                    commends.add(outPath);
                    return commends;
                });
                break;
            case WAV:
                result = AudioUtils.convertFormat(file, AudioFormat.type.WAV, (filePath, outPath) -> {
                    List<String> commends = new ArrayList<>();
                    commends.add("ffmpeg");
                    commends.add("-y");
                    commends.add("-i");
                    commends.add(filePath);
                    commends.add("-acodec");
                    commends.add("pcm_s16le");
//                    commends.add("-f");
//                    commends.add("s16le");
                    commends.add("-ac");
                    commends.add("1");
                    commends.add("-ar");
                    commends.add("16000");
                    commends.add(outPath);
                    return commends;
                });
                break;
//            case AMR:
//                result = AudioUtils.convertFormat(file, AudioFormat.type.WAV, new String[]{"pcm_s16le", "s16le", "1", "16000"});
//                break;
            default:
        }
        if (result == null) {
            throw new RuntimeException("file convert failed!");
        }
        return result;
    }

    /**
     * @param file
     * @param type
     * @param biFunction
     * @return
     */
    public static File convertFormat(@NonNull File file, AudioFormat.type type, BiFunction<String, String, List<String>> biFunction) {
        // 获取文件信息，in path ,out path
        String filePath = file.getPath();

        String outPath = filePath.replace(file.getName(), "");


        String outFilePath = outPath + Math.random() + file.getName()+ "." + type.toString().toLowerCase();

        List<String> commends = biFunction.apply(filePath, outFilePath);

        CmdResult cmdResult = runCommand(commends);
        return cmdResult.getSuccess() ? new File(outFilePath) : null;
    }

    /**
     * @param file
     * @param type
     * @param args
     * @return
     */
    public static File convertFormat(@NonNull File file, AudioFormat.type type, String[] args) {
        // 获取文件信息，in path ,out path
        String filePath = file.getAbsolutePath();

        String outPath = filePath.replace(file.getName(), "") + "tmp/";

        if (!new File(outPath).exists()) {
            new File(outPath).mkdirs();
        }

        String outFilePath = outPath  +file.getName().replace(".","")+"."+ type.toString().toLowerCase();

        List<String> commends = new ArrayList<>();
        commends.add("ffmpeg");
        commends.add("-y");
        commends.add("-i");
        commends.add(filePath);
        commends.add("-acodec");
        commends.add(args[0]);
        commends.add("-f");
        commends.add(args[1]);
        commends.add("-ac");
        commends.add(args[2]);
        commends.add("-ar");
        commends.add(args[3]);
        commends.add(outFilePath);

        CmdResult cmdResult = runCommand(commends);
        return cmdResult.getSuccess() ? new File(outFilePath) : null;
    }


    /**
     * To split audio file
     *
     * @param file The processing file
     * @return file's duration is less than 60s.
     */
    public static File splitAudioFile(@NonNull File file, int start, int duration,String suffix) {

        String filePath = file.getAbsolutePath();
        String outPath = file.getAbsolutePath().replace(file.getName(),"")+"tmp/";

        //TODO
        if (!new File(outPath).exists()) {
            new File(outPath).mkdirs();
        }


        String outFilePath = outPath + Math.random() + file.getName().split("\\.")[0]+"."+suffix.toLowerCase();
        List<String> commends = new ArrayList<>();
        commends.add("ffmpeg");
        commends.add("-i");
        commends.add(filePath);
        commends.add("-ss");
        commends.add(String.valueOf(start));
        commends.add("-t");
        commends.add(String.valueOf(duration));
        commends.add("-acodec");
        commends.add("copy");
        commends.add(outFilePath);
        CmdResult cmdResult = runCommand(commends);
        return cmdResult.getSuccess() ? new File(outFilePath) : null;
    }

    /**
     * To split audio file
     *
     * @param file        The processing file
     * @param minDuration
     * @return The List obtains files and each file's duration is less than 60s.
     */
    public static List<File> spiltAudioFile(@NonNull File file, int minDuration,String suffix) throws FileNotFoundException {

        if (getDuration(file) == 0) {
            throw new FileNotFoundException(file.getName() + " file not find");
        }

        List<Integer> list = IntStream.rangeClosed(0, getDuration(file)).parallel()
                .filter(n -> n % 60 == 0).boxed().collect(toList());

        List<File> collect = list.stream().map(m -> splitAudioFile(file, m, minDuration,suffix))
                .collect(toList());

        return collect;
    }

    /**
     * @param file f
     * @return f
     */
    public static int getDuration(File file) {

        List<String> commends = new ArrayList<>();
        commends.add("ffmpeg");
        commends.add("-i");
        commends.add(file.getAbsolutePath());
        CmdResult cmdResult = runCommand(commends);
        LocalTime time;

        if (cmdResult.getSuccess()) {

            String msg = cmdResult.getMsg();
            //Duration: 00:01:00.00
            Pattern pattern = compile("\\d{2}:\\d{2}:\\d{2}");

            Matcher matcher = pattern.matcher(msg);
            if (matcher.find()) {
                time = LocalTime.parse(matcher.group());
                return time.getMinute() * 60 + time.getSecond();
            }

        }
        return 0;
    }

    /**
     *
     * @param file
     * @return
     */
    public static String getBitRate(File file) {

        List<String> commends = new ArrayList<>();
        commends.add("ffmpeg");
        commends.add("-i");
        commends.add(file.getAbsolutePath());
        CmdResult cmdResult = runCommand(commends);
        LocalTime time;

        if (cmdResult.getSuccess()) {

            String msg = cmdResult.getMsg();
            //Duration: 00:01:00.00
            Pattern pattern = compile("\\d{3}\\skb\\/s");

            Matcher matcher = pattern.matcher(msg);
            if (matcher.find()) {

                return matcher.group();
            }

        }
        return "";
    }

    /**
     *
     */
    public static Boolean cleanTemporaryFiles(@NonNull File file) {

        File target = new File(file.getParent() + "/tmp");
        if (target.exists()) {
            Boolean reduce = Arrays.stream(target.listFiles()).map(File::delete)
                    .reduce(true, Boolean::equals);
            return reduce.equals(target.delete());
        }

        return true;
    }



    /**
     * @param file The file
     * @return
     */
    public static Future<Boolean> cleanTemporaryFilesAsnyc(@NonNull File file) {

        CompletableFuture<Boolean> futureBoolean = new CompletableFuture<>();

        new Thread(() -> {
            try {

                Thread.sleep(1000L);

                Boolean b = cleanTemporaryFiles(file);

                futureBoolean.complete(b);

            } catch (Exception ex) {
                futureBoolean.completeExceptionally(ex);
            }
        }).start();


        return futureBoolean;
    }

    /**
     * @param file
     * @param function
     * @param <T>
     * @param <R>
     * @return
     */
    public static <T, R> R invokeFfmpegTool(@NonNull File file, Function<File, R> function) {

        List<String> commends = new ArrayList<>();

        return function.apply(file);
    }


}
