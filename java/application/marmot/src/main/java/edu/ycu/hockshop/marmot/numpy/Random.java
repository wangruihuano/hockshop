package edu.ycu.hockshop.marmot.numpy;

/**
 * 随机数生成类.
 *
 * @author wrh
 * @since 2018-04-02
 */
public class Random {

    //随机数数量
    private int num;

    /**
     * 指定生存数据的大小.
     * @param num
     * @return
     */
    public Random rand(int num){
        this.num = num;
        return this;
    }

    /**
     * 这个方法用来生成32位浮点类型的数据.
     * @return
     */
    public float[] asTypeFloat32(){
        float[] res = new float[num];
        for (int i=0;i<this.num;i++){
            res[i] = (float)Math.random();
        }
        return res;
    }

    /**
     * 这个方法用来生成64位浮点类型的数据.
     * @return
     */
    public double[] asTypeFloat64(){
        double[] res = new double[num];
        for (int i=0;i<this.num;i++){
            res[i] = Math.random();
        }
        return res;
    }

}
