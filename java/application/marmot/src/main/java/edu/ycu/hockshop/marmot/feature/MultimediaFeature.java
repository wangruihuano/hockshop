package edu.ycu.hockshop.marmot.feature;

import lombok.Data;

import java.io.Serializable;

/**
 * 数字媒体特征.
 * @author wangruihuan
 * @since 2018-06-27
 */
@Data
public class MultimediaFeature implements Serializable {

    // 格式
    protected String format;

    // 时间长度
    protected Long duration;


}
