package edu.ycu.hockshop.marmot.media;

import edu.ycu.hockshop.common.util.io.FileUtils;
import edu.ycu.hockshop.marmot.feature.TextFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;

import java.io.File;

/**
 * 文本文件.
 *
 * @author wrh
 * @since 2018-04-21
 */
public class Text extends BaseExtractableFile implements Extractable<TextFeature> {

    public Text(File file) {
        super(file);
    }


    /**
     * {@inheritDoc}
     *
     * @param featureNames 特征名称
     * @return
     * @throws Exception
     */
    @Override
    public TextFeature extract(String[] featureNames) throws Exception {
        if (file == null || featureNames == null || featureNames.length == 0) {
            return null;
        }

        TextFeature textFeature = new TextFeature();
        String content = FileUtils.getStringFromFile(file);

        //根据要查询的特征名,赋予特征模型对应的值
        for (String featureName : featureNames) {
            switch (featureName) {
                case "content":
                    textFeature.setContent(content);
                    break;
                case "length":
                    textFeature.setLength(content.length());
                    break;
            }
        }
        return textFeature;
    }

    @Override
    public Class<TextFeature> featureClass() {
        return TextFeature.class;
    }


}
