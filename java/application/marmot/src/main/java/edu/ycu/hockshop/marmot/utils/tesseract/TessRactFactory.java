package edu.ycu.hockshop.marmot.utils.tesseract;

import net.sourceforge.tess4j.Tesseract;
import org.apache.log4j.Logger;

import java.io.File;

public class TessRactFactory {

    private static Logger logger = Logger.getLogger(TessRactFactory.class);

    //文字特征文件所在文件夹
    private static final String LANGUAGE_DATAPATH = new File("libs").getAbsolutePath();



    private TessRactFactory(){}

    /**
     * 获取文字识别实例
     * @return
     */
    public static Tesseract getTesseract(){
        Tesseract tesseract=new Tesseract();
        tesseract.setDatapath(LANGUAGE_DATAPATH);
        logger.info("数据路径"+LANGUAGE_DATAPATH);
        tesseract.setLanguage("chi_sim");
        return tesseract;
    }


}