package edu.ycu.hockshop.marmot.media;

/**
 * @author zql
 * @date 2018.6.13
 */
public class AudioFormat {
    private String type;

    public enum type {
        /**
         * 音视频交错格式
         */
        AVI,
        MP3,
        PCM,
        AMR,
        MWV,
        /**
         * 无损压缩音频格式
         */
        APE,
        /**
         * 常见的电影音频格式
         */
        AC3,
        AAC,
        /**
         * 无损压缩
         */
        FLAC,
        /**
         * CD音轨
         */
        CDA,
        /**
         * 杜比5.1
         */
        DD5,
        /**
         * 波型音频格式（容量大）
         */
        WAV,

        MMDI, OGG, WMA, RA
    }
}
