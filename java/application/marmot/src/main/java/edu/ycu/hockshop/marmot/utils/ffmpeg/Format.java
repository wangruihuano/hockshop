package edu.ycu.hockshop.marmot.utils.ffmpeg;

/**
 * ffmeg编解码格式.
 *
 * @author wangruihuan
 * @since 2018-06-27
 */
public enum Format {
    RoQ, ac3, adts, aiff, alaw, amr, asf, asf_stream, au, avi, crc, dv, dvd, ffm, flac, flv, framecrc, gif, gxf, h261, h263, h264, image2, image2pipe, m4v, matroska, mjpeg, mmf, mov, mp2, mp3, mp4, mpeg, mpeg1video, mpeg2video, mpegts, mpjpeg, mulaw, nut, ogg, oss, psp, rawvideo, rm, rtp, s16be, s16le, s8, svcd, swf, u16be, u16le, u8, vcd, vob, voc, wav, yuv4mpegpipe,

}
