package edu.ycu.hockshop.marmot.media;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.marmot.feature.AudioFeature;
import edu.ycu.hockshop.marmot.feature.MultimediaFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import edu.ycu.hockshop.marmot.utils.aip.BaiduApiClient;
import it.sauronsoftware.jave.AudioInfo;
import it.sauronsoftware.jave.Encoder;

import java.io.File;

/**
 * 音频特征提取.
 * @author wangruihuan
 * @since 2018-06-27
 */
public class Audio2 extends BaseExtractableFile implements Extractable<AudioFeature>{

    private Encoder encoder;
    public Audio2(File file) {
        super(file);
        this.encoder=new Encoder();
    }

    @Override
    public AudioFeature extract(String[] featureNames) throws Exception {

        MultimediaFeature mfeature =  new Multimedia(file).extract(featureNames);
        AudioFeature feature = JSON.parseObject(JSON.toJSONString(mfeature),AudioFeature.class);
        AudioInfo info=encoder.getInfo(file).getAudio();
        for (String featureName:featureNames){
            switch (featureName){
                case "channels":feature.setChannels(info.getChannels());break;
                case "decoder":feature.setDecoder(info.getDecoder());break;
                case "bitRate":feature.setBitRate(info.getBitRate());break;
                case "samplingRate":feature.setSamplingRate(info.getSamplingRate());break;
            }
        }
        return feature;
    }

    @Override
    public Class<AudioFeature> featureClass() {
        return AudioFeature.class;
    }
}
