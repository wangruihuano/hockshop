package edu.ycu.hockshop.marmot.utils.aip;

import com.baidu.aip.speech.AipSpeech;
import edu.ycu.hockshop.marmot.media.AudioFormat;
import lombok.NonNull;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 需要上传完整语音文件进行识别，时长不超过60s，支持、自定义词库设置
 * pcm（不压缩）、wav（不压缩，pcm编码）、amr（压缩格式）
 *
 * @author zql
 * @date 2018.6.13
 */
public class AipUtils {

    private static final String APP_ID = "11394088";
    private static final String API_KEY = "qnqiohLMvsozHm9gdeSQTyl6";
    private static final String SECRET_KEY = "KLrEx4eGBvaV37SKhibPETVepGB7h1U6";

    /**
     * @param list
     * @return
     */
    public static String requestAipServer(@NonNull List<File> list) {
        // 初始化一个AipSpeech
        AipSpeech client = new AipSpeech(APP_ID, API_KEY, SECRET_KEY);
        System.out.println(list.get(0).getAbsolutePath());
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(4000);
        client.setSocketTimeoutInMillis(180000);
        return list.stream().parallel().map(file -> {
            JSONObject jsonObject = client.asr(file.getPath(), "pcm", 16000, null);
            if (!jsonObject.has("result")){
                return "";
            }
            String res = jsonObject.get("result").toString().replaceAll("\\pP", "");
            return res.toString();
        }).reduce("", String::concat);
    }

    /**
     * @param list
     * @param type
     * @return
     */
    public static String requestAipServer(@NonNull List<File> list, AudioFormat.type type, int rate) {
        // 初始化一个AipSpeech
        AipSpeech client = new AipSpeech(APP_ID, API_KEY, SECRET_KEY);
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(4000);
        client.setSocketTimeoutInMillis(120000);
        return list.stream().parallel().map(file -> {
            JSONObject jsonObject = client.asr(file.getPath(), type.toString().toLowerCase(), rate, null);
            if (!jsonObject.has("result")){
                return "";
            }
            String res = jsonObject.get("result").toString().replaceAll("\\pP", "");
            return res.toString();
        }).reduce("", String::concat);
    }

    /**
     * @param list
     * @return
     */
    public static Future<String> requestAipServerAsync(@NonNull List<File> list) {
        // 初始化一个AipSpeech
        AipSpeech client = new AipSpeech(APP_ID, API_KEY, SECRET_KEY);
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(4000);
        client.setSocketTimeoutInMillis(120000);
        CompletableFuture<String> futureContent = new CompletableFuture<>();

        new Thread(() -> {
            try {
                String content = list.stream().parallel().map(file -> {
                    JSONObject jsonObject = client.asr(file.getPath(), "wav", 16000, null);
                    if (!jsonObject.has("result")){
                        return "";
                    }
                    String res = jsonObject.get("result").toString().replaceAll("\\pP", "");
                    return res.toString();
                }).reduce("", String::concat);

                futureContent.complete(content);
            } catch (Exception ex) {
                futureContent.completeExceptionally(ex);
            }
        }).start();
        return futureContent;
    }

}
