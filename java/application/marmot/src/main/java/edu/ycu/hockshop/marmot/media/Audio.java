package edu.ycu.hockshop.marmot.media;

import edu.ycu.hockshop.marmot.utils.AudioUtils;
import edu.ycu.hockshop.marmot.utils.VideoUtils;
import edu.ycu.hockshop.marmot.utils.aip.AipUtils;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.audio.mp3.MP3FileReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.function.Function;

import static edu.ycu.hockshop.marmot.utils.AudioUtils.convertToSpecificFormat;
import static java.util.stream.Collectors.toList;


/**
 * @author zql
 * @since 2018.6.7
 */
public interface Audio {

    /**
     * Verify the file And convert to be end with 'mp3'
     *
     * @param file processing file
     * @return The file is end with 'mp3'
     * @throws Exception The file does't exists.
     */
    default File convertOrSendBackMp3File(File file) throws Exception {
        return file.getName().endsWith("mp3") ? VideoUtils.transcodingMp3(file) : file;
    }

    /**
     * To get summary from the file supplied.
     *
     * @param file     The file is end with 'mp3'
     * @param function Function
     * @return AudioFeature` sub class.
     * @throws Exception The exception when the audio files try to be processed.
     */
    default <R> R getSummary(File file, Function<MP3File, R> function) throws Exception {
        MP3File mp3File = (MP3File) new MP3FileReader().read(file);
        return function.apply(mp3File);
    }

    /**
     * To parse ID3v2Tag
     *
     * @param s String
     * @return String
     */
    default String parseID3V2Tag(String s) {
        String[] split = s.replaceAll("\"", "").split("=");
        return split[1].replace(";", "").trim();
    }

    /**
     * Convert and get audio's contents.
     *
     * @param file The audio file.
     * @param type The audio obtains contents.
     * @return Future<String>
     */
    default Future<String> convertAudioToWordsAsnyc(@NonNull File file, AudioFormat.type type) {

        CompletableFuture<String> futureContent = new CompletableFuture<>();

        new Thread(() -> {

            try {
                // To spilte files.
                List<File> sample = AudioUtils.spiltAudioFile(file, 60,"mp3");
                // Convert specfic format that is suitable for Aip server.
                List<File> res = sample.stream().map(f -> convertToSpecificFormat(f, type)).collect(toList());

//                List<File> res = new ArrayList<>();
//                for (File file1:sample){
//                    System.out.println(file1.getAbsoluteFile());
//                    file1.createNewFile();
//                    File f2=convertToSpecificFormat(file1,type);
//                    res.add(f2);
//                    System.out.println(f2.getAbsoluteFile());
//                }
//                FileUtils.copyFile(sample.get(0),new File("test"));


                if (res == null || res.size() == 0) {
                    throw new NullPointerException();
                }

                String content = AipUtils.requestAipServer(res);

                futureContent.complete(content);

            } catch (Exception ex) {
                futureContent.completeExceptionally(ex);
            }
        }).start();
        //
//        AudioUtils.cleanTemporaryFilesAsnyc(file);
        // To clean all tempertory files
        // AudioUtils.cleanTemporaryFiles(file);
        return futureContent;
    }

    /**
     * Convert and get audio's contents.
     *
     * @param file The audio file.
     * @return The audio obtains contents.
     * @throws Exception exception
     */
    @Deprecated
    default String convertAudioToWords(@NonNull File file,String suffix) throws Exception {
        // TODO 文件的转换，切割，调用AIP服务

        // 调用转换方法，获取wav格式
        File sample = convertToSpecificFormat(file, AudioFormat.type.WAV);

        // 切分文件，时长小于60s
        List<File> res = AudioUtils.spiltAudioFile(sample, 60,suffix);

        if (res == null || res.size() == 0) {
            throw new NullPointerException();
        }

        return AipUtils.requestAipServer(res);
    }


    /**
     * @param file The audio file.
     * @return
     * @throws Exception Something exception is occered when method executes asynchronously.
     */
    @Deprecated
    default Future<String> convertAudioToWordsAsnyc(@NonNull File file,String suffix) throws Exception {

        CompletableFuture<String> futureContent = new CompletableFuture<>();

        new Thread(() -> {

            // 调用转换方法，获取wav格式
            File sample = null;
            try {
                sample = convertToSpecificFormat(file, AudioFormat.type.WAV);

                // 切分文件，时长小于60s
                List<File> res = AudioUtils.spiltAudioFile(sample, 60,suffix);

                if (res == null || res.size() == 0) {
                    throw new NullPointerException();
                }

                String content = AipUtils.requestAipServer(res);

                futureContent.complete(content);

            } catch (Exception ex) {
                futureContent.completeExceptionally(ex);
            }
        }).start();

        return futureContent;
    }

}
