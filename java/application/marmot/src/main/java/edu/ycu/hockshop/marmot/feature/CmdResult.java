package edu.ycu.hockshop.marmot.feature;


import lombok.*;

import java.io.Serializable;


/**
 * cmd执行结果.
 * @author dt
 * @since 2018-05-12
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CmdResult implements Serializable {

    private Boolean success;

    private String msg;

}
