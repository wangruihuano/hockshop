package edu.ycu.hockshop.marmot.media;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.marmot.feature.AudioFeature;
import edu.ycu.hockshop.marmot.feature.RecordFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import edu.ycu.hockshop.marmot.utils.aip.BaiduApiClient;
import org.apache.log4j.Logger;

import  edu.ycu.hockshop.marmot.utils.aip.BaiduApiClient.Lang;

import java.io.File;

/**
 * @author zql
 * @date 2018.6.18
 */
public class Record extends BaseExtractableFile implements Extractable<RecordFeature>, Audio {

    private static final Logger logger = Logger.getLogger(Record.class);
    public Record(File file) {
        super(file);
    }


    @Override
    public RecordFeature extract(String[] featureNames) throws Exception {

        if (file == null || featureNames == null || featureNames.length == 0) {
            return null;
        }

        AudioFeature audioFeature = new Audio2(file).extract(featureNames);
       RecordFeature feature = JSON.parseObject(JSON.toJSONString(audioFeature),RecordFeature.class);
       BaiduApiClient  client=BaiduApiClient.getClient();

       for (String featureName:featureNames){
           switch (featureName){
               case "PuTongHuaWithSimEng":
                   feature.setPuTongHuaWithSimEng(client.recognition(file, Lang.PuTongHuaWithSimEng));
                   break;
               case "PuTongHua":
                   feature.setPuTongHua(client.recognition(file, Lang.PuTongHua));
                   break;
               case "English":
                   feature.setEnglish(client.recognition(file, Lang.English));
                   break;
               case "Cantonese":
                   feature.setCantonese(client.recognition(file, Lang.Cantonese));
                   break;
               case "SichuanHua":
                   feature.setSichuanHua(client.recognition(file, Lang.SichuanHua));
                   break;
               case "PuTongHuaFarField":
                   feature.setPuTongHuaFarField(client.recognition(file, Lang.PuTongHuaFarField));
                   break;
           }
       }

        return feature;
    }

    @Override
    public Class<RecordFeature> featureClass() {
        return RecordFeature.class;
    }
}
