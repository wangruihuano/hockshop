package edu.ycu.hockshop.marmot;


import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class MarmotProvider {

    public static void main(String[] args) {
        new SpringApplicationBuilder(MarmotProvider.class)
                .web(WebApplicationType.NONE)
                .run(args);
    }
}
