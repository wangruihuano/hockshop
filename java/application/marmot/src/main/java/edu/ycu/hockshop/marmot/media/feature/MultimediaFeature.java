package edu.ycu.hockshop.marmot.media.feature;

import lombok.Data;

/**
 * 数字媒体特征.
 * @author wangruihuan
 * @since 2018-06-27
 */
@Data
public class MultimediaFeature {

    // 格式
    protected String format;

    // 时间长度
    protected Long duration;


}
