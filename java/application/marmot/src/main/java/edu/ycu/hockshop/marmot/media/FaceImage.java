package edu.ycu.hockshop.marmot.media;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.marmot.feature.Face;
import edu.ycu.hockshop.marmot.feature.FaceImgFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Properties;

/**
 * 人脸特征提取.
 *
 * @author wangruihuan
 * @since 2018-06-19
 */
public class FaceImage extends BaseExtractableFile implements Extractable<FaceImgFeature> {

    private static final Logger logger = LoggerFactory.getLogger(FaceImage.class);

    // 人脸特征提取url
    private String url;

    public FaceImage(File file) {
        super(file);
        init();
    }

    private void init() {
        if (file != null) {
            Properties properties = new Properties();
            // 使用ClassLoader加载properties配置文件生成对应的输入流
            InputStream in = FaceImage.class.getClassLoader().getResourceAsStream("config.properties");
            // 使用properties对象加载输入流
            try {
                properties.load(in);
                //获取key对应的value值
                url = properties.getProperty("FaceAPI_URL") + "/face";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param featureNames 特征名称
     * @return
     * @throws Exception
     */
    @Override
    public FaceImgFeature extract(String[] featureNames) throws Exception {
        if (featureNames == null || featureNames.length <= 0) {
            return null;
        }

        FaceImgFeature imgFeature = new FaceImgFeature();
        BufferedImage image = ImageIO.read(file);

        for (String feature : featureNames) {
            switch (feature) {
                case "faces":
                    String filePath = URLEncoder.encode(file.getAbsolutePath(), "utf8");
                    logger.info("文件在本机的路径" + filePath);
                    List<Face> faces = JSON.parseArray(Request.Get(url + "?file=" + file.getAbsolutePath()).execute().returnContent().asString(), Face.class);
                    imgFeature.setFaces(faces);
                    break;
                case "width":
                    imgFeature.setWidth(image.getWidth());
                    break;
                case "height":
                    imgFeature.setHeight(image.getHeight());
                    break;
            }
        }
        return imgFeature;
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public Class<FaceImgFeature> featureClass() {
        return FaceImgFeature.class;
    }
}
