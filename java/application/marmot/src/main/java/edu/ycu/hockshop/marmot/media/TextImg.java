package edu.ycu.hockshop.marmot.media;

import edu.ycu.hockshop.marmot.feature.TextImgFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import lombok.Setter;
import net.sourceforge.tess4j.ITessAPI;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.Word;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

/**
 * The image that we want to do OCR .
 * @author wangruihuan
 * @since 2018-05-05
 */
public class TextImg extends BaseExtractableFile implements Extractable<TextImgFeature> {

    public TextImg(File file) {
        super(file);
    }


    @Setter private String datapath = langPath();

    @Override
    public TextImgFeature extract(String[] featureNames) throws Exception {

        //解码数据
        BufferedImage bufferedImage = ImageIO.read(file);

        //获取文字识别对象实例
        Tesseract tesseract = new Tesseract();
        tesseract.setDatapath(datapath);
        tesseract.setLanguage("eng");

        //处理图像
        //TODO

        TextImgFeature textImg = new TextImgFeature();
        boolean doWords = false;

        for (String feature : featureNames) {
            switch (feature) {
                case "width":
                    textImg.setWidth(bufferedImage.getWidth());
                    break;
                case "height":
                    textImg.setHeight(bufferedImage.getHeight());
                    break;
                case "words":
                    doWords = true;
                    break;
                case "chi_sim":
                    tesseract.setLanguage("chi_sim");
                    textImg.setChi_sim(tesseract.doOCR(bufferedImage));
                    break;
                case "eng":
                    tesseract.setLanguage("eng");
                    textImg.setEng(tesseract.doOCR(bufferedImage));
                    break;
                case "jpn":
                    tesseract.setLanguage("jpn");
                    textImg.setJpn(tesseract.doOCR(bufferedImage));
                    break;
                case "rus":
                    tesseract.setLanguage("rus");
                    textImg.setRus(tesseract.doOCR(bufferedImage));
                    break;
                case "kor":
                    tesseract.setLanguage("kor");
                    textImg.setKor(tesseract.doOCR(bufferedImage));
                    break;
            }
        }

        if (doWords) {
            List<Word> words = tesseract.getWords(bufferedImage, ITessAPI.TessPageIteratorLevel.RIL_SYMBOL);
            textImg.setWords(words);
        }

        return textImg;
    }

    /**
     * 获取语言包路径.
     *
     * @return
     */
    private static String langPath() {
        File trainData = new File(TextImg.class.getClassLoader().getResource("tessdata").getFile());
        System.out.println(trainData.getParent());
        return trainData.getAbsolutePath();
    }

    @Override
    public Class featureClass() {
        return TextImgFeature.class;
    }
}
