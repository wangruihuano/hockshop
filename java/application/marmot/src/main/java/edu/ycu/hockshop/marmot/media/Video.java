package edu.ycu.hockshop.marmot.media;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.marmot.feature.MultimediaFeature;
import edu.ycu.hockshop.marmot.feature.VideoFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.VideoInfo;

import java.io.File;

/**
 * 视频特征提取.
 * @author wangruihuan
 * @since 2018-06-28
 */
public class Video extends BaseExtractableFile implements Extractable<VideoFeature> {

    public Video(File file) {
        super(file);
    }

    @Override
    public VideoFeature extract(String[] featureNames) throws Exception {
        MultimediaFeature multimediaFeature = new Multimedia(file).extract(featureNames);
        VideoFeature videoFeature = JSON.parseObject(JSON.toJSONString(multimediaFeature),VideoFeature.class);
        Encoder encoder = new Encoder();
        VideoInfo info=encoder.getInfo(file).getVideo();
        for (String featureName:featureNames){
            switch (featureName){
                case "width":videoFeature.setWidth(info.getSize().getWidth());break;
                case "height":videoFeature.setHeight(info.getSize().getHeight());break;
                case "decoder":videoFeature.setDecoder(info.getDecoder());break;
                case "bitRate":videoFeature.setBitRate(info.getBitRate());break;
                case "frameRate":videoFeature.setFrameRate(info.getFrameRate());break;
            }
        }
        return videoFeature;
    }

    @Override
    public Class<VideoFeature> featureClass() {
        return VideoFeature.class;
    }
}
