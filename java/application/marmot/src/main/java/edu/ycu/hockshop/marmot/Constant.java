package edu.ycu.hockshop.marmot;

/**
 * 本应用常量.
 * @author wangruihuan
 * @since 2018-06-27
 */
public interface Constant {

    // 配置文件名称
    String CONFIG_PROPERTIES="config.properties";

    /**
     * 配置文件中的键名
     */
    String APP_ID = "APP_ID";
    String TMP_DIR="Tmp_Dir";
    String API_KEY="API_KEY";
    String SECRET_KEY="SECRET_KEY";
    String CONNECTION_TIMEOUT_IN_MILLIS = "ConnectionTimeoutInMillis";
    String SOCKET_TIMEOUT_IN_MILLIS = "SocketTimeoutInMillis";

}
