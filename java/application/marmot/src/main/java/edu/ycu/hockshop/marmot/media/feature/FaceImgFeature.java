package edu.ycu.hockshop.marmot.media.feature;

import lombok.*;

import java.util.List;

@Setter
@Getter
@ToString
public class FaceImgFeature extends BaseImgFeature{

    //脸部特征
    List<Face> faces;
}


/**
 * 矩形.
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
class Rect{
    private int top;
    private int left;
    private int bottom;
    private int right;

    public Rect(int top, int left, int bottom, int right) {
        this.top = top;
        this.left = left;
        this.bottom=bottom;
        this.right = right;
    }
}

/**
 * 点位置.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class Point{

    private int x;
    private int y;
}

