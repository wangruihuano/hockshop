package edu.ycu.hockshop.marmot.utils.ffmpeg;

/**
 * 编码格式.
 * @author wangruihuan
 * @since 2018-06-28
 */
public enum Codec {

    ac3, adpcm_adx, adpcm_ima_wav, adpcm_ms, adpcm_swf, adpcm_yamaha, flac, g726, libfaac, libmp3lame, mp2, pcm_alaw, pcm_mulaw, pcm_s16be, pcm_s16le, pcm_s24be, pcm_s24daud, pcm_s24le, pcm_s32be, pcm_s32le, pcm_s8, pcm_u16be, pcm_u16le, pcm_u24be, pcm_u24le, pcm_u32be, pcm_u32le, pcm_u8, pcm_zork, roq_dpcm, sonic, sonicls, vorbis, wmav1, wmav2,

}
