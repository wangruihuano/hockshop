package edu.ycu.hockshop.marmot.utils;


import edu.ycu.hockshop.marmot.feature.CmdResult;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 视频文件工具类.
 * @author dt
 * @since 2018-05-10
 */
public class VideoUtils {

    private static long blockSize = 4*1024*1024;


    /**
     * 视频提取音频
     * @param filePath　文件路径
     * @param outPath　文件输出路径
     * @return cmd执行结果
     */
    public static CmdResult AudioExtraction(String filePath,String outPath) throws FileNotFoundException{
        File file = new File(filePath);
        if (!file.exists()) {
            throw new FileNotFoundException(filePath + "文件不存在");
        }
        String fileName[] = file.getName().split("\\.");
        List<String> commend = new ArrayList<String>();
        commend.add("ffmpeg");
        commend.add("-i");
        commend.add(filePath);
        commend.add("-f");
        commend.add("mp3");
        commend.add("-vn");
        commend.add(outPath + "/" + fileName[0] + ".mp3");

        CmdResult cmdResult = runCommand(commend);
        return cmdResult;
    }


    /**
     * 视频提取关键帧
     * @param filePath 文件路径
     * @param outPath  文件输出路径
     * @return cmd执行结果
     * @throws FileNotFoundException　
     */
    public static CmdResult ExtractKeyframes(String filePath,String outPath) throws FileNotFoundException{
        File file = new File(filePath);
        if (!file.exists()){
            throw new FileNotFoundException(filePath + "文件不存在");
        }
        String filename[] = file.getName().split("\\.");
        String fileResolution = getVideoResolution(filePath);
        List<String> commend = new ArrayList<String>();
        commend.add("ffmpeg");
        commend.add("-i");
        commend.add(filePath);
        commend.add("-vf");
        commend.add("select='eq(pict_type\\,I)'");
        commend.add("-vsync");
        commend.add("2");
        commend.add("-s");
        commend.add(fileResolution);
        commend.add("-f");
        commend.add("image2");
        commend.add(outPath+"/"+filename[0]+"-%02d.jpeg");

        CmdResult cmdResult = runCommand(commend);
        return cmdResult;
    }





    /**
     * 切割视频
     * @param filePath 文件路径
     * @return cmd执行结果
     * @throws Exception
     */
    public static CmdResult cutVideo(String filePath,String outPath) throws Exception{
        File file = new File(filePath);
        CmdResult cmdResult = new CmdResult(false,"");
        if (!file.exists()){
            throw new FileNotFoundException(filePath + "文件不存在");
        }
        if (!filePath.endsWith(".mp4")){
            file = transcodingMP4(file);
        }

        //从ffmpeg获得的时间长度00:00:00格式
        String videoTimeString = getVideoTime(file);
        //将时长转换为秒数
        int videoSecond = parseTimeToSecond(videoTimeString);
        //视频文件的大小
        long fileLength = getVideoFileLength(file);

        if (fileLength <= blockSize) {//如果视频文件大小不大于预设值，则直接返回原视频文件
            cmdResult.setSuccess(true);
            cmdResult.setMsg("文件不需要切割");
        }else {//如果超过预设大小，则需要切割
            int partNum = (int) (fileLength / blockSize);//文件大小除以分块大小的商
            long remainSize = fileLength % blockSize;//余数
            int cutNum;
            if (remainSize > 0) {
                cutNum = partNum + 1;
            } else {
                cutNum = partNum;
            }
            int eachPartTime = (videoSecond-1) / cutNum;
            List<String> commands = new ArrayList<String>();
            String fileName[] = file.getName().split("\\.");
            for (int i = 0; i < cutNum; i++) {
                commands.add("ffmpeg");
                commands.add("-ss");
                commands.add(parseTimeToString(eachPartTime * i));
                if (i != cutNum - 1) {
                    commands.add("-t");
                    commands.add(parseTimeToString(eachPartTime));
                }
                commands.add("-accurate_seek");
                commands.add("-i");
                commands.add(filePath);
                commands.add("-codec");
                commands.add("copy");
                commands.add(outPath + File.separator + fileName[0] + "_part" + i + "." + fileName[1]);
                cmdResult = runCommand(commands);
                commands.clear();
            }
        }

            return cmdResult;
    }


    /**
     * 音频转码mp3
     * @param file 转码文件
     * @return
     * @throws Exception
     */
    public static File transcodingMp3(File file) throws Exception{
        String filePath = file.getAbsolutePath();
        if (!file.exists()) {
            throw new FileNotFoundException(filePath + "文件不存在");
        }
        if (filePath.endsWith(".mp3")){
            throw new Exception("文件格式错误!");
        }
        String filename[] = filePath.split("\\.");
        String outPath = filename[0]+".mp3";
        List<String> commend = new ArrayList<>();
        commend.add("ffmpeg");
        commend.add("-i");
        commend.add(filePath);
        commend.add("-acodec");
        commend.add("libmp3lame");
        commend.add(outPath);

        CmdResult cmdResult = runCommand(commend);
        return cmdResult.getSuccess() ? new File(outPath) : null;
    }


    /**
     * 音频转码WAV
     * @param file 转码文件
     * @return
     * @throws Exception
     */
    public static File transcodingWAV(File file) throws Exception{
        String filePath = file.getPath();
        if (!file.exists()){
            throw new FileNotFoundException(filePath +"文件不存在!");
        }
        if (filePath.endsWith(".wav")){
            throw new Exception("文件格式错误!");
        }

        String filename[] = filePath.split("\\.");
        String outPath = filename[0]+".wav";

        List<String> commends = new ArrayList<>();
        commends.add("ffmpeg");
        commends.add("-i");
        commends.add(filePath);
        commends.add("-acodec");
        commends.add("pcm_s16le");
        commends.add("-f");
        commends.add("s16");
        commends.add("-ac");
        commends.add("2");
        commends.add("-ar");
        commends.add("16000");
        commends.add(outPath);

        CmdResult cmdResult = runCommand(commends);
        return cmdResult.getSuccess()? new File(outPath) : null;
    }


    /**
     * 视频转码mp4
     * @param file 转码文件
     * @return
     * @throws FileNotFoundException
     */
    public static File transcodingMP4(File file) throws FileNotFoundException {
        String filePath = file.getPath();
        if (!file.exists()){
            throw new FileNotFoundException(filePath + "文件不存在!");
        }
        String filename[] = filePath.split("\\.");
        String outPath = filename[0] + ".mp4";
        List<String> commands = new ArrayList<>();
        commands.add("ffmpeg");
        commands.add("-i");
        commands.add(filePath);
        commands.add("-b");
        commands.add(getVideoBitrate(filePath) + "k");
        commands.add("-s");
        commands.add(getVideoResolution(filePath));
        commands.add("-codec");
        commands.add("copy");
        commands.add(outPath);

        CmdResult cmdResult = runCommand(commands);
        return cmdResult.getSuccess()? new File(outPath) : null;
    }


    /**
     * 获取视频分辨率
     * @param filePath 文件路径
     * @return
     */
    private static String getVideoResolution(String filePath) {
        List<String> commend = new ArrayList<>();
        commend.add("ffmpeg");
        commend.add("-i");
        commend.add(filePath);
        CmdResult cmdResult = runCommand(commend);
        String msg = cmdResult.getMsg().toString();
        String regex = "Video: (.*?), (.*?), (.*?)[,\\s]";
        String fileResolution = "";
        if (cmdResult.getSuccess()){
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(msg);
            while (matcher.find()) {
                fileResolution =  matcher.group(3);
            }
            return fileResolution;
        } else {
            return "";
        }
    }


    /**
     * 获取音视频文件的比特率
     * @param filePath 文件路径
     * @return
     */
    private static String getVideoBitrate(String filePath){
        List<String> commend = new ArrayList<>();
        commend.add("ffmpeg");
        commend.add("-i");
        commend.add(filePath);
        CmdResult cmdResult = runCommand(commend);
        String msg = cmdResult.getMsg().toString();
        String regex = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
        String fileBitrate = "";
        if (cmdResult.getSuccess()){
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(msg);
            while (matcher.find()) {
                fileBitrate = matcher.group(3);
            }
            return fileBitrate;
        } else {
            return null;
        }
    }

    /**
     * 将秒表示时长转为00:00:00格式
     * @param second 秒数时长
     * @return 字符串格式时长
     */
    private static String parseTimeToString(int second) {
        int end = second % 60;
        int mid = second / 60;
        if (mid < 60) {
            return mid + ":" + end;
        } else if (mid == 60) {
            return "1:00:" + end;
        } else {
            int first = mid / 60;
            mid = mid % 60;
            return first + ":" + mid + ":" + end;
        }
    }


    /**
     * 获取视频文件长度
     * @param file　文件
     * @return 视频文件长度
     * @throws FileNotFoundException
     */
    private static long getVideoFileLength(File file) throws FileNotFoundException{
        if (!file.exists()) {
            throw new FileNotFoundException(file.getAbsolutePath() + "不存在");
        }
        return file.length();
    }



    /**
     * 将字符串时间格式转换为整型，以秒为单位
     * @param timeString 字符串时间时长
     * @return 时间所对应的秒数
     */
    private static int parseTimeToSecond(String timeString) {
        Pattern pattern = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
        Matcher matcher = pattern.matcher(timeString);
        if (!matcher.matches()) {
            try {
                throw new Exception("时间格式不正确");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String[] time = timeString.split(":");
        return Integer.parseInt(time[0]) * 3600 + Integer.parseInt(time[1]) * 60 + Integer.parseInt(time[2]);
    }




    /**
     * 获取视频文件时长
     * @param file　文件
     * @return  时长 格式hh:MM:ss
     * @throws FileNotFoundException
     */
    private static String getVideoTime(File file) throws FileNotFoundException{
        if (!file.exists()){
            throw new FileNotFoundException(file.getAbsolutePath() + "文件不存在!");
        }
        List<String> commend = new ArrayList<String>();
        commend.add("ffmpeg");
        commend.add("-i");
        commend.add(file.getAbsolutePath());
        CmdResult cmdResult = runCommand(commend);
        String msg = cmdResult.getMsg();
        if (cmdResult.getSuccess()){
            Pattern pattern = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
            Matcher matcher = pattern.matcher(msg);
            String time = "";
            while (matcher.find()) {
                time = matcher.group();
            }
            return time;
        } else {
            return "";
        }
    }



    /**
     * 执行cmd　
     * @param command　cmd指令集
     * @return cmd执行结果
     */
    protected static synchronized CmdResult runCommand(List<String> command) {
        CmdResult cmdResult = new CmdResult(false, "");
        ProcessBuilder builder = new ProcessBuilder(command);
        builder.redirectErrorStream(true);
        try {
            Process process = builder.start();
            final StringBuilder stringBuilder = new StringBuilder();
            final InputStream inputStream = process.getInputStream();
            new Thread(new Runnable() {//启动新线程为异步读取缓冲器，防止线程阻塞

                @Override
                public void run() {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            stringBuilder.append(line);
                        }
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                }
            }).start();
            process.waitFor();
            cmdResult.setSuccess(true);
            cmdResult.setMsg(stringBuilder.toString());
        } catch (Exception e) {
            throw new RuntimeException("ffmpeg执行异常" + e.getMessage());
        }
        return cmdResult;
    }


}
