package edu.ycu.hockshop.marmot.feature;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author zql
 * @since 2018.6.7
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class AudioFromVideoFeature extends AudioFeature implements Serializable {

}
