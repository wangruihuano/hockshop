package edu.ycu.hockshop.marmot.feature;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 脸部特征.
 */
@Getter
@Setter
@ToString
public class Face implements Serializable {

    /**
     * 人脸位置
     */
    private Rect rect;

    /**
     * 人脸特征点位置.
     */
    private Point[] points;
}
