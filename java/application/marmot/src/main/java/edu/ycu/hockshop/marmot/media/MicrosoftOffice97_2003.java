package edu.ycu.hockshop.marmot.media;

import edu.ycu.hockshop.marmot.feature.TextFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import org.apache.poi.POIOLE2TextExtractor;
import org.apache.poi.extractor.ExtractorFactory;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.File;
import java.io.FileInputStream;

/**
 * 微软Office文档信息提取.
 * @author wangruihuan
 * @since 2018-06-29
 */
public class MicrosoftOffice97_2003 extends BaseExtractableFile implements Extractable<TextFeature> {


    public MicrosoftOffice97_2003(File file) {
        super(file);
    }

    @Override
    public TextFeature extract(String[] featureNames) throws Exception {
        if (featureNames==null || featureNames.length==0){
            return null;
        }
        POIOLE2TextExtractor textExtractor =
                ExtractorFactory.createExtractor(new POIFSFileSystem(new FileInputStream(file)));
        TextFeature feature = new TextFeature();
        for (String name:featureNames) {
            switch (name){
                case "content":
                    feature.setContent(textExtractor.getText());
                    break;
                case "length":
                    feature.setLength(feature.getContent().length());
                    break;
            }
        }
        return feature;
    }

    @Override
    public Class<TextFeature> featureClass() {
        return TextFeature.class;
    }
}
