package edu.ycu.hockshop.marmot.feature;

import lombok.Data;

import java.io.Serializable;

/**
 * 视频基本特征.
 * @author wangruihuan
 * @since 2018-06-27
 */
@Data
public class VideoFeature extends MultimediaFeature implements Serializable {

    // 宽
    protected Integer width;

    // 高
    protected Integer height;

    // 解码器
    protected String decoder;

    // 比特率
    protected Integer bitRate;

    // 帧率
    protected Float frameRate;
}
