package edu.ycu.hockshop.marmot.media.feature;

import lombok.Getter;
import lombok.Setter;

/**
 * 图像共有特征类.
 */
@Getter
@Setter
public abstract class BaseImgFeature {

    //图像宽度
    protected Integer width;

    //图像高度
    protected Integer height;

}
