package edu.ycu.hockshop.marmot.media;

import edu.ycu.hockshop.marmot.feature.MultimediaFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.MultimediaInfo;

import java.io.File;

/**
 * 基础数字媒体信息提取.
 * @author wangruihuan
 * @since 2018-06-27
 */
public class Multimedia extends BaseExtractableFile implements Extractable<MultimediaFeature> {

    public Multimedia(File file) {
        super(file);
    }

    @Override
    public MultimediaFeature extract(String[] featureNames) throws Exception {
        MultimediaFeature feature = new MultimediaFeature();
        Encoder encoder = new Encoder();
        MultimediaInfo info=encoder.getInfo(file);
        for (String featureName:featureNames){
            switch (featureName){
                case "format":feature.setFormat(info.getFormat());break;
                case "duration":feature.setDuration(info.getDuration());break;
            }
        }
        return feature;
    }

    @Override
    public Class<MultimediaFeature> featureClass() {
        return MultimediaFeature.class;
    }
}
