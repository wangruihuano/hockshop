package edu.ycu.hockshop.marmot.utils.ffmpeg;

import edu.ycu.hockshop.marmot.utils.aip.BaiduApiClient;
import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.EncodingAttributes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 音频处理工具类.
 * @author wangruihuan
 * @since 2018-06-27
 */
public class AudioUtils {

    /**
     * 切割并且转码.
     * @param file 要进行转码切割的文件
     * @param format 格式
     * @param dir 保存处理后文件的文件夹
     * @param codec 编码方式
     * @param duration 切割一份的长度,单位:秒
     * @return 切割后的文件
     */
    public static List<File> splitAndEncode(File file, Format format, File dir, Codec codec,int duration) throws EncoderException {

        if (!dir.exists()){
            dir.mkdirs();
        }
        duration = duration*1000;

        Encoder encoder = new Encoder();
        Long srcDura=encoder.getInfo(file).getDuration();
        int num = (int) Math.ceil((double)srcDura/(double) duration);
        // 音频转码及切割

        //设置参数
        EncodingAttributes attributes = new EncodingAttributes();
        attributes.setFormat(format.name());
        AudioAttributes audioAttributes = new AudioAttributes();
        // 新文件的编码解码器的名称
        audioAttributes.setCodec(codec.name());
        audioAttributes.setBitRate(16);
        audioAttributes.setSamplingRate(16000);
        audioAttributes.setChannels(1);
        attributes.setDuration(Float.valueOf(duration)/1000);
        attributes.setAudioAttributes(audioAttributes);

        List<File> res = new ArrayList();
        for (int i = 0; i<num;i++){
            attributes.setOffset(Float.valueOf(i*duration/1000));
            File tmp=new File(dir.getAbsolutePath()+"/"+ UUID.randomUUID().toString());
            encoder.encode(file,tmp,attributes);
            res.add(tmp);
        }
        return res;
    }

    /**
     * 切割并转为PCM格式数据。
     * @param file 原始文件
     * @param duration 切割后单个文件长度,单位:秒
     * @param dir 切割后文件保存文件夹
     * @return 切割并转码后的文件年
     */
    public static  List<File> splitAndEncode2Pcm(File file,int duration, File dir) throws EncoderException {
        return splitAndEncode(file,Format.s16le,dir,Codec.pcm_s16le,duration);
    }
}
