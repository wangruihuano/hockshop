package edu.ycu.hockshop.marmot.media.feature;


import lombok.*;


/**
 * cmd执行结果.
 * @author dt
 * @since 2018-05-12
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CmdResult {

    private Boolean success;

    private String msg;

}
