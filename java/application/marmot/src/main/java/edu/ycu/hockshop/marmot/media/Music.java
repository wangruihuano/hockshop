package edu.ycu.hockshop.marmot.media;

import com.alibaba.fastjson.JSON;
import edu.ycu.hockshop.marmot.feature.AudioFeature;
import edu.ycu.hockshop.marmot.feature.MusicFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import edu.ycu.hockshop.marmot.utils.AudioUtils;
import org.apache.log4j.Logger;
import org.jaudiotagger.audio.mp3.MP3AudioHeader;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.Future;

/**
 * @author zql
 * @date 2018.5.11
 */
public class Music extends BaseExtractableFile implements Extractable<MusicFeature>, Audio {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Record.class);

    public Music(File file) {
        super(file);
    }


    @Override
    public MusicFeature extract(String[] featureNames) throws Exception {

        if (file == null || featureNames == null || featureNames.length == 0) {
            return null;
        }
        //
        File f = convertOrSendBackMp3File(file);

        // 异步请求
        Future<String> futureContent = convertAudioToWordsAsnyc(f, AudioFormat.type.PCM);

        MusicFeature summary = getSummary(f, musicFile -> {
            MP3AudioHeader header = musicFile.getMP3AudioHeader();
            MusicFeature musicFeature = new MusicFeature();
            HashMap<String, Object> frameMap = musicFile.getID3v2Tag().frameMap;

            musicFeature.setAlbum(parseID3V2Tag(frameMap.get("TALB").toString()));
            musicFeature.setSinger(parseID3V2Tag(frameMap.get("TPE1").toString()));
            musicFeature.setSongName(parseID3V2Tag(frameMap.get("TIT2").toString()));


            return musicFeature;

        });

        AudioFeature audioFeature = new Audio2(file).extract(featureNames);
        MusicFeature feature = JSON.parseObject(JSON.toJSONString(audioFeature),MusicFeature.class);


        for (String string : featureNames) {
            switch (string) {
                case "singer":
                    feature.setSinger(summary.getSinger());
                    break;
                case "songName":
                    feature.setSongName(summary.getSongName());
                    break;
                case "album":
                    feature.setAlbum(summary.getAlbum());
                    break;
            }
        }

        // To clean all tempertory files
        AudioUtils.cleanTemporaryFiles(file);

        return feature;
    }

    @Override
    public Class<MusicFeature> featureClass() {
        return MusicFeature.class;
    }


}
