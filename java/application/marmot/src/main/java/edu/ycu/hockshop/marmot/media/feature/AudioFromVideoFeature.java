package edu.ycu.hockshop.marmot.media.feature;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zql
 * @since 2018.6.7
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class AudioFromVideoFeature extends AudioFeature {

}
