package edu.ycu.hockshop.marmot.media;


import edu.ycu.hockshop.marmot.feature.PDFFeature;
import edu.ycu.hockshop.marmotStandard.BaseExtractableFile;
import edu.ycu.hockshop.marmotStandard.Extractable;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;

/**
 *  PDF文件处理.
 *  @author wangruihuan
 *  @since 2018-06-21
 */
public class PDF extends BaseExtractableFile implements Extractable<PDFFeature>{

    public PDF(File file) {
        super(file);
    }

    @Override
    public PDFFeature extract(String[] featureNames) throws Exception {
        PDFFeature feature = new PDFFeature();
        PDDocument document=PDDocument.load(file);
        for (String featureName:featureNames) {
            switch (featureName){
                case "version":feature.setVersion(document.getVersion());break;
                case "pageNumber":feature.setPageNumber(document.getNumberOfPages());break;
                case "content":
                    PDFTextStripper textStripper = new PDFTextStripper();
                    feature.setContent(textStripper.getText(document));
                    break;
            }
        }
        document.close();
        return feature;
    }

    @Override
    public Class<PDFFeature> featureClass() {
        return PDFFeature.class;
    }
}
