package edu.ycu.hockshop.marmot.feature;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 图像共有特征类.
 */
@Getter
@Setter
public abstract class BaseImgFeature implements Serializable {

    //图像宽度
    protected Integer width;

    //图像高度
    protected Integer height;

}
