package edu.ycu.hockshop.marmot.media.feature;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.sourceforge.tess4j.Word;

import java.util.List;

/**
 * The image include text.
 * @author wangruihuan
 * @since 2018-05-05
 */
@Getter
@Setter
@ToString
public class TextImgFeature extends BaseImgFeature{


    //中文
    private String chi_sim;

    //英文
    private String eng;

    //日文
    private String jpn;

    // 俄文
    private String rus;

    //韩文
    private String kor;

    // 文字及其位置
    private List<Word> words;

}
