package edu.ycu.hockshop.marmot.feature;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@ToString
public class FaceImgFeature extends BaseImgFeature implements Serializable {

    //脸部特征
    List<Face> faces;
}


/**
 * 矩形.
 */
@Setter
@Getter
@NoArgsConstructor
@ToString
class Rect implements Serializable{
    private int top;
    private int left;
    private int bottom;
    private int right;

    public Rect(int top, int left, int bottom, int right) {
        this.top = top;
        this.left = left;
        this.bottom=bottom;
        this.right = right;
    }
}

/**
 * 点位置.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
class Point implements Serializable{

    private int x;
    private int y;
}

