package edu.ycu.hockshop.marmot.media.feature;

import lombok.Data;

/**
 * @author zql
 * @date 2018.6.18
 */
@Data
public class RecordFeature extends AudioFeature {

    // 带有简单英文的普通话
    private String PuTongHuaWithSimEng;

    // 纯中文普通话
    private String PuTongHua;

    // 英文
    private String English;

    //粤语
    private String Cantonese;

    // 四川话
    private String SichuanHua;

    // 远场模型的普通话
    private String PuTongHuaFarField;
}
