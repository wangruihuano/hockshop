package edu.ycu.hockshop.marmot.media.feature;

import lombok.Data;

/**
 * @author zql
 * @since 2018.6.6
 */

@Data
public class AudioFeature extends MultimediaFeature{

    /**
     * 音道
     */
    protected Integer channels;

    /**
     * 解码器
     */
    protected String decoder;

    /**
     * 比特率
     */
    protected Integer bitRate;

    /**
     * 采样率
     */
    protected Integer samplingRate;

}
