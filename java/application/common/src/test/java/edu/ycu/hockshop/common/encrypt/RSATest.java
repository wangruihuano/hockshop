package edu.ycu.hockshop.common.encrypt;

import edu.ycu.hockshop.common.util.encrypt.RSAUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.security.KeyPair;

import static edu.ycu.hockshop.common.util.encrypt.RSAUtils.createKeys;

/**
 * RSA工具测试.
 * @author wrh
 * @since 2018-04-17
 */
public class RSATest {

    private static Logger logger = Logger.getLogger(RSATest.class);

    /**
     * 密钥对生成测试.
     */
    @Test
    public void generateTest(){
        try {
            KeyPair keyPair = createKeys(1024);
            edu.ycu.hockshop.common.util.encrypt.KeyPair keyPair1 = new edu.ycu.hockshop.common.util.encrypt.KeyPair(keyPair);
            System.out.println(keyPair1);
        }catch (Exception e){
            logger.warn("生成密钥对测试异常",e);
        }
    }

    /**
     * 公钥加密测试.
     */
    @Test
    public void encryTest(){
        try {
            String s=RSAUtils.encryptByPublicKey("123456","MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDwCZtRBwcm57Qs8hGddmRJbPcTB6wmrQ0nsNun\n" +
                    "MLSD4WmmuQkTiqU4VC/QkBKZqBFMtoNfibShipC4kvZdRTdkNQQT4OU91nh93/YrORAcQrYAg2Y8\n" +
                    "7DhnxEDNOl6tyRvS2j5+Nj84aST3idVmWD1KoBjVM+JmbUWo8KuSCbcmnwIDAQAB");
            System.out.println(s);
        } catch (Exception e) {
            logger.warn("RSA加密错误",e);
        }
    }

}
