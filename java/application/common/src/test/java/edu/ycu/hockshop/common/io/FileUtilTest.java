package edu.ycu.hockshop.common.io;

import edu.ycu.hockshop.common.util.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 文件工具类测试.
 * @author wrh
 * @since 2018-04-21
 */
public class FileUtilTest {

    Logger logger = Logger.getLogger(FileUtilTest.class);
    File file = new File("test");
    {
        try {
            if (!file.exists()){
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 文件写入测试.
     */
    @Test
    public void writeStr2FileTest(){
        try {
            FileUtils.writeStr2File(file,"jejewejw");
        } catch (IOException e) {
            logger.warn("文件写入测试异常",e);
        }
    }

    @Test
    public void delTest(){
        try {
            byte[] bytes=FileUtils.getBytes(file);
            file.delete();
            System.out.println(new String(bytes));
        } catch (IOException e) {
            logger.warn("文件读取字节异常",e);
        }


    }

    @Test
    public void hostnameTest(){
        try {
            InetAddress address=InetAddress.getLocalHost();
            System.out.println(address.getCanonicalHostName());
            System.out.println(address.getHostAddress());
            System.out.println(address.getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }


    }

}
