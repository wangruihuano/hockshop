package edu.ycu.hockshop.common.io;

import edu.ycu.hockshop.common.model.DigitizedAssests;
import edu.ycu.hockshop.common.util.io.ObjectUtil;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.File;

/**
 * 序列化及反序列化测试.
 * @author wrh
 * @since 2018-05-02
 */
public class ObjectUtilTest {

    private Logger logger = Logger.getLogger(ObjectUtilTest.class);

    private static final String fileName = "test.ser";
    @Test
    public void serializeTest(){
        try {
            File file = new File(fileName);
            if (!file.exists()){
                file.createNewFile();
                System.out.println(file.getAbsolutePath());
            }
            DigitizedAssests assests = new DigitizedAssests();
            assests.setLast_id("last_id");
            assests.setUser("user");
            ObjectUtil.serialize(assests,file);
        }catch (Exception e){
            logger.debug("序列化测试异常",e);
        }

    }

    @Test
    public void deserializeTest(){
        try {
            Object assests= ObjectUtil.deserialize(new File(fileName));
            DigitizedAssests assests1 = (DigitizedAssests) assests;
            System.out.println(assests);
            System.out.println(assests1);
        }catch (Exception e){
            logger.debug("反序列化异常",e);
        }

    }
}
