package edu.ycu.hockshop.common.util.io;

/**
 * 文件类型.
 * @author wangruihuan
 * @since 2018-06-26
 */
public enum  FileType {
    MP3("49443303"),
    JPEG ("FFD8FF");
    //TODO 待添加其他种类


    String value;


    FileType(String value) {
        this.value = value;
    }


    public String  getValue() {
        return this.value;
    }
}
