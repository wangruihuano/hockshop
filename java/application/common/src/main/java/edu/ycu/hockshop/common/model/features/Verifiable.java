package edu.ycu.hockshop.common.model.features;

/**
 * 可校验接口.
 *
 * 包含计算MD5
 */
public interface Verifiable {

    /**
     * 计算MD5
     * @return
     */
    String calMD5();

    /**
     * 校验MD5
     * 判断改区块是否合法
     * @return
     */
    boolean isLegal();


}
