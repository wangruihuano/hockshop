package edu.ycu.hockshop.common.util.io;

import java.io.*;

/**
 * 序列化工具.
 * @author wrh
 * @since 2018-05-01
 */
public class ObjectUtil {

    /**
     * 序列化对象到文件中.
     * @param o 将要序列化的对象
     * @param file 要写入的文件
     * @throws IOException
     */
    public static void serialize(Object o, File file) throws IOException {

        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
        out.writeObject(o);
        out.close();
    }

    /**
     * 从文件中反序列化对象.
     * @param file 文件
     * @return 反序列化的对象
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Object deserialize(File file) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
        Object o =  in.readObject();
        in.close();
        return o;
    }

    private ObjectUtil() {
    }
}
