package edu.ycu.hockshop.common.model.features;


import edu.ycu.hockshop.common.model.DigitizedAssestsType;

/**
 * 提取信息接口
 *
 * @author wrh
 * @since 2018-03-10
 */
public interface ExtractInfo {

    /**
     * 计算文件类型
     *
     * @return
     */
    DigitizedAssestsType calFileType();

    /**
     * 提取文件信息
     * @return
     */
    void extract(String info);
}
