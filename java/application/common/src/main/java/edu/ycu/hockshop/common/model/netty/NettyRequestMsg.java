package edu.ycu.hockshop.common.model.netty;

import lombok.*;

import java.io.Serializable;

/**
 * 自定义netty请求消息.
 * @author wangruihuan
 * @since 2018-05-10
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class NettyRequestMsg implements Serializable {

    //请求id
    private String id;

    //操作码
    private NettyRequestCode code;

    //内容
    private String content;

    public enum NettyRequestCode{
        POST,//用于上传
        GET,//获取
        DEL//删除
    }
}
