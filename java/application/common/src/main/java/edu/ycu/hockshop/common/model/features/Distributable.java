package edu.ycu.hockshop.common.model.features;

import java.io.File;

/**
 *
 * 可分布式接口
 *
 * @author wrh
 * @since 2018-03-12
 */
public interface Distributable {

    /**
     * 解析出文件
     * @return
     */
    File[] resolveFiles();
}
