package edu.ycu.hockshop.common.util.encrypt;

import org.apache.log4j.Logger;
import sun.security.rsa.RSAPublicKeyImpl;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;

/**
 * RSA工具类.
 * @author wrh
 * @since 2018-04-16
 */
public class RSAUtils {

    private static Logger logger = Logger.getLogger(RSAUtils.class);

    public static final String CHARSET = "UTF-8";
    public static final String RSA_ALGORITHM = "RSA";

    /**
     * 创建密钥对.
     * @param keySize 密钥长度.
     * @return
     */
    public static KeyPair createKeys(int keySize) {
        //为RSA算法创建一个KeyPairGenerator对象
        KeyPair keyPair = null;
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(RSA_ALGORITHM);
            //初始化KeyPairGenerator对象,密钥长度
            kpg.initialize(keySize);
            keyPair= kpg.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            logger.error("加密类型错误",e);
        }

      return keyPair;
    }

    /**
     * 公钥加密.
     * @param data 要加密的数据.
     * @param publicKey 公钥
     * @return
     */
    public static String encryptByPublicKey(String data, RSAPublicKey publicKey)
            throws Exception {
        Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return Base64.encode(cipher.doFinal(data.getBytes()));
    }

    /**
     * 字符串格式公钥加密.
     * @param data 要加密的数据.
     * @param publicKey 公钥.
     * @return
     * @throws Exception
     */
    public static String encryptByPublicKey(String data, String  publicKey)
            throws Exception {
        Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
        RSAPublicKey rsaPublicKey = new RSAPublicKeyImpl(Base64.decode2Byte(publicKey));
        cipher.init(Cipher.ENCRYPT_MODE, rsaPublicKey);
        return Base64.encode(cipher.doFinal(data.getBytes()));
    }

}