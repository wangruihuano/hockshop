package edu.ycu.hockshop.common.model;

import edu.ycu.hockshop.common.util.encrypt.MD5;

import java.io.Serializable;

/**
 * 数字资产模型.
 *
 * @author wrh
 * @since 2018-03-08
 */

public class DigitizedAssests implements Serializable {

    //上一个区块的id
    private String last_id;

    //当前区块id
    private String id;

    //时间戳
    private Long timestamp;

    //所有者
    private String user;

    //文件名
    private String file_name;


    //是否解析过
    private Boolean parsed;

    //操作码
    private OprateCode oprate_code;

    //数字资产类型
    private DigitizedAssestsType assets_type;

    //数字资产(文件)的MD5值
    private String md5;

    //数字资产的关键信息
    private String info;

    //保留,备用
    private String other;

    public String getInfo_md5() {
        return info_md5;
    }

    //关键信息的md5
    private String info_md5;

    //解析资产文件对应的全类名
    private String classname;

    /**
     * 操作码枚举类型.
     *
     * @author wangruihuan
     * @since 2018-03-28
     */
    public enum OprateCode {
        ADD,//添加
        DEL,//删除
        UPDATE//更新
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getLast_id() {
        return last_id;
    }

    public void setLast_id(String last_id) {
        this.last_id = last_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUser() {
        return user;
    }

    public DigitizedAssestsType getAssets_type() {
        return assets_type;
    }

    public void setAssets_type(DigitizedAssestsType assets_type) {
        this.assets_type = assets_type;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    @Deprecated
    public OprateCode getOprate_code() {
        return oprate_code;
    }

    @Deprecated
    public void setOprate_code(OprateCode oprate_code) {
        this.oprate_code = oprate_code;
    }

    public DigitizedAssestsType getType() {
        return assets_type;
    }


    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getInfo() {
        return info;
    }

    /**
     * 设置信息时,info_md5会同步改变.
     * @param info
     */
    public void setInfo(String info) {
        this.info = info;
        this.info_md5 = MD5.encode(info);
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public Boolean getParsed() {
        return parsed;
    }

    public void setParsed(Boolean parsed) {
        this.parsed = parsed;
    }
}
