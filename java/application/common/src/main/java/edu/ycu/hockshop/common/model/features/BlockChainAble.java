package edu.ycu.hockshop.common.model.features;

/**
 * 能够区块化接口.
 *
 */
public interface BlockChainAble {

    /**
     * 获取到到区块信息.
     * 区块信息为数字资产(数组)JSON序列化之后的字符串信息.
     * 反向解析参考fastjson文档.<a href="https://github.com/alibaba/fastjson">https://github.com/alibaba/fastjson</a>
     * @return
     */
    String resolveBlockDataInfo();

    /**
     * 获取当前区块的MD5值
     * @return
     */
    String resolveBlockMD5();
}
