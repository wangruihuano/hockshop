package edu.ycu.hockshop.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * 用以标记方法在执行之前是否需要检查服务.
 * @author wrh
 * @since 2018-07-18
 */
@Target(ElementType.METHOD)
public @interface CheckServer {
}
