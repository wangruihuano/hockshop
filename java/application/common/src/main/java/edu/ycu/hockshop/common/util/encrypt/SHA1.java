package edu.ycu.hockshop.common.util.encrypt;

import java.security.MessageDigest;

import static edu.ycu.hockshop.common.util.encrypt.ConvertUtil.byteArrayToHexString;


/**
 * SHA1 加密
 *
 */
public class SHA1 {


    public static String encode(String str) {
        if (str == null) {
            return null;
        }
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            messageDigest.update(str.getBytes());
            return byteArrayToHexString(messageDigest.digest());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
