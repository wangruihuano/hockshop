package edu.ycu.hockshop.common.model.netty;

import lombok.*;

import java.io.Serializable;

/**
 * 自定义netty相应消息.
 * @author wangruihuan
 * @since 2018-05-10
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class NettyResponseMsg implements Serializable {

    //相应id
    private String id;
    private NettyResponseCode code;
    private String content;

    public enum NettyResponseCode{
        success,
        fail
    }
}
