package edu.ycu.hockshop.common.util.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 流工具类.
 * @author wangruihuan
 * @since 2018-05-14
 */
public class StreamUtils {

    private StreamUtils(){}

    /**
     * 从输入流中获取字符串
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static String getStringFromStream(InputStream inputStream) throws IOException {
        InputStreamReader reader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(reader);

        StringBuilder builder = new StringBuilder();

        String s;
        while ((s=bufferedReader.readLine())!=null){
            builder.append(s+"\n");
        }
        s = builder.toString();
        return s.substring(0,s.length()-1);
    }
}
