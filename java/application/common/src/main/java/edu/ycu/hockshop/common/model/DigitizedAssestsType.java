package edu.ycu.hockshop.common.model;

/**
 *
 * 数字资产类型
 *
 * @author wrh
 * @date 2018-03-08
 */
public enum DigitizedAssestsType {

    document,//文档
    image,//图片
    voice,//声音
    video,//视频
    other//其他

}
