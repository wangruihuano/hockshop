package edu.ycu.hockshop.common.util.io;

import java.io.*;
import java.util.Properties;

import static edu.ycu.hockshop.common.util.encrypt.ConvertUtil.byteArrayToHexString;

/**
 * 文件工具类.
 *
 * @author wrh
 * @since 2018-04-23
 */
public class FileUtils {

    /**
     * 从文件中读取字符串.
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String getStringFromFile(File file) throws IOException {
        BufferedReader bf = new BufferedReader(new FileReader(file));
        StringBuilder builder = new StringBuilder();

        String buffer;
        while ((buffer = bf.readLine()) != null) {
            builder.append(buffer + "\n");
        }
        return builder.toString();
    }

    /**
     * 向文件写入字符串.
     *
     * @param file   要写入的文件
     * @param string 待写入的字符串
     */
    public static void writeStr2File(File file, String string) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(string);
        writer.close();
    }

    /**
     * 获取文件头信息
     *
     * @param file
     * @return
     */
    public static String getFileHeader(File file) {
        FileInputStream is = null;
        String value = null;
        try {
            is = new FileInputStream(file);
            byte[] b = new byte[4];
            /*
             * int read() 从此输入流中读取一个数据字节。int read(byte[] b) 从此输入流中将最多 b.length
             * 个字节的数据读入一个 byte 数组中。 int read(byte[] b, int off, int len)
             * 从此输入流中将最多 len 个字节的数据读入一个 byte 数组中。
             */
            is.read(b, 0, b.length);
            value = byteArrayToHexString(b);
        } catch (Exception e) {
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }
        return value;
    }

    /**
     * 从文件中加载一个配置文件.
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static Properties loadPropertierFromFile(File file) throws IOException {
        Properties properties = new Properties();
        // 使用properties对象加载输入流
        properties.load(new FileInputStream(file));
        return properties;
    }

    /**
     * @param file 文件.
     * 获得指定文件的byte数组.
     */
    public static byte[] getBytes(File file) throws IOException {
        byte[] buffer = null;
        FileInputStream fis = new FileInputStream(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
        byte[] b = new byte[1000];
        int n;
        while ((n = fis.read(b)) != -1) {
            bos.write(b, 0, n);
        }
        fis.close();
        bos.close();
        buffer = bos.toByteArray();
        return buffer;
    }

    /**
     * @param bfile
     * @param file
     * 根据byte数组，生成文件
     */
    public static void getFile(byte[] bfile,File file) throws IOException {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bfile);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
