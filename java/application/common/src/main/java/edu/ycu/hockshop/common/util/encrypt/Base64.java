package edu.ycu.hockshop.common.util.encrypt;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

public class Base64 {

    /**
     * Base64编码
     * @param origin
     * @return
     */
    public static String encode(String origin){
        BASE64Encoder encoder=new BASE64Encoder();
        String result=encoder.encode(origin.getBytes());
        return result;
    }

    /**
     * Base64解码
     * @param origin
     * @return
     * @throws IOException
     */
    public static String decode(String origin) throws IOException {
        String result=new String(decode2Byte(origin));
        return result;
    }

    /**
     * Base64解码为byte数组.
     * @param origin
     * @return
     * @throws IOException
     */
    public static byte[] decode2Byte(String origin) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] result=decoder.decodeBuffer(origin);
        return result;
    }


    /**
     * Base64编码字节数组.
     * @param origin
     * @return
     */
    public static String encode(byte[] origin){
        BASE64Encoder encoder=new BASE64Encoder();
        String result=encoder.encode(origin);
        return result;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(encode("1"));
        System.out.println(encode("1".getBytes()));
        System.out.println(decode("MQ=="));
    }
}
