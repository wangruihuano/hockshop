package edu.ycu.hockshop.common.model.netty;

/**
 * 中间件服务常量.
 * @author wangruihuan
 * @since 2018-05-11
 */
public interface Constant {

    // 区块链中间件端口
    int BLOCKCHIAN_NETTY_PORT = 10002;

    // 传输对象最大值
    int MAX_OBJECT_SIZE = 1024*1024*20;
}
