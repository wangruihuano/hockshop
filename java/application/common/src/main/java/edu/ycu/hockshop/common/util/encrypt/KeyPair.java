package edu.ycu.hockshop.common.util.encrypt;

import lombok.Getter;

/**
 * 密钥对。
 */
@Getter
public class KeyPair {

    public static final String SPLIT_CHAR="'";

    //公钥
    private String publicKey;

    //私钥
    private String privateKey;

    public KeyPair(java.security.KeyPair keyPair){
        publicKey = Base64.encode(keyPair.getPublic().getEncoded());
        privateKey = Base64.encode(keyPair.getPrivate().getEncoded());
    }

    @Override
    public String toString() {
        return publicKey+SPLIT_CHAR+privateKey;
    }
}
