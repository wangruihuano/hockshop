package edu.ycu.hockshop.common;

/**
 * 整个系统通用的常量定义.
 *
 * @author wangruihuan
 * @since 2018-05-16
 */
public interface Constant {

    // 用户上传的文件在hadoop文件系统中的存储路径
    String DFS_FILE_PATH  = "/assets/";

    // 特征保存路经
    String REDUCE_FIEL_PATH = "/feature/";
}
