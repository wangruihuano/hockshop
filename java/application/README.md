# 存证应用JAVA部分
--------

## 模块设计
1.0版本服务模块设计
![模块及其间关系1.0](../../doc/img/模块及其间关系1.0.png)

2.0版本服务模块设计
![模块及其间关系0=2.0](../../doc/img/模块及其间关系2.0.png)

## 服务构建及部署

### 构建
java部分均采用maven进行构建,其部署基本一致,构建打包时请参照以下顺序进行.

1. common模块

    common模块为其他模块基础,提供了部分通用工具类及相关模型定义.
    
2. marmot-standard模块

    marmot-standard模块提供了信息提取的标准接口及规范,marmot-api项目及marmot依赖于此.
    
3. blockchain-api,dfs-api及marmot-api

    这三个项目负责提供规范RPC远程服务需暴露的接口.
    
4. blockchain,dfs,marmot及httpapi项目

    前三个项目为上一步中三个项目中接口提供具体实现.httpapi提供[web项目](../../html/hockshop-web)所需的http接口.
    
    分别进入[blockchain](./blockchain),[dfs](./dfs),[marmot](./marmot)及[httpapi](./httpapi)中,
    根据各模块的README.md文档修改其所需的的配置信息.
    
    **请确保配置信息填写正确,否则可能造正服务注册中心无法发现服务,RPC远程调用异常等情况出现**
    
    修改配置文件之后,在各模块内分别执行 `mvn package`,构建成功后将会在各模块目录内生成target目录,在目录内将会包含
    
    *由于这四个项目为直接部署所用的服务,不被其他项目所依赖,所以打包( `mvn package spring-boot:repackage -Dmaven.test.skip=true` )即可*
    