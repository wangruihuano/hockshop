package edu.ycu.hockshop.marmotapi;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Marmot核心功能接口类.
 * @author wrh
 * @since 2018-04-21
 */
public interface Marmot {

    /**
     * 通过类型的全类名获取特征.
     * @param className 类别的全类名
     * @return
     */
    String[] getFeaturesByTypeName(String className) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;

    /**
     * 根据全类名获取类属性名.
     * @param className 全类名
     * @return 类属性名数组.
     */
    String[] getFeatures(String className) throws ClassNotFoundException;

    /**
     * 根据类获取类属性名.
     * @param clazz 类
     * @return 类属性名数组.
     */
    String[] getFeatures(Class clazz);

    /**
     * 通过全类名执行信息抽取.
     * @param className 全类名
     * @param fileByte 要抽取信息的文件的字节数组
     * @param featureNames 要抽取的特征属性
     * @return 特征模型
     * @throws ClassNotFoundException
     * @throws IOException
     */
    Object doExtract(String className, byte[] fileByte, String[] featureNames) throws ClassNotFoundException,IOException;

    /**
     * 获取类别信息.
     * @return
     */
    Map<String ,Map<String,String>> getTypeInfo();

}
