package cn.licte.utils;

import cn.licte.fabric.BlockChainProvider;
import cn.licte.fabric.service.BlockChainImpl;
import com.alibaba.dubbo.config.annotation.Reference;
import edu.ycu.hockshop.blockchainapi.BlockChain;
import edu.ycu.hockshop.common.model.DigitizedAssests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = BlockChainProvider.class)
@RunWith(SpringRunner.class)
public class DubboTest {

    @Reference(version = "${demo.service.version}",
            application = "${dubbo.application.id}")
    BlockChain blockChain;

    @Test
    public void testBlockChain(){
        BlockChainImpl blockChain = new BlockChainImpl();
        DigitizedAssests digitizedAssests = new DigitizedAssests();
        digitizedAssests.setUser("tom");
        digitizedAssests.setId("666"+Math.random());
        digitizedAssests.setInfo("ssssssssss");
        digitizedAssests.setMd5("25555888585");
        try {
            blockChain.pushAssetsInfo(digitizedAssests);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
