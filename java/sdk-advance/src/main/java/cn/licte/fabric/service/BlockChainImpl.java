package cn.licte.fabric.service;

import cn.licte.fabric.sdk.FabricManager;
import cn.licte.fabric.sdk.OrgManager;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import edu.ycu.hockshop.blockchainapi.BlockChain;
import edu.ycu.hockshop.common.annotation.CheckServer;
import edu.ycu.hockshop.common.model.DigitizedAssests;
import lombok.Getter;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static cn.licte.fabric.utils.Context.*;

/**
 * @author zql
 * @date 2018-5-31
 */
@Service(version = "1.0.0",
        application = "${dubbo.application.id}",
        protocol = "${dubbo.protocol.id}",
        registry = "${dubbo.registry.id}",
        timeout = 5000)
public class BlockChainImpl implements BlockChain {


    private static Logger logger = Logger.getLogger(BlockChainImpl.class);

    private static FabricManager manager;

    @Getter
    private static boolean initSucess = false;

    public BlockChainImpl() {
        init();
    }

    public BlockChainImpl init() {
        try {
            manager = obtainFabricManager();
            this.manager = manager;
            initSucess = true;
        } catch (Exception e) {
            logger.error("区块网络初始化异常", e);
        }
        return this;
    }

    public boolean serverIsSuccess() {
        return initSucess;
    }

    private FabricManager obtainFabricManager() throws Exception {
        OrgManager orgManager = new OrgManager();
        orgManager
                .init(getPropertyValue("org1Name"))
                .setUser(getPropertyValue("username"), getPropertyValue("cryptoConfigPath"), getPropertyValue("channelArtifactsPath"))
                .setCA(getPropertyValue("caName"), "http://" + getCaLocationOrDefault())
                .setOrderers(getPropertyValue("ordererDomainName"))
                .addOrderer(getPropertyValue("orderer0Name"), getPropertyValue("orderer0Location"))
                .addOrderer(getPropertyValue("orderer1Name"), getPropertyValue("orderer1Location"))
                .addOrderer(getPropertyValue("orderer2Name"), getPropertyValue("orderer2Location"))
                .setPeers(getPropertyValue("org1MSPID"), getPropertyValue("org1DomainName"))
                .addPeer(getHostNameOrDefault(), getHostNameOrDefault(), "grpc://" + getPeerLocationOrDefault()
                        , "grpc://" + getPeerEventHubLocationOrDefault(), true)
                .setChannel(getPropertyValue("channelName"))
                .setChainCode(getPropertyValue("chaincodeName"), getPropertyValue("chaincodeSource")
                        , getPropertyValue("chaincodePath"), getPropertyValue("chaincodeVersion")
                        , Integer.valueOf(getPropertyValue("proposalWaitTime")), Integer.valueOf(getPropertyValue("invokeWaitTime")))
                .openTLS(false)
                .openCATLS(false)
                .setBlockListener(map -> {
                    logger.debug(map.get("code"));
                    logger.debug(map.get("data"));
                })
                .add();

        return orgManager.use(getPropertyValue("org1Name"));
    }

    @Override
    @CheckServer
    public Boolean pushAssetsInfo(DigitizedAssests assets) throws Exception {

        String currentId = assets.getId();

        String[] args = new String[]{currentId, JSON.toJSONString(assets)};

        Map<String, String> invoke = manager.invoke("push", args);

        return invoke.get("code").equals("success");

    }

    @Override
    @CheckServer
    public DigitizedAssests pullAssetsInfo(String currentId) throws Exception {

        String[] args = new String[]{currentId};
        Map<String, String> query = manager.query("pull", args);

        if (query.get("code").equals("success")) {

            JSONObject object = JSON.parseObject(query.get("data"));
            String info = (String) object.get("assetsInfo");
            logger.debug(info);
            DigitizedAssests assets = JSON.parseObject(info, DigitizedAssests.class);

            return assets;
        }

        return null;
    }

    @Override
    @CheckServer
    public Boolean cancelAssetsInfo(String currentId) throws Exception {

        String[] args = new String[]{currentId};
        Map<String, String> invoke = manager.invoke("cancel", args);
        if (invoke.get("code") == "success") {
            return true;
        }
        return false;
    }


    @Override
    @CheckServer
    public List<DigitizedAssests> pullAllAssetsInfo(String start, String end) throws Exception {

        Map<String, String> query = manager.query("pullAllAssetsInfo", new String[]{start, end});

        List<DigitizedAssests> list = new ArrayList<>();

        if (query.get("code") == "success") {

            String data = query.get("data");

            JSONArray jsonArray = JSONArray.parseArray(data);
            for (Object j : jsonArray) {
                JSONObject jsonObjectone = (JSONObject) j;
                String info = (String) jsonObjectone.get("assetsInfo");
                DigitizedAssests assets = JSON.parseObject(info, DigitizedAssests.class);
                list.add(assets);
                logger.debug(list);
            }

        }

        return list;
    }

    @Override
    @CheckServer
    public String pullAssetsHistoryByID(String currentId) throws Exception {

        Map<String, String> query = manager.query("pullAssetsHistoryByID", new String[]{currentId});
        String assHistory = null;
        if (query.get("code") == "success") {
            assHistory = query.get("data");
        }

        return assHistory;
    }

    @Override
    @CheckServer
    public String pullCalledChainName() throws Exception {

        Map<String, String> query = manager.query("pullDomainName", new String[]{""});
        String name = null;
        if (query.get("code") == "success") {
            name = query.get("data");
        }

        return name;
    }


}
