package cn.licte.fabric;

import cn.licte.fabric.service.BlockChainImpl;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 向区块网络进行读写操作之前执行的方法.
 * @author wrh
 * @since 2018-07-18
 */
@Aspect
@Component
public class ActionAspect {

    /**
     * 切点定义.
     */
    @Pointcut("@annotation(edu.ycu.hockshop.common.annotation.CheckServer)")
    public void doAction(){}

    /**
     * 方法执行之前进行的操作.
     * @param joinPoint 切点
     */
    @Before("doAction()")
    public void deBefore(JoinPoint joinPoint){
        BlockChainImpl blockChain = (BlockChainImpl) joinPoint.getThis();
        if (!blockChain.serverIsSuccess()){
            blockChain.init();
        }
    }

}
