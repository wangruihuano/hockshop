package cn.licte.fabric.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * Application context.
 *
 * @author wangruihuan
 * @author zql
 * @since 2018-06-22
 */
public class Context {

    private static Properties properties = new Properties();

    private static Logger logger = Logger.getLogger(Context.class);


    static {
        InputStream in = Context.class.getClassLoader().getResourceAsStream("config.properties");
        // 使用properties对象加载输入流
        try {
            properties.load(in);
        } catch (IOException e) {
            logger.error("加载配置文件异常", e);
        }
    }

    /**
     * Gets hostname.
     *
     * @return hostname
     */
    public static String getHostNameOrDefault() {

        String hostName = "";
        try {
            hostName = Inet4Address.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            logger.debug(e);
        }
        logger.debug("HostName:" + hostName);

        return !hostName.equals("") ? hostName : getPropertyValue("defaultPeerHostName");
    }

    /**
     * Gets host's ip address.else use default.
     *
     * @return ipv4
     */
    private static String getIPV4OrDefault() {

        String ipv4 = "";
        try {
            ipv4 = Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            logger.debug(e);
        }
        logger.debug("IPV4:" + ipv4);

        return !ipv4.equals("") ? ipv4 : getPropertyValue("defaultPeerIP");
    }

    /**
     * Gets peer location,else use default.
     *
     * @return peer location
     */
    public static String getPeerLocationOrDefault() {

        return getIPV4OrDefault() +":"+ getPropertyValue("peerLocationPort");
    }

    /**
     * Gets event hub location.
     *
     * @return event hub location
     */
    public static String getPeerEventHubLocationOrDefault() {

        return getIPV4OrDefault() +":"+ getPropertyValue("peerEventHubLocationPort");
    }

    /**
     * Gets CA location.
     *
     * @return ipv4
     */
    public static String getCaLocationOrDefault() {
        return getIPV4OrDefault() +":"+ getPropertyValue("caPort");
    }


    /**
     * Gets attribute value.
     *
     * @param name attribute name
     * @return attribute value
     */
    public static String getPropertyValue(String name) {
        return properties.getProperty(name);
    }


}
