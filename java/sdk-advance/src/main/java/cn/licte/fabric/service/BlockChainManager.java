//package cn.licte.fabric.service;
//
//import cn.licte.fabric.sdk.FabricManager;
//import cn.licte.fabric.sdk.OrgManager;
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
//import org.hyperledger.fabric.sdk.exception.ProposalException;
//
//import java.io.IOException;
//import java.net.Inet4Address;
//import java.util.Map;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.TimeoutException;
//
//import static cn.licte.fabric.utils.Context.*;
///**
// * @author zql
// * @since  2018.7.27
// */
//
//public class BlockChainManager {
//
//    private static BlockChainManager instance;
//
//    private FabricManager fabricManager;
//
//    private static Logger logger = LogManager.getLogger(BlockChainManager.class);
//
//
//    public static BlockChainManager obtain() throws Exception {
//        if (null == instance) {
//            synchronized (BlockChainManager.class) {
//                if (null == instance) {
//                    instance = new BlockChainManager();
//                }
//            }
//        }
//        return instance;
//    }
//
//    private BlockChainManager() throws Exception {
//        fabricManager = obtainFabricManager();
//    }
//
//
//    public FabricManager getFabricManager() {
//        return fabricManager;
//    }
//
//    private FabricManager obtainFabricManager() throws Exception {
//        OrgManager orgManager = new OrgManager();
//        orgManager
//                .init("Org1")
//                .setUser(getPropertyValue("username"), getPropertyValue("cryptoConfigPath"), getPropertyValue("channelArtifactsPath"))
//                .setCA(getPropertyValue("caName"),getPropertyValue("http//"+getIPV4()))
//                .setOrderers(getPropertyValue("ordererDomainName"))
//                .addOrderer(getPropertyValue("ordererName"), getPropertyValue("ordererLocation"))
//                .setPeers(getOrgMSPID(), "")
//                .addPeer(getHostName(), "", "", "", true)
//                .setChannel("")
//                .setChainCode("", "", "", "", 1, 1)
//                .openTLS(false)
//                .openCATLS(false)
//                .setBlockListener(map -> {
//                    logger.debug(map.get("code"));
//                    logger.debug(map.get("data"));
//                })
//                .add();
//
////        fabricManager.invoke("initMarble",new String[]{"marble3","yellow","25","jim"});
//
//        return orgManager.use("Org1");
//
//    }
//
//
//
//}
