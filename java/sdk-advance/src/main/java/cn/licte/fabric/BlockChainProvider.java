package cn.licte.fabric;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class BlockChainProvider {

    public static void main(String[] args) {
        new SpringApplicationBuilder(BlockChainProvider.class)
                .web(WebApplicationType.NONE)
                .run(args);
    }
}
