import edu.ycu.monitor.BaseServerNode;
import edu.ycu.monitor.hardware.*;
import edu.ycu.monitor.system.HardwareTools;
import org.hyperic.sigar.NetInterfaceStat;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

public class DigTest {

    @Test
    public void CpuTest(){
        try {

          for (int j=0;j<1;j++){
              Thread.sleep(100);
              CPU cpu[] =  HardwareTools.cpus();
              for (int i = 0; i < cpu.length; i++){
                  System.out.println(cpu[i].toString());
              }
          }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void ExchangeTest(){
        try {
            Exchange exchange = HardwareTools.exchange();
            System.out.println("交换空间总量:"+exchange.getTotal()+"  已经使用量:"+exchange.getUsed()+"  空闲量:"+exchange.getFree()+".");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void MemoryTest(){
        try {
            Memory memory = HardwareTools.memory();
            System.out.println("内存总量:"+memory.getTotal()+"  内存使用量:"+memory.getUsed()+"  内存空闲量:"+memory.getFree()+".");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void HardDiskTest(){
        try {
            HardDisk hardDisk = HardwareTools.hardDisk();
            System.out.println("磁盘总大小:"+hardDisk.getTotal()+"GB ;磁盘空闲量:"+hardDisk.getFree()+"GB ;磁盘可使用量:"+hardDisk.getAvail()+"GB ;磁盘已使用量:"+hardDisk.getUsed()+"GB");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Test
    public void netWorkTest(){
        NetInterface[] ss = HardwareTools.netInterfaceConfig();
        System.out.println(ss);
    }


    @Test
    public void ipTest(){
        InetAddress address=HardwareTools.getLocalHostLANAddress();
        byte[] i = address.getAddress();
        String hostname = address.getHostName();
        System.out.println(hostname);
    }

    @Test
    public void ipAndHostTest(){
        InetAddress ia = null;
        try {
            ia = InetAddress.getLocalHost();
            String host = ia.getHostName();//获取计算机主机名
            String IP= ia.getHostAddress();//获取计算机IP
            System.out.println(host);
            System.out.println(IP);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void nodeTest(){
        BaseServerNode node=HardwareTools.serverNode();
        System.out.println(node);
    }
}
