import com.alibaba.fastjson.JSON;
import edu.ycu.monitor.BaseServerNode;
import edu.ycu.monitor.InfoUpdater;
import org.junit.Test;

public class ByteTest {
    BaseServerNode serverNode = InfoUpdater.getServerNode();

    @Test
    public void nodeJsonSizeTest(){
        serverNode.setHostname("hostname");
        serverNode.setIp("454.66.656.64");
        String json = JSON.toJSONString(serverNode);
        System.out.println(json);
        System.out.println(json.getBytes().length);
    }

    @Test
    public void pathTest(){
        System.out.println(ByteTest.class.getClassLoader().getResource(""));
        System.out.println(ByteTest.class.getClassLoader().getResource("/"));
    }
}
