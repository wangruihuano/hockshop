package edu.ycu.monitor.hardware;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 交换空间.
 */
@Getter
@Setter
@ToString
public class Exchange {

    private Long total; //交换空间总量
    private Long used; //已经使用量
    private Long free; //空闲

}
