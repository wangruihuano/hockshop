package edu.ycu.monitor;

import edu.ycu.monitor.hardware.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.log4j.Logger;
import org.hyperic.sigar.NetInterfaceStat;

/**
 * 基础服务器节点类.
 * 便于统计服务器信息及管理服务器.
 *
 * @author wrh
 * @since 2018-03-12
 */

@Getter
@Setter
@ToString
public class BaseServerNode {

    private static final Logger logger = Logger.getLogger(BaseServerNode.class);

    private String ip;
    private String hostname; //主机名
    private ServerStatus serverStatus; //服务器状态

    //计算机硬件
    private CPU[] cpu;
    private HardDisk hardDisk;
    private Memory memory;

    //一台机器可能有多个网络接口
    private NetInterface[] netInterfaceStats;
    private Exchange exchange;

    /**
     * 服务器状态枚举类型
     */
    public enum ServerStatus {
        down, //宕机
        work, //工作中
        Maintain //维护中
    }

}
