package edu.ycu.monitor;

import org.apache.log4j.Logger;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import static edu.ycu.monitor.Constant.*;

/**
 * MonitorClient.
 * @author wangruihuan
 * @since 2018-05-07
 */
public class MonitorClient {

    private Logger logger = Logger.getLogger(MonitorClient.class);

    private DatagramSocket datagramSocket;
    private DatagramPacket datagramPacket;

    public MonitorClient(){
        init();

    }

    public String request(String netAddress){
        String receStr = null;
        try {
            byte[] buf = PROTOCOL_REQUEST.getBytes();
            InetAddress address = InetAddress.getByName(netAddress);
            datagramPacket = new DatagramPacket(buf, buf.length, address, PORT);
            // 发送数据
            datagramSocket.send(datagramPacket);

            /*** 接收数据***/
            byte[] receBuf = new byte[RECEIVE_MAX_LENGTH];
            DatagramPacket recePacket = new DatagramPacket(receBuf, receBuf.length);
            datagramSocket.receive(recePacket);

            receStr = new String(recePacket.getData(), 0 , recePacket.getLength());

        }catch (Exception e){
            logger.warn("udp communicate error",e);
        }
       return receStr;
    }

    private void init() {
        try {

            /*** 发送数据***/
            // 初始化datagramSocket,注意与前面Server端实现的差别
            datagramSocket = new DatagramSocket();
            datagramSocket.setSoTimeout(SOCKET_TIME_OUT);
            // 使用DatagramPacket(byte buf[], int length, InetAddress address, int port)函数组装发送UDP数据报



        } catch (SocketException e) {
            logger.error("client init error",e);
    }
    }

}