package edu.ycu.monitor.hardware;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 计算机内存.
 *
 * @author wrh
 * @since 2018-03-13
 */
@Setter
@Getter
@ToString
public class Memory {

    private Long total; //内存总量
    private Long used; //已经使用量
    private Long free; //空闲



}
