package edu.ycu.monitor.system;


import edu.ycu.monitor.BaseServerNode;
import edu.ycu.monitor.InfoUpdater;
import edu.ycu.monitor.hardware.*;
import org.apache.log4j.Logger;
import org.hyperic.sigar.*;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;


/**
 * 计算机硬件相关工具.
 *
 * @since 2018-03-13
 */
public class HardwareTools {

    private static Logger logger = Logger.getLogger(HardwareTools.class);

    /**
     * get running this function's server node info.
     *
     * @return
     */
    public static BaseServerNode serverNode() {
        return InfoUpdater.getServerNode();
    }

    /**
     * 获取主机名
     *
     * @return
     */
    public static String hostName() {
        try {
            InetAddress address = InetAddress.getLocalHost();
            return address.getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取ip
     *
     * @return
     */
    public static InetAddress getLocalHostLANAddress() {
        try {
            InetAddress candidateAddress = null;
            // 遍历所有的网络接口
            for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements(); ) {
                NetworkInterface iface = (NetworkInterface) ifaces.nextElement();
                // 在所有的接口下再遍历IP
                for (Enumeration inetAddrs = iface.getInetAddresses(); inetAddrs.hasMoreElements(); ) {
                    InetAddress inetAddr = (InetAddress) inetAddrs.nextElement();
                    if (!inetAddr.isLoopbackAddress()) {// 排除loopback类型地址
                        if (inetAddr.isSiteLocalAddress()) {
                            // 如果是site-local地址，就是它了
                            return inetAddr;
                        } else if (candidateAddress == null) {
                            // site-local类型的地址未被发现，先记录候选地址
                            candidateAddress = inetAddr;
                        }
                    }
                }
            }
            if (candidateAddress != null) {
                return candidateAddress;
            }
            // 如果没有发现 non-loopback地址.只能用最次选的方案
            InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
            return jdkSuppliedAddress;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取当前机器CPU运行状态.
     *
     * @return 多核处理器
     */
    public static CPU[] cpus() {

        Sigar sigar = SigarUtils.sigar;
        CpuInfo[] infos = null;   //获取CPU块数
        CpuPerc cpuList[] = null;
        try {
            infos = sigar.getCpuInfoList();
            cpuList = sigar.getCpuPercList();           //获取CPU状态
        } catch (SigarException e) {
            logger.error("get cpu info error", e);
            return null;
        }

        CPU cpu[] = new CPU[infos.length];

        for (int i = 0; i < infos.length; i++) {// 不管是单块CPU还是多CPU都适用
            CpuInfo info = infos[i];
            cpu[i] = new CPU(cpuList[i].getCombined());
        }
        return cpu;
    }


    /**
     * 获取交换空间信息.
     *
     * @return
     */
    public static Exchange exchange() {
        Sigar sigar = SigarUtils.sigar;
        Exchange exchange = null;
        Swap swap = null;
        try {
            swap = sigar.getSwap();
            exchange = new Exchange();
            exchange.setTotal(swap.getTotal() / 1024L);
            exchange.setUsed(swap.getUsed() / 1024L);
            exchange.setFree(swap.getFree() / 1024L);
        } catch (SigarException e) {
            logger.error("获取swap异常", e);
        }
        return exchange;
    }

    /**
     * 获取内存状态
     *
     * @return
     */
    public static Memory memory() {
        Sigar sigar = SigarUtils.sigar;
        Mem mem;
        try {
            mem = sigar.getMem();
        } catch (SigarException e) {
            logger.warn("get memory info error", e);
            return null;
        }

        Memory memory = new Memory();
        memory.setTotal(mem.getTotal() / 1024L);
        memory.setUsed(mem.getUsed() / 1024L);
        memory.setFree(mem.getFree() / 1024L);
        return memory;
    }

    /**
     * 获取硬盘状态
     *
     * @return
     */
    public static HardDisk hardDisk() {

        HardDisk hardDisk = null;

        Sigar sigar = SigarUtils.sigar;
        FileSystem fslist[] = new FileSystem[0];
        try {
            fslist = sigar.getFileSystemList();
        } catch (SigarException e) {
            logger.error("get hardDisk info error", e);
            return null;
        }

        for (int i = 0; i < fslist.length; i++) {
            FileSystem fs = fslist[i];
            FileSystemUsage usage;
            try {
                usage = sigar.getFileSystemUsage(fs.getDirName());
            } catch (SigarException e) {
                logger.error("get hardDisk info error", e);
                return null;
            }
            switch (fs.getType()) {
                case 0: // TYPE_UNKNOWN ：未知
                    break;
                case 1: // TYPE_NONE
                    break;
                case 2: // TYPE_LOCAL_DISK : 本地硬盘
                    hardDisk = new HardDisk();
                    // 文件系统总大小
                    hardDisk.setTotal((int) usage.getTotal() / 1024 / 1024);
                    // 文件系统空闲
                    hardDisk.setFree((int) usage.getFree() / 1024 / 1024);
                    // 文件系统可用大小
                    hardDisk.setAvail((int) usage.getAvail() / 1024 / 1024);
                    // 文件系统已经使用量
                    hardDisk.setUsed((int) usage.getUsed() / 1024 / 1024);
                    return hardDisk;
                case 3:// TYPE_NETWORK ：网络
                    break;
                case 4:// TYPE_RAM_DISK ：闪存
                    break;
                case 5:// TYPE_CDROM ：光驱
                    break;
                case 6:// TYPE_SWAP ：页面交换
                    break;
            }
        }
        return hardDisk;
    }



    public static NetInterface[] netInterfaceConfig(NetInterface[] last) {

        NetInterface[] res = netInterfaceConfig();
        if (last==null || last.length==0){
            return res;
        }

        for (int i=0;i<last.length;i++){
            res[i]=calNew(last[i].getName(),last[i],res[i]);
        }

        return res;
    }

    public static NetInterface[] netInterfaceConfig(){
        NetInterface[] stats = null;
        Sigar sigar = SigarUtils.sigar;
        try {
            String[] names = sigar.getNetInterfaceList();
            int num = names.length;
            if (names==null || num<=0){
                return null;
            }
            stats = new NetInterface[names.length];
            for (int i=0;i<num;i++){
                stats[i]=adaptationFromSigar(names[i],sigar.getNetInterfaceStat(names[i]));
            }
            return stats;
        } catch (SigarException e) {
            logger.error("获取网络数据异常",e);
        }
        return null;
    }

    /**
     * 计算某时间段内两个网络状况的平均差值
     * @param name 网络接口名称
     * @param old 旧数据
     * @param news 新数据
     * @return
     */
    //TODO
    public static NetInterface calNew(String name, NetInterface old, NetInterface news){
        NetInterface netWork = new NetInterface();
//        logger.debug("news Rxbyte:"+news.getRxBytes()+",old:"+old.getRxBytes());
        netWork.setName(name);
        netWork.setRxBytes((news.getRxBytes()-old.getRxBytes()));
        netWork.setTxBytes((news.getTxBytes()-old.getTxBytes()));
        netWork.setRxPackets((news.getRxPackets()-old.getRxPackets()));
        netWork.setTxPackets((news.getTxPackets()-old.getTxPackets()));
        netWork.setRxFrame((news.getRxFrame()-old.getRxFrame()));
        netWork.setRxDropped((news.getRxDropped()-old.getRxDropped()));
        netWork.setTxDropped((news.getTxDropped()-old.getTxDropped()));
        netWork.setRxErrors((news.getRxErrors()-old.getRxErrors()));
        netWork.setTxErrors((news.getTxErrors()-old.getTxErrors()));
//        netWork.set
        return netWork;
    }
    public static void main(String[] args) throws SigarException {
        Sigar sigar = SigarUtils.sigar;
        String[] names = sigar.getNetInterfaceList();
        while (true){

        }
    }

    /**
     * 从Sigar提供的网络接口类适配至自定义的网络接口类.
     * @param name 网络接口名称
     * @param stat Sigar网络接口信息
     * @return
     */
    public static NetInterface adaptationFromSigar(String name,NetInterfaceStat stat){
        NetInterface netInterface = new NetInterface();
        netInterface.setName(name);
        netInterface.setRxBytes(stat.getRxBytes());
        netInterface.setTxBytes(stat.getTxBytes());
        netInterface.setRxDropped(stat.getRxDropped());
        netInterface.setTxDropped(stat.getTxDropped());
        netInterface.setRxErrors(stat.getRxErrors());
        netInterface.setTxErrors(stat.getTxErrors());
        netInterface.setRxPackets(stat.getRxPackets());
        netInterface.setTxPackets(stat.getTxPackets());
        netInterface.setRxFrame(stat.getRxFrame());
        return netInterface;
    }
}
