package edu.ycu.monitor.hardware;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * CPU对应实体类.
 *
 * @author wrh
 * @since 2018-03-13
 */
@Getter
@Setter
@ToString
public class CPU {

    private double usage_rate; //使用率

    //TODO 暂定使用这一个属性

    public CPU(){}
    public CPU(double usage_rate) {
        this.usage_rate = usage_rate;
    }

}
