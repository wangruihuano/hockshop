package edu.ycu.monitor;

import com.alibaba.fastjson.JSON;
import edu.ycu.monitor.system.HardwareTools;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * UDP server端.
 * */
public class UdpServer implements Runnable{

    private Logger logger = Logger.getLogger(UdpServer.class);

    // 用以存放接收数据的字节数组
    private byte[] receMsgs ;
    // 数据报套接字
    private DatagramSocket datagramSocket;
    // 用以接收数据报
    private DatagramPacket datagramPacket;

    BaseServerNode serverNode;

    public UdpServer(){
        init();
    }

    private void init() {
        receMsgs = new byte[Constant.RECEIVE_MAX_LENGTH];
        serverNode = InfoUpdater.getServerNode();

        // 创建一个数据报套接字，并将其绑定到指定port上
        try {
            datagramSocket = new DatagramSocket(Constant.PORT);
        } catch (SocketException e) {
            logger.error("建立DatagramSocket异常",e);
        }
        // DatagramPacket(byte buf[], int length),建立一个字节数组来接收UDP包
        datagramPacket = new DatagramPacket(receMsgs, receMsgs.length);
    }


    @Override
    public void run() {
        while (true){
            // receive()来等待接收UDP数据报
            try {
                datagramSocket.receive(datagramPacket);

                String receStr = new String(datagramPacket.getData(), 0 , datagramPacket.getLength());
                receStr=receStr.trim().replace("\n","");
                switch (receStr){
                    case Constant.PROTOCOL_REQUEST:
                        responseServerNode(datagramPacket);
                }
            } catch (IOException e) {
                logger.error("receive data exception",e);
            }

        }
    }

    private void responseServerNode(DatagramPacket datagramPacket) {

//        //Limited by sigar library, two request intervals need to be greater than 100 milliseconds
//        if (System.currentTimeMillis()-lastResponseTime>=100){
//            serverNode=HardwareTools.serverNode();
//        }

        String info = JSON.toJSONString(serverNode);
        byte[] buf = info.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(buf, buf.length, datagramPacket.getAddress(), datagramPacket.getPort());

        try {
            // 发送消息
            datagramSocket.send(sendPacket);
        } catch (IOException e) {
           logger.warn("send data exception!",e);
        }
    }
}