package edu.ycu.monitor;

/**
 * 监控服务常量定义.
 * @since 2018-05-02
 * @author wrh
 */
public interface Constant {

    //the udp server port
    int PORT = 10001;

    //max length of receive data
    int RECEIVE_MAX_LENGTH = 1024*8;

    //protocol for request server node info
    String PROTOCOL_REQUEST = "r";

    //the max time of socket connect time
    int SOCKET_TIME_OUT=5000;

}
