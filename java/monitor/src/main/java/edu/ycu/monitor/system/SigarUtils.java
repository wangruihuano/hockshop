package edu.ycu.monitor.system;

import org.apache.log4j.Logger;
import org.hyperic.sigar.Sigar;

import java.io.File;

/**
 * Sigar工具类
 */
public class SigarUtils{
    private static final Logger logger = Logger.getLogger("SigarUtils");

    public final static Sigar sigar = initSigar();

    /**
     * 初始化Sigar
     * @return
     */
    private static Sigar initSigar() {

        try {
            String  classPath = new File("libs/sigar").getAbsolutePath();

            //加载java.library.path类库
            String path = System.getProperty("java.library.path");
            if (OsCheck.getOperatingSystemType() == OsCheck.OSType.Windows) {
                path += ";" + classPath;
            } else {
                path += ":" + classPath;
            }
            System.setProperty("java.library.path", path);
            logger.info("set property:"+path);
            return new Sigar();
        } catch (Exception e) {
            logger.warn("init Sigar exception!",e);
            return null;
        }
    }

}
