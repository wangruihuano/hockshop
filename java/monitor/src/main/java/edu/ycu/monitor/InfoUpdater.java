package edu.ycu.monitor;

import com.alibaba.fastjson.JSON;
import edu.ycu.monitor.hardware.NetInterface;
import org.apache.log4j.Logger;
import org.hyperic.sigar.NetInterfaceStat;

import static edu.ycu.monitor.system.HardwareTools.*;

/**
 * 数据更新器.
 * @author wangruihuan
 * @since 2018-05-20
 */
public class InfoUpdater  extends Thread{

    private static final Logger logger = Logger.getLogger(InfoUpdater.class);

    private InfoUpdater(){
        this.start();
    }

    public static InfoUpdater infoUpdater = new InfoUpdater();

    public static InfoUpdater updater(){
        return infoUpdater;
    }
    public static BaseServerNode getServerNode() {
        return serverNode;
    }

    private static BaseServerNode serverNode = new BaseServerNode();

    private static final long TIME_DIFF=1000L;//采集信息时差

    @Override
    public void run() {
        NetInterface[] lastNet;
        while (true){
            try {

                serverNode.setIp(getLocalHostLANAddress().getHostName());
                serverNode.setHostname(hostName());
                serverNode.setCpu(cpus());
                serverNode.setHardDisk(hardDisk());
                serverNode.setExchange(exchange());
                serverNode.setMemory(memory());
                lastNet=netInterfaceConfig();
                Thread.sleep(TIME_DIFF);
                serverNode.setNetInterfaceStats(netInterfaceConfig(lastNet));
                serverNode.setServerStatus(BaseServerNode.ServerStatus.work);
                logger.info(JSON.toJSONString(serverNode));
            } catch (InterruptedException e) {
                logger.error("",e);
            }

        }
    }

}
