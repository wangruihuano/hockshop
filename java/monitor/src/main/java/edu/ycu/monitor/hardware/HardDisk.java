package edu.ycu.monitor.hardware;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 硬盘信息对应类.
 *
 * @author wrh
 * @since 2018-03-13
 */
@Getter
@Setter
@ToString
public class HardDisk {

    private int total;//总量
    private int free; //空闲
    private int avail; //可使用
    private int used; //已使用
}
