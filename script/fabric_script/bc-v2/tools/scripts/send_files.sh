#!/bin/bash
. $PWD/scripts/set_env.sh
src_path=$2
des_path=$3
sed -n '2,$ w ./output.txt' ${HOSTS_INFO}

if [ $1 = "-compose" ]; then

  i=0
  while read line; do
    ips[${i}]=$(echo ${line} | cut -d ' ' -f 1)

    scp ${src_path}/docker-compose-peer$i.yaml root@${ips[${i}]}:${des_path}/docker-compose-peer.yaml

    i=$(expr ${i} + 1)
  done <./output.txt

elif [ $1 = "-f" ]; then
  awk -F " " '/[0-9]/ {cmd="scp -r '"$src_path"'" " root@"$1":""'"$des_path"'";system(cmd)}' ./output.txt
fi

rm -rf ./output.txt
