#!/bin/bash

. $PWD/scripts/set_env.sh

SIGN=$1
STRING=$*
SEND_SHELL=${STRING:2}
: ${SEND_SHELL:="hostname"}
function unit() {
  awk -F " " '/[0-9]/ {cmd="ssh" " root@"$1""" \"'"$1"';exit\" ";system(cmd)}' "$2"
}

function ker() {
  if [ "$1" = "-a" ]; then
    unit "$2" ${HOSTS_INFO}
  elif [ "$1" = "-o" ]; then
    sed -n '2,$ w output.txt' "${HOSTS_INFO}"
    unit "$2" output.txt
    rm -rf output.txt
  fi
}

ker "${SIGN}" "${SEND_SHELL}"
