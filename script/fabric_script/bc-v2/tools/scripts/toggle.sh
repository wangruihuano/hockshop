#!/bin/bash

. /root/bc-v2/tools/scripts/set_env.sh

# define docker-xxx.yaml path

YAML_ZK="${WORKING_DIR}/docker-zookeeper.yaml"
YAML_KAKA="${WORKING_DIR}/docker-kafka.yaml"
YAML_PEER="${WORKING_DIR}/docker-peer.yaml"
YAML_ORDER="${WORKING_DIR}/docker-orderer.yaml"


function toggle_orderer() {

  if [ "$1" = "-ost" ]; then
    info "start"
    docker-compose -f "${YAML_ORDER}" up -d

  elif [ "$1" = "-osp" ]; then
    info "stop"
    docker-compose -f "${YAML_ORDER}" down

  elif [ "$1" = "-orst" ]; then
    info "restart"
    # restart docker
    service docker restart
    # restart orderer
    docker-compose -f "${YAML_ORDER}" down
    docker-compose -f "${YAML_ORDER}" up -d
  fi
}

function toggle_zookeeper() {
  if [ "$1" = "-zst" ]; then
    info "start"
    docker-compose -f "${YAML_ZK}" up -d

  elif [ "$1" = "-zsp" ]; then
    info "stop"
    docker-compose -f "${YAML_ZK}" down

  elif [ "$1" = "-zrst" ]; then
    info "restart"
    # restart docker
    service docker restart
    # restart peer
    docker-compose -f "${YAML_ZK}" down
    docker-compose -f "${YAML_ZK}" up -d
  fi
}

function toggle_kafka() {
  if [ "$1" = "-kst" ]; then
    info "start"
    docker-compose -f "${YAML_KAKA}" up -d

  elif [ "$1" = "-ksp" ]; then
    info "stop"
    docker-compose -f "${YAML_KAKA}" down

  elif [ "$1" = "-krst" ]; then
    info "restart"
    # restart docker
    service docker restart
    # restart peer
    docker-compose -f "${YAML_KAKA}" down
    docker-compose -f "${YAML_KAKA}" up -d
  fi
}

function toggle_peer() {

  if [ "$1" = "-pst" ]; then
    info "start"
    docker-compose -f "${YAML_PEER}" up -d
  elif [ "$1" = "-psp" ]; then
    info "stop"
    docker-compose -f "${YAML_PEER}" down

    if [ -n $(docker images | grep dev) ]; then
      echo $(docker images | grep dev) | awk -F " " '{cmd="docker rmi -f ";system(cmd $1)}'
    fi

  elif [ "$1" = "-prst" ]; then
    info "restart"
    # restart docker
    service docker restart
    # restart peer
    docker-compose -f "${YAML_PEER}" down
    docker-compose -f "${YAML_PEER}" up -d

    # Cancel the chaincode that is installed
    if [ -n $(docker images | grep dev) ]; then
      echo $(docker images | grep dev) | awk -F " " '{cmd="docker rmi -f ";system(cmd $1)}'
    fi

  fi
}


function info() {
  if [ "${HOSTNAME}" = "orderer.hockshop.com" ]; then
    PEER=$(echo ${HOSTNAME} | awk -F '.' '{print $1}')
    echo
    echo "===================== \"NODE ${PEER} ${1}\" ===================== "
    echo
  else
    PEER=$(echo ${HOSTNAME} | awk -F '.' '{print $1 "-" $2}')
    echo
    echo "===================== \"NODE ${PEER} ${1}\" ===================== "
    echo
  fi
}




if [ "$1" = "-ost" -o "$1" = "-osp" -o "$1" = "-orst" ]; then
  # echo "$1"
  toggle_orderer "$1"
elif [ "$1" = "-pst" -o "$1" = "-psp" -o "$1" = "-prst"  ]; then
  # echo "$1"
  toggle_peer "$1"
elif [ "$1" = "-zst" -o "$1" = "-zsp" -o "$1" = "-zrst" ]; then
  # echo "$1"
  toggle_zookeeper "$1"
elif [ "$1" = "-kst" -o "$1" = "-ksp" -o "$1" = "-krst" ]; then
  # echo "$1"
  toggle_kafka "$1"
fi
