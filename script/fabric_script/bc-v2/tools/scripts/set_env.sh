#!/bin/bash
# public key path
PUB_KEY="/root/.ssh/id_rsa.pub"
# fabric respository path
RESPOSITORY="$PWD/.."
# tools path
TOOLS_PATH="${RESPOSITORY}/tools"
# hosts info files path
HOSTS_INFO="${TOOLS_PATH}/config/hosts"
# basic scripts files path
SCRIPT_PATH="${TOOLS_PATH}/scripts"
# logs output path
LOGS_PATH="${TOOLS_PATH}/logs"
# fabric working path
WORKING_DIR="/opt/gopath/src/hockshop"
# important files path
RETEN_PATH="${TOOLS_PATH}/retention"
GENESIS_PATH="${RETEN_PATH}/basic"
CHANNELBAK="${RETEN_PATH}/channelbak"
CHANNELNAME="hschannel"
# ONode path
# ONODE_PATH="${TOOLS_PATH}/ONode"
# # PNode path
# PNODE_PATH="${TOOLS_PATH}/PNode"
