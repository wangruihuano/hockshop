#!/bin/bash

# #############################################
#  功能：1.配置ssh免密登录
#       2.配置fabric集群hostname
#  依赖文件：hosts-info.txt
#  文件格式：IP HOSTNAME USERNAME
# #############################################

#  hosts information
. $PWD/scripts/set_env.sh

# public key directory
: ${PUB_KEY:="/root/.ssh/id_rsa.pub"}

# Configing SSH non-secret login
function ssh_free_pass() {
  # generate public key
  ssh-keygen -t rsa
  # rewrite hosts
  echo -e "127.0.0.1 localhost\n::1 localhost" >/etc/hosts
  awk -F ' ' '{print $1" "$2}' ${HOSTS_INFO} >>/etc/hosts
  # copy hosts files
  echo "Copy the hosts to other machine ..."
  awk -F ' ' '{if(NR>1){cmd="scp /etc/hosts "$3"@"$1":/etc/hosts";system(cmd)} }' ${HOSTS_INFO}
  # copy public key
  awk -F ' ' '{cmd="ssh-copy-id -i "$3"@"$1"";system(cmd)}' ${HOSTS_INFO}
}

# Configing the fabric cluster`s hostname
function edit_hostname() {
  if [ $1 -eq 0 ]; then
    awk -F' ' '/orderer/{print $2}' ${HOSTS_INFO} >/etc/hostname
    shutdown -r now
  elif [ $1 -eq 1 ]; then
    # configure hostname
    echo "write hostname  ..."
    awk -F ' ' '{if(NR>1){cmd="ssh "$3"@"$1" \"echo "$2" >/etc/hostname;shutdown -r now\"";system(cmd)} }' ${HOSTS_INFO}
    sleep 5
  fi
}

echo "Configing the ssh free..."
ssh_free_pass
# 0 : orderer 1 : peers
echo "Configing the hostname..."
edit_hostname 1
edit_hostname 0
