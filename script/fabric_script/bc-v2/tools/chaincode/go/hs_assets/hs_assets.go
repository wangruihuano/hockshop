package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

type SimpleChaincode struct {
}

// REVIEW:
type hs_assets struct {
	// Id         string `json:"id"`
	Md5        string `json:"md5"`
	User       string `json:"user"`
	Info       string `json:"info"`
	InfoMd5    string `json:"infoMd5"`
	Filename   string `json:"filename"`
	Timestamp  string `json:"timestamp"`
	OprateCode string `json:"oprateCode"`
	AssetsType string `json:"asstesType"`
	LastID     string `json:"lastID"`
	Parsed     string `json:"parsed"`
	Other      string `json:"other"`
}

// ===================================================================================
// Main
// ===================================================================================
func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// Init initializes chaincode
// ===========================
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke - Our entry point for Invocations
// ========================================
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	// Handle different functions
	if function == "push" { //create a new marble
		return t.push(stub, args)
	} else if function == "pull" { //change owner of a specific marble
		return t.pull(stub, args)
	} else if function == "cancel" { //transfer all marbles of a certain color
		return t.cancel(stub, args)
	} else if function == "pullByOwner" {
		return t.pullByOwner(stub, args)
	} else if function == "pullAssetsHistoryByID" { //read a marble
		return t.pullAssetsHistoryByID(stub, args)
	}

	fmt.Println("invoke did not find func: " + function) //error
	return shim.Error("Received unknown function invocation")
}

/*

	Push assets information to blockchain.

*/
func (t *SimpleChaincode) push(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var err error

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 11")
	}

	id := args[0]
	// ==== Check if marble already exists ====
	assetsAsBytes, err := stub.GetState(id)

	if err != nil {
		return shim.Error("Failed to get assets" + err.Error())
	} else if assetsAsBytes != nil {
		return shim.Error("This assets already exists: " + id)
	}

	var assets hs_assets
	assetsInfoJson := args[1]
	err = json.Unmarshal([]byte(assetsInfoJson), &assets)

	if err != nil {
		return shim.Error(err.Error())
	}

	assetsJSONasBytes, err := json.Marshal(assets)

	if err != nil {
		return shim.Error(err.Error())
	}
	err = stub.PutState(id, assetsJSONasBytes)

	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

/*

	Pull assets from blockchain network.

*/
func (t *SimpleChaincode) pull(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var id, jsonResp string

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting assetsId of the assets to query")
	}

	id = args[0]
	valAsbytes, err := stub.GetState(id)

	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + id + "\"}"
		return shim.Error(jsonResp)
	} else if valAsbytes == nil {
		jsonResp = "{\"Error\":\"The assets does not exist: " + id + "\"}"
		return shim.Error(jsonResp)
	}
	return shim.Success(valAsbytes)
}

/*

	Cancle asset information

*/
func (t *SimpleChaincode) cancel(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var jsonResp string
	// var assets hs_assets

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	id := args[0]
	// REVIEW: Index
	err := stub.DelState(id)

	if err != nil {
		jsonResp = "{\"Error\":\"Failed to delete state for " + id + "\"}"
		return shim.Error(jsonResp)
	}

	return shim.Success(nil)
}

/*

	Gets one's all assets information

*/
func (t *SimpleChaincode) pullByOwner(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	owner := strings.ToLower(args[0])
	//// REVIEW:
	queryString := fmt.Sprintf("{\"selector\":{\"user\":\"%s\"}}", owner)

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

// =========================================================================================
// getQueryResultForQueryString executes the passed in query string.
// Result set is built and returned as a byte array containing the JSON results.
// =========================================================================================
func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryRecords
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.Bytes(), nil
}

/*

  Get an asset ID history

*/
func (t *SimpleChaincode) pullAssetsHistoryByID(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	currentID := args[0]

	it, err := stub.GetHistoryForKey(currentID)

	if err != nil {
		return shim.Error(err.Error())
	}
	var result, _ = getHistoryListResult(it)
	return shim.Success(result)
}

func getHistoryListResult(resultsIterator shim.HistoryQueryIteratorInterface) ([]byte, error) {

	defer resultsIterator.Close()
	// buffer is a JSON array containing QueryRecords
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		item, _ := json.Marshal(queryResponse)
		buffer.Write(item)
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")
	fmt.Printf("queryResult:\n%s\n", buffer.String())
	return buffer.Bytes(), nil
}
