#!/bin/bash

function toggle_fabric_network() {
  . /root/bc-v2/tools/scripts/set_env.sh

  SUB_TOGGLE="$PWD/scripts/toggle.sh"
  CLUSTER_SHELL="$PWD/scripts/cluster_shell.sh"

  if [ "$1" = "-st" ]; then

    cat ${HOSTS_INFO} |grep orderer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -zst \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep peer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -kst \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep orderer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -ost \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep peer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -pst \"";system(cmd)}'

  elif [ "$1" = "-sp" ]; then

    cat ${HOSTS_INFO} |grep peer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -psp \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep orderer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -osp \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep peer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -ksp \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep orderer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -zsp \"";system(cmd)}'


  elif [ "$1" = "-rst" ]; then

    # RESTART FABRIC NETWORKE
    cat ${HOSTS_INFO} |grep peer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -psp \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep orderer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -osp \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep peer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -ksp \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep orderer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -zsp \"";system(cmd)}'


    cat ${HOSTS_INFO} |grep orderer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -zst \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep peer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -kst \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep orderer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -ost \"";system(cmd)}'
    sleep 2
    cat ${HOSTS_INFO} |grep peer |awk -F ' ' '{cmd="ssh root@"$1" \"bash '"${SUB_TOGGLE}"' -pst \"";system(cmd)}'

  fi
}

toggle_fabric_network "$@"
