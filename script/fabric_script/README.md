Hyperledger fabric 1.0 部署脚本
-------------------------------

---

NOTETICE：
----------

部署环境：CentOS 7

go语言版本：1.8.3

DockerCE版本：18.03

Docker Compose:1.19
### NO.1 (bc-v2)工具包的目录一览

```shell
$ tree bc-v2/
bc-v2/
├── images
│   ├── baseos.tar
│   ├── ccenv.tar
│   ├── javaenv.tar
│   ├── orderer.tar
│   ├── peer.tar
│   ├── tools.tar
└── tools
    ├── logs
    ├── ONode
    │   ├── base
    │   │   └── docker-compose-base.yaml
    │   └── docker-compose-orderer.yaml
    ├── PNode
    │   ├── base
    │   │   ├── docker-compose-base.yaml
    │   │   └── peer-base.yaml
    │   ├── docker-compose-peer0.yaml
    │   ├── docker-compose-peer1.yaml
    │   ├── docker-compose-peer2.yaml
    │   └── docker-compose-peer3.yaml
    ├── retention
    │   ├── basic
    │   │   ├── configtx.yaml
    │   │   ├── crypto-config.yaml
    │   │   ├── daemon.json
    │   │   └── hosts-info.txt
    │   ├── bin
    │   │   ├── configtxgen
    │   │   ├── configtxlator
    │   │   ├── cryptogen
    │   │   ├── get-byfn.sh
    │   │   ├── get-docker-images.sh
    │   │   ├── orderer
    │   │   └── peer
    │   ├── chaincode
    │   │   └── assets
    │   │       └── assets.go
    │   ├── channelbak
    │   ├── docker-compose
    │   │   └── docker-compose-Linux-x86_64
    │   └── fabric
    └── scripts
        ├── base
        │   ├── assets.sh
        │   ├── cluster_shell.sh
        │   ├── docker_cp.sh
        │   ├── joinAndInstall.sh
        │   ├── script.sh
        │   ├── send_files.sh
        │   ├── set_env.sh
        │   └── toggle.sh
        ├── deploy_fabric_env.sh
        ├── generate_fabric_cluster.sh
        ├── README.md
        ├── reback.sh
        ├── ssh-non-secret.sh
        └── toggle_cluster.sh
```


### NO.2 Hyperledger Fabric 部署示例

-	Tasks: 创建一个主机节点orderer和4个peer节点

| service |      IP       |          NAME           | USRE |
|:-------:|:-------------:|:-----------------------:|:----:|
| orderer | 192.168.0.183 |  orderer.hockshop.com   | root |
|  peer   | 192.168.0.185 | peer0.org1.hockshop.com | root |
|  peer   | 192.168.0.122 | peer1.org1.hockshop.com | root |
|  peer   | 192.168.0.182 | peer0.org2.hockshop.com | root |
|  peer   | 192.168.0.174 | peer1.org2.hockshop.com | root |

#### 在orderer节点进行脚本工具的构建
* 相关链接可以参考 NO.3
* 将事先下载好的fabric镜像导出成压缩包后放入{bc-v2}/images路径下
```
$ docker pull hyperledger/fabric-peer:x86_64-1.0.0
$ docker images # 查看tag
$ docker save ${tag} >{bc-v2}/images/peer.tar #将查看到的tag替换${tag}
$ ...
$ tree {bc-v2}/images
  ├── baseos.tar
  ├── ccenv.tar
  ├── javaenv.tar
  ├── orderer.tar
  ├── peer.tar
  ├── tools.tar
```
* 将下载好的docker-compose放入{bc-v2}/tools/retention/docker-compose,并命名
```
$ mv x {bc-v2}/tools/retention/docker-compose/docker-compose-Linux-x86_64
```
* 下载fabric二进制文件并将其放入{bc-v2}/tools/retention/
```
$ sudo curl -O https://nexus.hyperledger.org/content/repositories/releases/org/hyperledger/fabric/hyperledger-fabric/linux-amd64-1.0.0/hyperledger-fabric-linux-amd64-1.0.0.tar.gz
$ tar -zxvf hyperledger-fabric-linux-amd64-1.0.0.tar.gz
```
#### 获取所有主机IP，修改host-info.txt
```
$ vi ${bc-v2}/tools/retention/basic/host-info.txt
```
#### 配置hostname和ssh免密登录
* 将工具包首先发送到orderer节点
```
$ cd ${bc-v2}/tools/scripts/
$ ./ssh-non-secret.sh
```
#### 分发工具包
```
$ ./base/cluster_shell.sh -o {bc-v2} {path}
```
#### 安装环境
```
$ ./deploy_fabric_env.sh
```
#### 创建fabric集群
```
$ ./generate_fabric_cluster.sh
```
#### 启动集群

```
$ ./toggle_cluster.sh -st
```
#### 安装链码
```shell
$ ssh root@peer0.org1.hockshop.com
$ docker exec -it cli bash
$ root@xxx:/opt/gopath/src/hockshop/org/peer#./scripts/assets.sh
```

### NO.3 参考链接

[DokerCE 的安装](https://docs.docker.com/install/linux/docker-ce/centos/)

[Docker Compose 的安装](https://docs.docker.com/compose/install/#prerequisites)

[Fabric 的安装](http://hyperledger-fabric.readthedocs.io/en/latest/samples.html#download-platform-specific-binaries)
