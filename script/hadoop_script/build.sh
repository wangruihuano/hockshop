#!/usr/bin/env bash
# 构建镜像
docker build -t licte/cent-java:latest ./centos_java8
docker build -t licte/hadoop-base:latest ./hadoop_base
docker build -t licte/hadoop-master:latest ./hadoop_master
docker build -t licte/hadoop-slave:latest ./hadoop_base
