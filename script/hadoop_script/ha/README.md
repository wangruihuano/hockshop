# HADOOP HDFS 高可用部署

**示意图如下所示,其中ZK为zookeeper节点,JN为journal节点,NN为namenode节点,DN为datanode节点.**
![高可用示意图](/doc/img/HADOOP_HDFS_HA.png)

## 准备

*在搭建高可用集群之前,我们建议你先熟悉[单台namenode集群搭建](../README.md).*

- 拥有java8以上的linux主机(至少三台)
- 已搭建好zookeeper集群

为便于描述,我们以四台主机为例进行说明,所启动的服务如下所示.

|主机名|namenode|journalnode|datanode|DFSZKFailoverController|
|-----|:------:|:---------:|:------:|:-------:|
|hadoop01|√|√|√|√|
|hadoop02|√|√|√|√|
|hadoop03||√|√||
|hadoop04|||√||

## 一.安装

### 1. 下载

在本项目中,我们使用的hadoop版本为2.8.4,下载地址[http://mirror.bit.edu.cn/apache/hadoop/common/hadoop-2.8.4/hadoop-2.8.4.tar.gz](http://mirror.bit.edu.cn/apache/hadoop/common/hadoop-2.8.4/hadoop-2.8.4.tar.gz).

### 2. 解压

下载之后在下载目录执行 `tar zxvf hadoop-2.8.4.tar.gz`.

## 二.配置

由于本项目只使用到hadoop中的hdfs服务,下面仅介绍关于hdfs相关的配置.其中包括 *core-site.xml*, *hdfs-site.xml*  ,*hadoop-env.sh* 和 *slaves*,你需要根据主机名等信息参考下方示例进行修改,修改之后覆盖掉hadoop安装目录中 `hadoop-2.8.4/etc/hadoop/`目录下原有的这三个配置文件.

- ### core-site.xml

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

    <configuration>
        <!-- 指定nameservice的名称 -->
        <property>
            <name>fs.defaultFS</name>
            <value>hdfs://mycluster</value>
        </property>

        <!-- 编辑日志文件路径  -->
        <property>
          <name>dfs.journalnode.edits.dir</name>
          <value>/root/hadoop/data/jn</value>
        </property>

        <!-- 缓冲文件目录 -->
        <property>
            <name>hadoop.tmp.dir</name>
            <value>file:/root/hadoop/data/tmp</value>
        </property>
        <property>
            <name>io.file.buffer.size</name>
            <value>131072</value>
        </property>

    </configuration>
    ```

- ### hdfs-site.xml

    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <?xml-stylesheet type="text/xsl" href="configuration.xsl"?>

    <configuration>
    	<!-- 指定nameservice的名称 -->
    	<property>
    	  <name>dfs.nameservices</name>
    	  <value>mycluster</value>
    	</property>

    	<!-- 指定两个namenode的名称 -->
    	<property>
    	  <name>dfs.ha.namenodes.mycluster</name>
    	  <value>nn1,nn2</value>
    	</property>

    	<!-- 配置namenode的rpc地址及端口 -->
    	<property>
    	  <name>dfs.namenode.rpc-address.mycluster.nn1</name>
    	  <value>hadoop01:8020</value>
    	</property>
    	<property>
    	  <name>dfs.namenode.rpc-address.mycluster.nn2</name>
    	  <value>hadoop02:8020</value>
    	</property>

    	<!-- 配置namenode的http地址及端口 -->
    	<property>
    	  <name>dfs.namenode.http-address.mycluster.nn1</name>
    	  <value>hadoop01:50070</value>
    	</property>
    	<property>
    	  <name>dfs.namenode.http-address.mycluster.nn2</name>
    	  <value>hadoop02:50070</value>
    	</property>

    	<!-- 指定namenode的元数据在journalnode中的存储路径 -->
    	<property>
    	  <name>dfs.namenode.shared.edits.dir</name>
    	  <value>qjournal://hadoop01:8485;hadoop02:8485;hadoop03:8485/mycluster</value>
    	</property>

    	<!-- 失败自动切换方式 -->
    	<property>
    	  <name>dfs.client.failover.proxy.provider.mycluster</name>
    	  <value>org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider</value>
    	</property>

    	<!-- 隔离机制 -->
    	<property>
          <name>dfs.ha.fencing.methods</name>
          <value>sshfence</value>
        </property>

        <property>
          <name>dfs.ha.fencing.ssh.private-key-files</name>
          <value>/root/.ssh/id_rsa</value>
        </property>

    	<!-- 文件备份数量 -->
    	<property>
            <name>dfs.replication</name>
            <value>3</value>
        </property>

    	<!-- 是否开启权限验证 -->
    	<property>
    		<name>dfs.permissions.enabled</name>
    		<value>flase</value>
    	</property>

    	<!-- 故障自动转移 -->
    	<property>
    	   <name>dfs.ha.automatic-failover.enabled</name>
    	   <value>true</value>
    	 </property>

    	 <!-- zookeeper集群地址及端口 -->
    	 <property>
    	   <name>ha.zookeeper.quorum</name>
    	   <value>zk1.licte.cn:2181,zk2.licte.cn:2181,zk3.licte.cn:2181</value>
    	 </property>

    	<property>
            <name>dfs.namenode.name.dir</name>
            <value>file:/root/hadoop/name</value>
        </property>

        <property>
            <name>dfs.datanode.data.dir</name>
            <value>file:/root/hadoop/data</value>
        </property>

    	<!-- 检查ip和主机名对应 -->
    	<property>
    		<name>dfs.namenode.datanode.registration.ip-hostname-check</name>
    		<value>false</value>
    	</property>

    </configuration>
    ```

- ### hadoop-env.sh

    修改其中的变量为系统中安装的java安装目录(JAVA_HOME).
    ```bash
    export JAVA_HOME=/usr/lib/jvm/java-1.8.0
    ```

- ### slaves

    将启动datanode节点主机名配置其中.
    ```
    hadoop01
    hadoop02
    hadoop03
    hadoop04
    ```

## 三.启动

### 0.配置免密登录

namenode所在的两台主机需要配置免密登录其他主机.

若主机之间无法通过主机名进行交互,则需要添加 `/etc/hosts`文件进行主机名及ip的映射.(所有主机均需要)

在各个主机执行 `ssh-keygen -t rsa`生成密钥对.

在作为namenode的两台主机执行 `ssh-copy-id ${user}@${hostname}` ,将其中的${user}${hostname}分别替换为登录其他主机的登录户用名及其主机名.

例子:
```bash
### hadoop01,hadoop02都执行
ssh-copy-id root@hadoop01
ssh-copy-id root@hadoop02
ssh-copy-id root@hadoop03
ssh-copy-id root@hadoop04
```

配置之后可以通过执行 `ssh ${user}@${hostname}` 验证是否配置成功,若配置成功,则会直接登录至${hostname}主机.

### 1.分发内容

将解压并修改配置后的hadoop复制到其他节点,执行 `scp -r hadoop-2.8.4/  ${user}@${hostname}:${path}`进行复制,其中${path}为服务到其他主机的路径,为便于管理建议与本机一致.

### 2.启动journalnode

在hadoop01主机的hadoop安装目录下执行 `sbin/hadoop-daemons.sh start journalnode`进行集群的journalnode启动.

### 3.格式化文件系统

在hadoop01主机的hadoop安装目录下执行 `bin/hadoop namenode -format`进行namenode格式化.

格式化之后执行 `sbin/hadoop-daemon.sh start namenode`启动namenode.

启动之后在hadoop02主机上的hadoop安装目录中执行 `bin/hdfs namenode -bootstrapStandby`进行元数据的同步.

### 4.初始化HA状态

在hadoop01主机的hadoop安装目录下执行 `bin/hdfs zkfc -formatZK`进行初始化HA状态.

### 5.重启集群

在任一namenode所在主机执行 `sbin/stop-dfs.sh`关闭dfs服务后执行 `sbin/start-dfs.sh`启动服务.

## 四.测试

打开浏览器访问 http://hadoop01:50070 和 http://hadoop02:50070 查看.
