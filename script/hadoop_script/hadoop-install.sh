#!/bin/bash

hadoop_package_location="./hadoop_base/"
hadoop_package_name="hadoop-2.8.3.tar.gz"
hadoop_dir_name="hadoop-2.8.3"
hadoop_cfg_dir="./hadoop_base/hadoop_cfg"

jdk_package_name="jdk-8u144-linux-x64.rpm"
jdk_package_location="./"

#hosts附带登录账户文件
hosts_file_name="hosts-login.txt"
#CentOS ssh公钥
pub_key=".ssh/id_rsa.pub"

#创建配置文件
generate_hadoop_cfg(){
	for j in `seq 0 ${i}`
	do
		#创建slave文件
		echo ${hostnames[$j]} >> ${hadoop_cfg_dir}/slaves
	done
}


#配置公钥免密登录
ssh-keygen -t rsa
i=0 #便于统计定义变量

#重写hosts文件
echo -e "127.0.0.1 localhost\n::1 localhost" > /etc/hosts

while read line
do
	ips[${i}]=`echo ${line} | cut -d ' ' -f 1`
	hostnames[${i}]=`echo ${line} | cut -d ' ' -f 2`
	logins[${i}]=`echo ${line} | cut -d ' ' -f 3`

  #追加hosts
	echo "${ips[${i}]} ${hostnames[${i}]}" >> /etc/hosts
	#复制公钥到slave节点
	ssh-copy-id -i /${logins}/${pub_key} ${logins[${i}]}@${ips[${i}]}
	echo -e "\033[36m 复制公钥到${hostnames[$i]} \033[0m"

	i=`expr ${i} + 1`
done<${hosts_file_name}

#为每台机器安装jdk
install_jdk_for_slaves(){
	for j in `seq 1 ${i}`
	do
		echo -e "\033[36m 复制jdk到${hostnames[$j]} \033[0m"
		scp ${jdk_package_location}${jdk_package_name} ${login[$j]}@${hostnames[$j]}:/${login[$j]}
		ssh ${logins[$j]}@${hostnames[$j]} "rpm -ivh /${login[$j]}/${jdk_package_name}"
	done
}

rpm -ivh ${jdk_package_location}${jdk_package_name}
generate_hadoop_cfg
install_jdk_for_slaves

hadoop_location="/root/hadoop"
mkdir ${hadoop_location}
mkdir ${hadoop_location}/tmp #此处配置应与core-site.xml文件中hadoop.tmp.dir的value值相同
mkdir ${hadoop_location}/var  #mapred-site.xml mapred.local.dir
mkdir ${hadoop_location}/dfs
mkdir ${hadoop_location}/dfs/name  #hdfs-site.xml dfs.namenode.name.dir
mkdir ${hadoop_location}/dfs/data #hdfs-site.xml dfs.datanode.data.dir


#解压到指定目录
tar zxvf ${hadoop_package_location}${hadoop_package_name} -C $hadoop_location
#拷贝hadoop配置文件
cp ${hadoop_cfg_dir}/* ${hadoop_location}/${hadoop_dir_name}/etc/hadoop/

#复制集群
for j in `seq 1 ${i}`
do
		echo -e "\033[36m 复制hadoop文件夹到${hostnames[$j]} \033[0m"
    scp -r ${hadoop_location} ${login[$j]}@${hostnames[$j]}:/root
		scp /etc/hosts ${login[$j]}@${hostnames[$j]}:/etc/hosts
done
