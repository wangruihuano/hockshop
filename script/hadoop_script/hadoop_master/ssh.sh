#! /bin/bash
# 配置ssh免密登陆

#hosts附带登录账户文件
hosts_file_name="./hosts"

# hadoop配置文件目录
hadoop_cfg="${HADOOP_HOME}/etc/hadoop"
# slaves文件
slaves="${hadoop_cfg}/slaves"

master_hostname=`hostname`
master_eth0_ip=`ifconfig eth0 | grep "inet addr:" | awk '{print $2}' | cut -c 6-`
#重写hosts文件
#echo -e "127.0.0.1 localhost\n::1 localhost" > /etc/hosts
echo -e "${master_eth0_ip} ${master_hostname}" > /etc/hosts

# 重写slaves文件
mv ${slaves} ${slaves}.old.`date -d today +"%Y-%m-%d-%H:%M:%S"`
touch ${slaves}

#cat /proc/sys/kernel/random/uuid

i=0

while read line
do
	ip[${i}]=`echo ${line} | cut -d ' ' -f 1`
	hostname[${i}]=`echo ${line} | cut -d ' ' -f 2`
	login[${i}]=`echo ${line} | cut -d ' ' -f 3`

    #追加hosts
	echo "${ip[${i}]} ${hostname[${i}]}" >> /etc/hosts
	echo "${hostname[${i}]}" >> ${slaves}


	i=`expr ${i} + 1`

done<${hosts_file_name}

i=0
while read line
do

	scp /etc/hosts ${login[${i}]}@${ip[${i}]}:/etc
	scp ${slaves} ${login[${i}]}@${ip[${i}]}:${slaves}
	scp -r ${hadoop_cfg}/* ${login[${i}]}@${ip[${i}]}:${hadoop_cfg}/
	set -e -x
	i=`expr ${i} + 1`

done<${hosts_file_name}