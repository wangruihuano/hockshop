# Hadoop一键部署脚本

-------------

## 不使用docker部署

适用Hadoop版本:2.8.3,[下载地址](http://www.apache.org/dyn/closer.cgi/hadoop/common/hadoop-2.8.3/hadoop-2.8.3.tar.gz)

JDK版本:jdk-8u144-linux-x64.rpm,[官网地址](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

操作系统:CentOS 7

使用脚本前请先将hadoop,jdk下载至此项目中

### 步骤一:

将本目录下所有内容拷贝至将要部署hadoop-master节点的主机中.

并根据实际情况更改[hadoop-install.sh](./hadoop-install.sh)文件中如下几项

```shell
hadoop_package_location="./" #Hadoop压缩包放置位置

hadoop_package_name="hadoop-2.8.3.tar.gz" #Hadoop压缩包名称

hadoop_dir_name="hadoop-2.8.3" #解压后Hadoop根文件夹名称

jdk_package_name="jdk-8u144-linux-x64.rpm" #jdk安装包名称

jdk_package_location="./" #jdk安装包路径

```

### 步骤二:

按照样例更改[hosts-login.txt](./hosts-login.txt),ip地址,主机名,登录用户名,之间用空格分隔,**namenode主节点必须位于文件第一行**

```
192.168.0.104 hadoop.master root
```

### 步骤三

根据实际情况更改[hadoop-env.sh](./hadoop_base/hadoop_cfg/hadoop-env.sh)脚本中JAVA环境变量(rpm安装后默认为/usr/java/jdk1.8.0_144)

```shell
export JAVA_HOME=${JAVA_HOME}
```

### 步骤四

[core-site.xml](./hadoop_base/hadoop_cfg/core-site.xml),[hdfs-site.xml](./hadoop_base/hadoop_cfg/hdfs-site.xml),[mapred-site.xml](./hadoop_base/hadoop_cfg/mapred-site.xml),[yarn-site.xml](./hadoop_base/hadoop_cfg/yarn-site.xml)这四个配置文件参考[Hadoop官网](http://hadoop.apache.org/docs/r2.8.3/hadoop-project-dist/hadoop-common/ClusterSetup.html)进行进一步配置

### 步骤五
执行[hadoop-install.sh](./hadoop-install.sh)
**执行过程中需要输入ssh确认信息及其他slaves节点账户密码**
安装完成后,启动Hadoop节点执行如下指令,hadoop_location默认值为/root/hadoop,hadoop_dir_name为步骤一中配置值

```shell
cd ${hadoop_location}/${hadoop_dir_name}
./bin/hadoop namenode -format #格式化文件系统
./sbin/start-all.sh #启动hdfs及yard
jps #查看节点已启动的服务
```

打开浏览器访问http://namenode:50070,namenode为主节点Ip

CentOS7添加防火墙规则
```shell
firewall-cmd --zone=dmz --add-port=50070/tcp #允许入站端口50070
firewall-cmd --reload #更新防火墙规则
```
若添加规则仍无效,可以关闭防火墙
```shell
systemctl stop firewalld.service #停止firewall
```

------------------

## 使用docker部署

### 前提条件

目前测试环境为CentOS 7.0, docker版本为1.13.1

docker安装参考[这里](http://www.runoob.com/docker/centos-docker-install.html).

构建镜像前请将[hadoop-2.8.3.tar.gz](http://www.apache.org/dyn/closer.cgi/hadoop/common/hadoop-2.8.3/hadoop-2.8.3.tar.gz)下载至[hadoop_base目录下](./hadoop_base/).

确保服务器主机时间正确!

**下述所有操作均在centos管理员账户root下执行**

### 同步服务器时间(可选)

安装ntpdate同步时间

```bash
 yum install -y ntpdate
 ntpdate asia.pool.ntp.org
```

### 构建镜像

部署中需要构建四个镜像,java, base,slave 和 master,由于master镜像和slave镜像基于 base 构建，base 基于centos-java,
所以你需要从底到上进行构建.


**我们提供了一个自动化[脚本](./build.sh)用以构建镜像,你可以将通过执行 `chmod +x ./build.sh && ./build.sh`进行构建,
当然，你也可以通过以下几个步骤手动构建,以便了解流程**

**构建时间与机器性能及网络状态有关,若构建成功将会有 `Successfully built cbdfb7666b10`,
   其中 `cbdfb7666b10` 为镜像id,不同镜像构建之后该值会有所不同.**

1. centos-java构建:

    在[centos_java8](./centos_java8)目录下执行 `docker build -t licte/cent-java:latest .`.

2. hadoop-base构建:

    *若有必要,在此镜像构建前，你可以进入[hadoop配置文件目录](./hadoop_base/hadoop_cfg)根据需要更改配置信息,我们提供了一些默认值，更改时请注意同步更改构建本镜像的[Dockerfile](./hadoop_base/Dockerfile)中的ARG值,目前版本中与配置文件相关的参数包括: HADOOP_HDFS_NAMENODE, HADOOP_HDFS_DATANODE 和 HADOOP_TMP_DIR .*

    在[hadoop_base](./hadoop_base)目录下执行 `docker build -t licte/hadoop-base:latest .`.

3. hadoop-master构建:

    在[hadoop_master](./hadoop_master)目录中执行 `docker build -t licte/hadoop-master:latest .`.

4. hadoop-slave构建:

    ~~*公钥配置已废弃.注意:为方便Hadoop后续运行，我们将name节点公钥分发过程放在镜像构建中,所以在slave镜像构建前,你可以首先启动一个name节点: `docker run -p 50070:50070 -p 8088:8088 --name=master --hostname=hadoop.master licte/hadoop-master`,启动之后将会打印出该节点的公钥信息，将该公钥复制到[hadoop_slave下的id_rsa.pub文件](./hadoop_slave/id_rsa.pub)中，再进行下面步骤.当然你也可以在镜像构建之后手动将公钥内容复制到slave节点的 `/root/.ssh/authorized_keys`文件中,其作用是一样的*~~

    在[hadoop_slave](./hadoop_slave)目录下执行 `docker build -t licte/hadoop-slave:latest .`.

5. 查看镜像:

    执行 `docker images` 查看镜像列表,列表中应该包含以下内容:
    ```
    REPOSITORY                    TAG                 IMAGE ID            CREATED              SIZE
    licte/hadoop-slave            latest              050011e9b4a8        10 seconds ago       1.83 GB
    licte/hadoop-master           latest              4e3bdb2f1aff        About a minute ago   1.91 GB
    licte/hadoop-base             latest              9f4b4f0f8d5f        3 minutes ago        1.83 GB
    licte/cent-java               latest              fd370190e217        13 minutes ago       783 MB
    hub.c.163.com/public/centos   6.5                 997f0ed97903        2 years ago          442 MB
    ```

### 创建docker swarm网络

由于hadoop以集群发挥其作用,将全部容器放置在一台主机内失去其分布式作用,所以容器间跨主机通信十分必要.

此案例中有三台主机,一台运行hadoop-master容器,称其HM,其余两台运行slave容器,分别称为HS1,HS2.

这三台主机均安装了docker,都有上述镜像,为便于测试,在此我们可以先将防火墙关闭 `systemctl stop firewalld` .

在HM主机运行 `docker swarm init`,运行之后将会有token口令，请记下该token用于其他主机加入该网络.

在HS1和HS2都执行 ` docker swarm join --token ${TOKEN} ${IP}:2377`,${TOKEN}为上一步生成的值,${IP}为HM主机的ip.加入成功之后将会有 `This node joined a swarm as a worker.`的提示.

在HM主机运行 `docker node ls`,应该类似所示

```
ID                           HOSTNAME  STATUS  AVAILABILITY  MANAGER STATUS
f3j741he8o6tkjv7vvue36t48 *  HM        Ready   Active        Leader
ux2mb9d1kpm0ldut65r3820og    HS1       Ready   Active        
```
创建自定义docker网络:(在主机HM创建)
`docker network create -d overlay --attachable hadoop-net`

执行 `docker network ls`,应该包含以下内容:
```
NETWORK ID          NAME                DRIVER              SCOPE
c1c53e22e54b        bridge              bridge              local
95dbfa451619        docker_gwbridge     bridge              local
xllhrzzj8dmq        hadoop-net          overlay             swarm
e2291caef7c6        host                host                local
k15ytvl4yvj3        ingress             overlay             swarm
a112ae08ef4f        none                null                local
```

### 运行及配置容器

根据需要，启动一个master节点及若干个slave节点.

启动 namenode:

```bash
docker run -itd \
        --net=hadoop-net \
        --name=master \
        -p 50070:50070 \
        -p 8088:8088 \
        -p 9000:9000 \
        -p 8042:8042 \
        --hostname=hadoop.master \
        licte/hadoop-master
```

启动 datanode,(根据需要更改name及hostname):
```bash
docker run -itd \
                --name slave1 \
                --hostname hadoop.slave1 \
                -p 50075:50075 \
                --net=hadoop-net \
                licte/hadoop-slave
```

容器启动之后进入master容器( `docker exec -it master bash`),执行 `ls`,将会看到以下文件

```
anaconda-ks.cfg  hdfs hosts ssh.sh hosts.example
```

你需要根据slave的节点信息参考 hosts.example修改 hosts 文件，

~~**查看容器ip可以通过在宿主机执行 `sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' ${?}` ,将${?}替换为容器的name或者id即可**~~

*查看容器ip可以通过在宿主机执行 `docker exec -it ${?} ip addr`,将${?}替换为容器的name或者id即可*

修改之后执行 `./ssh.sh` 进行ssh免密登陆配置，该脚本执行过程中需要按提示输入信息(yes/no).

执行之后,你就可以进入${HADOOP_HOME}下，执行 `sbin/start-all.sh` 启动hadoop集群. **第一次启动集群需要进行确认公钥,需按提示输入(yes/no)**

启动之后,可以通过在${HADOOP_HOME}/bin目录下执行 `./hadoop dfsadmin -report` 查看集群信息，
也可以通过浏览器访问 *http://xx.xx.xx.xx:50070* 查看集群信息,其中 *xx.xx.xx.xx* 为部署master容器的宿主机的ip.

mapreduce例子:
```bash
# 上传 ${HADOOP_HOME}/bin/hadoop 脚本到dfs系统的根目录下
${HADOOP_HOME}/bin/hadoop fs -put ${HADOOP_HOME}/bin/hadoop /

# 运行hadoop提供的wordcount例子
${HADOOP_HOME}/bin/hadoop jar  /usr/local/hadoop-2.8.3/share/hadoop/mapreduce/sources/hadoop-mapreduce-examples-2.8.3-sources.jar org.apache.hadoop.examples.WordCount /hadoop /out

# 查看wordcount的结果
${HADOOP_HOME}/bin/hdfs dfs -cat /out/part-r-00000
```

### 数据节点扩展及配置更改

**以下所有操作均在master容器中进行,即namenode容器**

创建新节点容器后,根据主机名,ip,登陆名修改 `/root/hosts` 文件.

hadoop配置文件在 `${HADOOP_HOME}/ect/hadoop` 目录下,主要配置文件包括 **core-site.xml,hdfs-site.xml,mapred-site.xml,yarn-site.xml**这四个.

修改文件之后，在/root目录下执行 `./ssh.sh`,即可.
