# py-face 项目部署

1. 前提准备:

    - 目前测试环境为CentOS 7.0,*由于需要通过网络安装其他python库及构建工具cmake,清确保主机能够访问公网*

    - python版本2.7.
    
    - 将[py-face](/python/py-face)项目拷贝至将要部署的主机上.为便于描述下述命令中的${your_path}为你所拷贝的路径.

**本项目通过文件路径作为参数进行程序执行,所以务必将本项目与java服务marmot部署在同一主机**

2. 安装应用

```bash
# 安装cmake
yum install cmake gcc-c++ python-devel

# 下载dlib库
curl -O http://dlib.net/files/dlib-19.13.tar.bz2
# 解压dlib库
tar -xjf ./dlib-19.13.tar.bz2
# 进入dlib目录中
cd dlib-19.13
# 安装dlib库
python setup.py install

# 安装web框架flask(若环境中无pip,可通过执行下方注释的命令进行安装)
# yum install python-pip
pip install flask
pip install numpy

# 进入前提准备中拷贝的py-face目录中,将${py-face}替换为你拷贝的目录
cd ${your_path}/py-face/
# 启动服务
python API.py
```

可以通过执行 `curl http://localhost/face?file=${your_path}/py-face/src/3.jpeg`查看是否启动,运行正确将会得到以下数据:

```json
[{
	"points": [{
		"y": 122,
		"x": 317
	}, {
		"y": 136,
		"x": 318
	},{
//	...
	}],
	"rect": {
		"top": 411,
		"left": 425,
		"right": 554,
		"bottom": 540
	}
}]
```

