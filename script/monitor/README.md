# 监控服务部署

### 1. 前提准备


由于监控服务采用JAVA开发,maven构建，运行此服务需要JAVA运行环境，版本需高于1.8，构建工具maven的版本应高于3.5.

在monitor目录中执行 `mvn package`,打包完成后,此目录中会创建target目录.

将target目录中的 *monitor-x.x-jar-with-dependencies.jar* 文件及[monitor项目中的libs文件夹](/java/monitor/libs)复制到此处.

### 2. 部署服务

根据需要，将要部署机器的ip及登陆名按照示例写入[hosts-login.txt](/script/monitor/hosts-login.txt)中.

示例：

```
192.168.0.104 root
192.168.0.167 root
192.168.0.138 root
192.168.0.103 root
```

同时，根据步骤一打包后的jar文件名称修改[monitor-install.sh](/script/monitor/monitor-install.sh)中的 `jar="monitor-1.0-SNAPSHOT-jar-with-dependencies.jar"`

此监控服务占用UDP协议进行通信，请确保UDP的10001端口未被其他程序占用或防火墙阻隔.

### 3. 测试服务

在部署完成之后，在linux下可使用 `nc` 进行服务测试.

```bash
nc -u ${host} 10001
# 之后输入r
r
```

将上述命令中的${host}替换为部署监控服务的主机名或ip即可.

若服务启动成功，返回数据像如下所示：

```json
{
    "cpu": [{
        "usage_rate": 3.261844573106092E-4
    }, {
        "usage_rate": 2.4459845087647776E-4
    }],
    "hardDisk": {
        "avail": 47,
        "free": 47,
        "total": 49,
        "used": 2
    },
    "hostname": "hadoop.master",
    "ip": "192.168.0.104",
    "memory": {
        "free": 1534888,
        "total": 1848348,
        "used": 313460
    },
    "serverStatus": "work"
}
```

### 4.错误排查

- JAVA运行环境异常.
- 端口被占用.
- 防火墙屏蔽.
