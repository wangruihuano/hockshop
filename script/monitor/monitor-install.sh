#!/usr/bin/env bash

#hosts附带登录账户文件
hosts_file_name="hosts-login.txt"

libs="libs"
jar="monitor-1.0-SNAPSHOT-jar-with-dependencies.jar"


while read line
do
	ip=`echo ${line} | cut -d ' ' -f 1`
	login=`echo ${line} | cut -d ' ' -f 2`

    # 配置ssh免密
	ssh-copy-id ${login}@${ip}
	set -e -x

    scp -r $libs $jar ${login}@${ip}:~
    set -x -e

    ssh  ${login}@${ip} "yum install sysstat"

    ssh -f -q ${login}@${ip} "java -jar ./${jar}"
    set -x -e

done<${hosts_file_name}