# Zookeeper集群部署

### 前提准备

- 系统要求:CentOS7 64位,能够访问公网

- 下面以三台主机进行集群搭建演示

### 搭建

1. 三台主机全部执行以下操作
    
    ```bash
    # 安装jdk
    yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel
    
    # 下载zookeeper-3.4.13,地址为清华镜像站
    curl -O https://mirrors.tuna.tsinghua.edu.cn/apache/zookeeper/zookeeper-3.4.13/zookeeper-3.4.13.tar.gz
    
    # 解压
    tar zxvf ./zookeeper-3.4.13.tar.gz
    
    # 设置默认配置文件
    cp ./zookeeper-3.4.13/conf/zoo_sample.cfg ./zookeeper-3.4.13/conf/zoo.cfg
    ``` 

2. 分别修改配置文件
    
    分别向三台主机中zookeeper安装目录中 `conf/zoo.cfg`追加或修改以下内容.
    
    **注意:若下述配置中hosts为公网ip或域名,其主机上的本机host应填写为0.0.0.0**

    ```properties
    # 数据存放路径
    dataDir=/root/zookeeper/data

    # host1配置
    # server.x中,x为服务编号,将在下面配置中使用
    server.1=0.0.0.0:2888:3888
    server.2=${host2}:2888:3888
    server.3=${host3}:2888:3888
    
    # host2配置
    #server.1=${host1}:2888:3888
    #server.2=0.0.0.0:2888:3888
    #server.3=${host3}:2888:3888

    # host3配置
    #server.1=${host1}:2888:3888
    #server.2=${host1}:2888:3888
    #server.3=0.0.0.0:2888:3888
    ```
    修改完上述配置后,在各个主机的数据存放路径中创建名为 *myid* 的文件,将host对应的服务编号写入其中.

3. 启动服务及常用命令

    - 服务启动
    
        ```bash
        # 启动服务
        zookeeper-3.4.13/bin/zkServer.sh start
        
        # 状态查看
        zookeeper-3.4.13/bin/zkServer.sh status
        
        # 停止服务
        #zookeeper-3.4.13/bin/zkServer.sh stop
        ```
    
    - 常用命令
    
        ```bash
        # 进入cli
        zookeeper-3.4.13/bin/zkServer.sh/zkCli.sh
        
        # 查看目录 
        # [zk: localhost:2181(CONNECTED) 0]
        ls /
        
        # 查看文件
        # [zk: localhost:2181(CONNECTED) 1]
        get /zookeeper/quota
        
        # 添加用户名密码,将${username}及${password}替换为你自己的
        # [zk: localhost:2181(CONNECTED) 2]
        addauth digest ${username}:${password}
        ```
        **设置权限后请务必牢记用户名密码,这将在配置Dubbo服务注册中心时使用.**


