# -*- coding: UTF-8 -*-

import FaceDetector
from flask import Flask, request, abort

import json

fd = FaceDetector.FaceDetector()

# Flask初始化参数尽量使用你的包名，这个初始化方式是官方推荐的，官方解释：http://flask.pocoo.org/docs/0.12/api/#flask.Flask
app = Flask(__name__)


@app.route('/face')
def face():
    if not request.args or 'file' not in request.args:
        abort(404)
    return json.dumps(fd.face_features68(request.args['file']), default=lambda obj: obj.__dict__);


if __name__ == "__main__":
    # 这种是不太推荐的启动方式，我这只是做演示用，官方启动方式参见：http://flask.pocoo.org/docs/0.12/quickstart/#a-minimal-application
    app.run(debug=True)
