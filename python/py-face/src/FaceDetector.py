# -*- coding: UTF-8 -*-
from json import JSONEncoder

import dlib
import json


class Rect(object):
    """
    矩形类,包含上下左右四个int值
    """

    def __init__(self, top, bottom, left, right):
        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.__dict__)

    # def __repr__(self):
    #     return self.__str__()


class Point(object):
    """
    坐标点,包含x,y两个成员变量
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return str(self.__dict__)

    # def __repr__(self):
    #     return self.__str__()


class Face(object):
    """
    脸部特征
    """

    # 人脸位置
    rect = None;

    # 68个特征点
    points = [];

    def __init__(self, rect, points):
        self.points = points;
        self.rect = rect;

    def json(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return str(self.__dict__)

    # def __repr__(self):
    #     return self.__str__()


class FaceDetector(object):
    """
    脸部特征提取类
    """

    # 人脸预测器
    predictor = None;

    # 探测器
    detector = None;

    '''
    初始化类，加载模型文件
    :parameter predictor_data 模型文件
    '''

    def __init__(self, predictor_data="./shape_predictor_68_face_landmarks.dat"):
        self.predictor = dlib.shape_predictor(predictor_data);
        self.detector = dlib.get_frontal_face_detector()

    '''
    人脸检测及特征提取
    :parameter image 要提取特征的人脸图像文件
    '''

    def face_features68(self, image):
        img = dlib.load_rgb_image(image);
        dets = self.detector(img, 1);

        # 判断是否有人脸
        self.face_num = len(dets);
        if self.face_num <= 0:
            return None;

        faces = [];
        # 遍历每张人脸提取特征
        '''
        :parameter num 第几个人脸
        :parameter fr face rect 人脸矩阵
        '''
        for num, fr in enumerate(dets):


            # Get the landmarks/parts for the face in box d.
            points = [];
            shape = self.predictor(img, fr)
            print("Part 0: {}, Part 1: {} ...".format(shape.part(0),
                                                      shape.part(1)))
            for part in shape.parts():
                points.append(Point(part.x, part.y))

            faces.append(Face(Rect(fr.top(), fr.bottom(), fr.left(), fr.right()), points));

        return faces



# faces_file = "/home/wangruihuan/PycharmProjects/untitled1/4.jpeg"
#
# fd = FaceDetector();
# res = fd.face_features68(faces_file);
#
# print(res)
# print(json.dumps(res, default=lambda obj: obj.__dict__))
#
