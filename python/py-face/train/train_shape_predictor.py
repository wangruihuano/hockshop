#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import dlib

# 图像及标注数据路径
faces_folder = "./faces"

# 参数设置
options = dlib.shape_predictor_training_options()
options.oversampling_amount = 10
options.nu = 0.05
options.tree_depth = 2
options.be_verbose = True

# 标注数据
training_xml_path = os.path.join(faces_folder, "training_with_face_landmarks.xml")
dlib.train_shape_predictor(training_xml_path, "predictor.dat", options)

# 测试
print("\n训练准确性: {}".format(
    dlib.test_shape_predictor(training_xml_path, "predictor.dat")))

testing_xml_path = os.path.join(faces_folder, "testing_with_face_landmarks.xml")
print("测试准确性: {}".format(
    dlib.test_shape_predictor(testing_xml_path, "predictor.dat")))


