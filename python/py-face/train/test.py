#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import glob
import time
import sys

from PIL import Image,ImageDraw

import dlib

# 使用案例
predictor = dlib.shape_predictor("predictor.dat")
detector = dlib.get_frontal_face_detector()

print("显示faces文件夹中图像的检测和预测...")
# win = dlib.image_window()
#for f in glob.glob(os.path.join(faces_folder, "*.jpg")):
f = "./test.jpg"
if len(sys.argv) == 2:
	f = sys.argv[1]

print("处理文件: {}".format(f))
img = dlib.load_rgb_image(f)

im = Image.open(f)

dets = detector(img, 1)
print("Number of faces detected: {}".format(len(dets)))
for k, d in enumerate(dets):

    draw = ImageDraw.Draw(im)

    print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
        k, d.left(), d.top(), d.right(), d.bottom()))

    draw.rectangle((d.left(), d.top(), d.right(), d.bottom()))
    shape = predictor(img, d)
    print("Part 0: {}, Part 1: {} ...".format(shape.part(0),
                                              shape.part(1)))
    for part in shape.parts():
        draw.point((part.x,part.y))

im.show()
#im.save("test.png")

