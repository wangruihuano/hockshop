# 人脸特征提取python实现

介于java对图像的复杂程度及性能,在[marmot项目](/java/application/marmot)中的人脸特征提取部分采用http请求本项目,由本项目处理后响应.

## 环境搭建
    
  环境搭建部分参阅[目录/script/py_face/下的README](/script/py_face/README.md)
  
    
## 接口说明

**下述接口中的${url}为部署本项目之后的API地址.**

```
url: http://${url}/face?file=${file}
请求方式: GET
参数说明: file,string类型,图像文件在本项目所在主机的绝对路径
```
  
## 特征模型训练

1. 准备训练样本数据

    由于训练样本压缩之后仍有1.7GB,所以请自行下载,地址为[http://dlib.net/files/data/ibug_300W_large_face_landmark_dataset.tar.gz ](http://dlib.net/files/data/ibug_300W_large_face_landmark_dataset.tar.gz).
    
    下载之后解压,根据你解压的路径修改[train_shape_predictor.py](./train/train_shape_predictor.py)中faces_folder变量的值.

2. 训练

    我们提供了一些默认训练参数,你可以根据机器的性能及需求修改[train_shape_predictor.py](./train/train_shape_predictor.py)中 *options*的属性值.
    
    修改之后,在train目录下执行 `python ./train_shape_predictor.py`.
    
    训练的时间及准确性根据由样本数量,机器性能及配置参数决定,训练之后将会打印出训练结果,训练的模型将会在train目录下生成一个名为 *predictor.dat*的文件.
    
3. 可视化测试

    可视化用到了python图形库PIL,可以通过执行 `pip install pillow`进行安装.
    
    你可以根据自行准备的测试数据所在目录修改[test.py](./train/test.py)中faces_folder变量的值.
    
    在train目录下执行 `python ./test.py`,你将会看到如下所示的标注后的图片.
    
    ![test.png](./train/test.png)