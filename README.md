# 数字资产存证系统

## 概述

部署存证应用的区块链网络，存证应用可以通过大数据、深度学习、图像识别等方式对数字资产进行识别并提取其中关键信息，将数字资产中关键信息及数字资产的md5值保存到区块链，同时将数字资产保存到存储服务器。

### 功能性需求

1. 提供WEB展示页面，包括并不限于：可查询、可验证（是否被篡改）、可展示等功能；
2. 将数字资产保存到存储服务器；
3. 用户可从本应用查询相应的数字资产信息，若要鉴权可以查看数字资产进行比对；
4. 可直接在文本中提取信息、使用图像识别等工具、或利用大数据、深度学习将数字资产中的关键信息（包括但不限于中、英文等语言）及数字资产的md5值保存至区块链网络。


  ![系统架构](doc/img/SystemStuct.png)

  ![集群示意图](doc/img/集群示意图.png)

  ![模块设计及关系](doc/img/模块及其间关系2.0.png)

## 系统环境搭建

本系统依赖如下环境,我们建议按照顺序进行部署.

1. 区块链

   - Hyperledger fabric 部署

        在此项目中，我们使用 Hyperledger fabric 运行区块链网络，关于 Hyperledger fabric 环境的搭建请参阅[Hyperledger fabric 部署](/script/fabric_script/README.md).

2. Zookeeper 部署

    在2.0版本中,为便于系统扩展及负载均衡,我们将dfs,blockchain,marmot三个项目作为独立服务分离出来,服务发现及注册中心采用zookeeper,同时zookeeper被hadoop高可用服务中journalnode所依赖,关于zookeeper部署参阅[/script/zookeeper/README.md](/script/zookeeper/README.md).

3. Hadoop HDFS 部署

    本系统的文件存储采用hadoop的分布式文件系统HDFS,关于这部分的部署参阅[这里](/script/hadoop_script/README.md).

    在2.0版本中,我们改进了之前版本中hadoop dhfs中namenode存在的单节点故障问题,采用主备配置,关于高可用部署请参阅[hadoop hdfs高可用部署](/script/hadoop_script/ha/README.md).

4. 监控服务

    机器性能及状态监控服务的部署参阅[monitor部署](/script/monitor/README.md).

5. py-face 部署

    特征提取项目中人脸特征提取的部分由python开发,关于[py-face项目](/python/py-face)的部署参阅[这里](/script/py_face/README.md).

6. java各项服务部署

    2.0版本中,我们将dfs,marmot,blockchain这三个项目作为RPC独立服务分离出来,关于这RPC服务的部署请参阅[/java/application/README.md](/java/application/README.md).

6. restful API 部署

    在上述服务全部启用之后，部署API项目,API项目部署参阅[这里](/java/application/httpapi/README.md).

7. web 项目部署

    前端web项目部署请参阅 [这里](/html/hockshop-web/README.md).

-----------
## 应用操作手册

  应用操作手册请参阅[hockshop-web/README.md](/html/hockshop-web/README.md).

----------
## 关键技术

### 非对称加密的用户认证方式

  本项目中采用非对称加密的RSA公私钥进行用户认证,认证流程如下所示:

![公私钥认证流程](doc/img/RSA认证流程.png)

### 区块网络中间件

  在实际开发过程中,我们遇到了诸如java依赖冲突,单项服务异常引起服务不稳定等问题,所以将与区块网络交互的部分作开发了一个服务中间件,以网络交互的方式替换本地JAVA调用,现在用户请求资产存证流程如下所示:

![请求流程](doc/img/中间件流程.png)

### 应用容器化

  由于本系统较为复杂,单独的服务包括 *mysql,redis,apache,hadoop,fabric,tomcat,flask*,这还未包括应用的构建及运行,这使得运维的工作较为繁琐,所以除部分应用采用传统的物理主机部署之外,我们采用docker技术将包括fabric,hadoop及py-face应用的部署进行了容器化处理,关于docker使用及部署,请详细参阅[script目录](script)下的内容.

### 数据缓存及失败重传

  由于本应用几乎所有关于用户数据的操作均通过进行,而网络IO又是极为消耗时间的操作,所以缓存在本项目中具有极为重要的地位.

1. 文件最终一致缓存

    由于文件的存储在本应用中是以哈希值进行区分,所以文件操作之间相互没有对时间的依赖性,我们选择对系统资源要求更低的最终一致性缓存策略.即文件保存至缓存中但未保存至HDFS中时也认为保存成功。
    针对由网络或者HDFS服务瘫痪造成的上传失败,失败任务将会在重传队列中按照策略(包括:定时重传,随机轮询,增量重试等)进行重传.
    流程如下图所示:

![文件缓存](doc/img/文件缓存.png)

2. 区块信息的强一致缓存

    区块信息的操作与时间具有紧密的联系,关于区块信息的缓存,需要对操作进行加锁确保操作的事务性.
    流程如下所示:

![区块信息缓存](doc/img/区块信息缓存.png)

### 人脸图像特征识别

人脸特征点提取采用 ERT（ensemble of regression trees）级联回归，即基于梯度提高学习的回归树方法。该算法使用级联回归因子，使用一系列标定好的人脸图片作为训练集，然后使用训练后的模型进行特征点提取。关于自定义模型训练及配置参考[系统扩展部分](##系统扩展及二次开发).

### 图像文字提取


1. 二值化提取出轮廓

2. 去掉同一条直线上的点，以及将直线变平滑（如90度角处理成45度）得到点集

3. 再次去冗余。大概去掉距离比较近的点

   最后得到的点类似于笔画拐点，点的个数也是mf特征的个数（两点一条直线一个特征，n个点闭合有n条直线，即有n个特征，最后提取的特征为直线的属性

4. MF特征计算

   通过拐点提取特征点，取两个拐点的直线的中点坐标，直线长度,直线角度,最终提取到的特征

### 前后端分离

  为便于项目扩展及前端移植,本项目中的前后端以RestfulAPI接口通过json数据进行交互.流程如下所示.

![前后端分离](doc/img/前后端分离.png)

-------------
## 系统扩展及二次开发

### 自定义分布式文件系统

  在本项目中,我们采用便于后续大数据处理Hadoop HDFS进行分布式文件存储,你也可以通过实现 [hockshop-dfs项目中的DFS.java接口](/java/application/dfs-api/src/main/java/edu/ycu/hockshop/dfsapi/DFS.java)进行自定义文件存储系统,实现之后,修改[hockshop-httpapi项目中的AppConfig.java](hockshop-httpapi/src/main/java/edu/ycu/hockshop/httpapi/AppConfig.java)中的 *public DFS dfs(){}* 方法,将其返回为你自定义类的实例化对象,最后重新打包部署项目。

### 资产特征提取

1. 添加自定义资产特征提取方式

  为便于系统扩展及可维护性,我们对特征提取的java项目进行了接口规范,关于自定义特征提取的规范请参阅[信息抽取标准化项目说明](/java/application/marmot-standard/README.md).

2. 图像文字提取语言种类添加

  由于字数数量及语言种类数量繁多,将其全部纳入信息提取项目中尚有不妥,你可以根据需要从[https://github.com/tesseract-ocr/tessdata](https://github.com/tesseract-ocr/tessdata)下载对应的文字训练后数据至[marmot项目中的resources/tessdata目录下](hockshop-marmot/src/main/resources/tessdata),同时参照已有的内容及java文档注释修改[marmot项目中TextImg.java文件](hockshop-marmot/src/main/java/edu/ycu/hockshop/marmot/media/TextImg.java)和[TextImg.java文件](hockshop-marmot/src/main/java/edu/ycu/hockshop/marmot/media/TextImg.java),修改之后,重新安装(`mvn clean install`)marmot项目及httpapi项目.

3. 自定义图像特征点提取

  在本项目中,人脸特征点的提取采用python开发.若你拥有足够数量的其他物体特征标注文件,你可以训练其他物品特征点提取的模型,关于模型训练及部署参阅[py-face项目](/python/py-face/README.md).

### 自定义前端WEB项目

  在本项目中,前后端是完全分离的,若有需要,你可以参阅[http-api项目的http接口说明](/java/application/httpapi/README.md#接口说明)自定义前端或者其他平台的应用开发.
